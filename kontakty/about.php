<section class="section about-company">
      <div class="container about-company__container">
        <h2 class="section__title about-company__title">О компании VIGO</h2>
        <div class="about-company__img">
          <picture>
            <source type="image/webp" data-srcset="/upload/vigo/logo-vigo.webp">
            <img data-src="/upload/vigo/logo-vigo.png" alt="Vigo" width="500" class="lazyload">
          </picture>
        </div>
        <p class="about-company__text">VIGO – это современная российская компания, которая с 2010 года производит мебель для ванных комнат эконом и медиум класса. Узкая специализация компании позволяет более углубленно заниматься оптимизацией производства, разработкой новых моделей. Благодаря тщательному изучению современных дизайнерских тенденций в интерьере, спроса и потребностей покупателей, мы создаем стильную, функциональную и красивую мебель.</p>
      </div>
</section>