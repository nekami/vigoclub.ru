<section class="section about-company about-company--club">
      <div class="container about-company__container">
        <h2 class="section__title about-company__title">VigoClub</h2>
        <div class="about-company__img about-company__img">
          <picture>
            <source type="image/webp" data-srcset="/upload/vigo/VigoClub-logo.webp">
            <img data-src="/upload/vigo/VigoClub-logo.png" alt="Vigo" width="500" class="lazyload">
          </picture>
        </div>
        <p class="about-company__text">VigoClub – это мотивационная программа для розничных продавцов компании VIGO. Она создана с целью повышения качества обслуживания наших клинетов на торговых точках, дополнительной мотивации продавцов и объединению нашей большой дружной семьи по всей России и за ее пределами.</p>
      </div>
</section>