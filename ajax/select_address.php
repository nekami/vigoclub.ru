<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

CModule::IncludeModule("iblock");


if ( ($_POST['action'] == 'outlet') && !empty($_POST['ID_CITY']) ) 
{	
    $id_city = trim($_POST['ID_CITY']);
	
	$rsElements = CIBlockElement::GetList(["SORT"=>"NAME"], ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $id_city, 'ACTIVE' => 'Y'], false, false, ['ID', 'NAME']);
	
	$arOutlet = [];
	
	while ($arElement = $rsElements->Fetch())
	{
		$arOutlet[$arElement['ID']] = $arElement['NAME'];	
	}
	
	echo json_encode($arOutlet);
}


if ( ($_POST['action'] == 'outlet_address') && !empty($_POST['ID_OUTLET']) ) 
{	
    $id_outlet = trim($_POST['ID_OUTLET']);
	
	$rsElements = CIBlockElement::GetList(["SORT"=>"PROPERTY_ADDRESSES"], ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ACTIVE' => 'Y', 'ID' => $id_outlet], false, false, ['ID', 'PROPERTY_ADDRESSES']);
	
	$arAddresses = [];
	
	while ($arElement = $rsElements->Fetch())
	{
		$arAddresses[$arElement['PROPERTY_ADDRESSES_VALUE_ID']] = $arElement['PROPERTY_ADDRESSES_VALUE'];
	}

	echo json_encode($arAddresses);
}

?>