$(function() 
{	
	// вывод тогровых точек в зависимости от города		
	$(document).on('change', 'select[name="WORK_CITY"]', function(){		
			
		var id_city = $(this).val();

		if (id_city != 0) 
		{	
			$.ajax({
				url: '/ajax/select_address.php',
				data: {'ID_CITY': id_city, 'action': 'outlet'},
				method: 'POST',
				type: 'POST',
			    dataType: 'json',
				success: function(data)
				{	
					$('select[name="WORK_COMPANY"]').html('<option value="0"></option>');
					$('select[name="WORK_STREET"]').html('<option value="0"></option>');
					
					$.each(data, function(key, value) 
					{							 
						$('select[name="WORK_COMPANY"]').append('<option value="'+key+'">'+value+'</option>');
					});
				},         
				error:  function(data){
					console.log('Ошибка ajax-скрипта');
				}
			})
		}							
	});


	// вывод адресов в зависимости от тороговой точки
	$(document).on('change', 'select[name="WORK_COMPANY"]', function(){
			
		var id_outlet = $(this).val();

		if (id_outlet != 0) 
		{	
			$.ajax({
				url: '/ajax/select_address.php',
				data: {'ID_OUTLET': id_outlet, 'action' : 'outlet_address'},
				method: 'POST',
				type: 'POST',
			    dataType: 'json',
				success: function(data)
				{	
					$('select[name="WORK_STREET"]').html('<option value="0"></option>');
					
					$.each(data, function(key, value) 
					{							 
						$('select[name="WORK_STREET"]').append('<option value="'+key+'">'+value+'</option>');
					});
				},         
				error:  function(data){
					console.log('Ошибка ajax-скрипта');
				}
			})
		}						
	});	
	
}); 