<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест");
?>

<?
$transationID = time();
$login = 'vitrina_test6';
$password = 'Bhsue3Vf6';
$IdProduct = 1;

// выбор продукта

$method = 'GetProduct';
$url = 'https://test.mgc-loyalty.ru/v1/'.$method;
$hash = md5($transationID.$method.$login.$password);

$request = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Request>
   <Authentication>
       <Login>{$login}</Login>
       <TransactionID>{$transationID}</TransactionID>
       <MethodName>{$method}</MethodName>
       <Hash>{$hash}</Hash>
   </Authentication>
   <Parameters>       
	   <Products>           
           <Product>{$IdProduct}</Product>	   
       </Products>	   
   </Parameters>
</Request>
XML;

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml; charset=utf-8',
    'Content-Length: ' . strlen($request))
);
curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

$xml_string = curl_exec($curl);
curl_close($curl);

s($xml_string);

// Выбор способа доставки и получение id корзины

$transationID = time()+1;

$method = 'GetDeliveryVariants';
$url = 'https://test.mgc-loyalty.ru/v1/'.$method;
$hash = md5($transationID.$method.$login.$password);

$request = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Request>
   <Authentication>
       <Login>{$login}</Login>
       <TransactionID>{$transationID}</TransactionID>
       <MethodName>{$method}</MethodName>
       <Hash>{$hash}</Hash>
   </Authentication>
   <Parameters>  
       <Products>
           <Product>
               <Id>{$IdProduct}</Id>
               <Quantity>1</Quantity>
           </Product>           
       </Products>
   </Parameters>
</Request>
XML;

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml; charset=utf-8',
    'Content-Length: ' . strlen($request))
);
curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

$xml_string = curl_exec($curl);
curl_close($curl);

//s($xml_string);


$array_data = json_decode(json_encode(simplexml_load_string($xml_string)), true);

$BasketID = $array_data['BasketID'];

$DeliveryID = $array_data['Deliverys']['Logistician']['DeliveryVariants']['DeliveryVariant']['Id'];

// Оформление заказа

$transationID = time()+2;

$method = 'MakeOrder';
$url = 'https://test.mgc-loyalty.ru/v1/'.$method;
$hash = md5($transationID.$method.$login.$password);

$request = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Request>
  <Authentication>
    <Login>{$login}</Login>
    <TransactionID>{$transationID}</TransactionID>
    <MethodName>{$method}</MethodName>
    <Hash>{$hash}</Hash>
  </Authentication>
  <Parameters> 
	<BasketID>{$BasketID}</BasketID>	
	<User>
      <FirstName>Дарья</FirstName>
      <MiddleName>Викторовна</MiddleName>
      <LastName>Малова</LastName>      
      <Phone>+79081515187</Phone>
   </User>	
   <Products>
       <Product>
           <Id>{$IdProduct}</Id>
           <DeliveryID>{$DeliveryID}</DeliveryID>
           <Quantity>1</Quantity>
       </Product>           
    </Products>
  </Parameters>
</Request>
XML;

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml; charset=utf-8',
    'Content-Length: ' . strlen($request))
);
curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

$xml_string = curl_exec($curl);
curl_close($curl);

//s($xml_string);

////////////////////////////////////////////////////////////////////////////
/*
$transationID = time()+3;

$method = 'GetOrderDownloadParams';
$url = 'http://test.mgc-loyalty.ru/v1/'.$method;
$hash = md5($transationID.$method.$login.$password);

$request = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Request>
  <Authentication>
    <Login>{$login}</Login>
    <TransactionID>{$transationID}</TransactionID>
    <MethodName>{$method}</MethodName>
    <Hash>{$hash}</Hash>
  </Authentication>
  <Parameters> 
	<BasketID>{$BasketID}</BasketID>
  </Parameters>
</Request>
XML;

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/xml; charset=utf-8',
    'Content-Length: ' . strlen($request))
);
curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

$xml_string = curl_exec($curl);
curl_close($curl);

s($xml_string);*/

?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>