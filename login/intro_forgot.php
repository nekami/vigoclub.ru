<h1 class="visually-hidden">Напомнить пароль</h1>
<section class="section hero hero--inner hero--text hero--remind">
   <div class="container hero__container">
      <p class="hero__black-text">Добро пожаловать👋</p>
      <p class="hero__gray-text">Бонусы за продажи и обучение в клубе успешных продавцов</p>
   </div>
</section>