<?
define("NEED_AUTH", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Вход на сайт");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0){
	LocalRedirect($_REQUEST["backurl"]);
}
?>

<section>	
<div class="container">		
<div class="about__text-container2">				
<p class="about__text">Вы зарегистрированы и успешно авторизовались.</p>			
<p class="about__text"><a href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>			
</div>	
</div>        
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>