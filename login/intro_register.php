<h1 class="visually-hidden">Регистрация</h1>
<section class="section hero hero--inner hero--register">
	<div class="container hero__container">
		<p class="hero__black-text">
			Нас пока немного, но мы самые крутые!👊😎
		</p>
        <p class="hero__gray-text">
			Для самых смелых энтузиастов, присоединившихся к команде VigoClub до 1 июня 2020, мы приготовили специальный вступительный бонус.
		</p>
	</div>
</section>