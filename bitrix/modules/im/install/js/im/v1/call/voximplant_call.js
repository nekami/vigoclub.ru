;(function()
{
	/**
	 * Implements Call interface
	 * Public methods:
	 * - inviteUsers
	 * - cancel
	 * - answer
	 * - decline
	 * - hangup
	 *
	 * Events:
	 * - onCallStateChanged //not sure about this.
	 * - onUserStateChanged
	 * - onStreamReceived
	 * - onStreamRemoved
	 * - onDestroy
	 */

	debug = false;

	BX.namespace('BX.Call');

	var ajaxActions = {
		invite: 'im.call.invite',
		cancel: 'im.call.cancel',
		answer: 'im.call.answer',
		decline: 'im.call.decline',
		hangup: 'im.call.hangup',
		ping: 'im.call.ping'
	};

	var pullEvents = {
		ping: 'Call::ping',
		hangup: 'Call::hangup'
	};

	var clientEvents = {
		voiceStarted: 'Call::voiceStarted',
		voiceStopped: 'Call::voiceStopped',
		microphoneState: 'Call::microphoneState'
	};

	var pingPeriod = 5000;
	var backendPingPeriod = 25000;

	// screensharing workaround
	if(window["BXDesktopSystem"])
	{
		navigator['getDisplayMedia'] = function()
		{
			var mediaParams = {
				audio: false,
				video: {
					mandatory: {
						chromeMediaSource: 'desktop',
						maxWidth: screen.width > 1920 ? screen.width : 1920,
						maxHeight: screen.height > 1080 ? screen.height : 1080,
					},
					optional: [{googTemporalLayeredScreencast: true}],
				},
			};
			return navigator.mediaDevices.getUserMedia(mediaParams);
		};
	}

	BX.Call.VoximplantCall = function(config)
	{
		BX.Call.VoximplantCall.superclass.constructor.apply(this, arguments);

		if(!window.VoxImplant)
		{
			throw new Error("Voximplant SDK is not found");
		}

		this.voximplantCall = null;

		this.signaling = new BX.Call.VoximplantCall.Signaling({
			call: this
		});

		this.peers = {};
		this.joinedElsewhere = false;

		this.screenShared = false;
		this.localVideoShown = false;

		this.clientEventsBound = false;

		this.deviceList = [];

		// event handlers
		this.__onLocalDevicesUpdatedHandler = this.__onLocalDevicesUpdated.bind(this);
		this.__onLocalMediaRendererAddedHandler = this.__onLocalMediaRendererAdded.bind(this);
		this.__onBeforeLocalMediaRendererRemovedHandler = this.__onBeforeLocalMediaRendererRemoved.bind(this);


		this.__onCallDisconnectedHandler = this.__onCallDisconnected.bind(this);
		this.__onCallMessageReceivedHandler = this.__onCallMessageReceived.bind(this);
		this.__onCallEndpointAddedHandler = this.__onCallEndpointAdded.bind(this);

		this.initPeers();

		this.pingUsersInterval = setInterval(this.pingUsers.bind(this), pingPeriod);
		this.pingBackendInterval = setInterval(this.pingBackend.bind(this), backendPingPeriod);

		this.lastPingReceivedTimeout = null;
		this.lastSelfPingReceivedTimeout = null;
	};

	BX.extend(BX.Call.VoximplantCall, BX.Call.AbstractCall);

	BX.Call.VoximplantCall.prototype.initPeers = function ()
	{
		this.users.forEach(function(userId)
		{
			this.peers[userId] = this.createPeer(userId);
		}, this);
	};

	BX.Call.VoximplantCall.prototype.reinitPeers = function ()
	{
		for (var userId in this.peers)
		{
			if(this.peers.hasOwnProperty(userId) && this.peers[userId])
			{
				this.peers[userId].destroy();
				this.peers[userId] = null;
			}
		}

		this.initPeers();
	};

	BX.Call.VoximplantCall.prototype.pingUsers = function()
	{
		if (this.ready)
		{
			var users = this.users.concat(this.userId);
			this.signaling.sendPingToUsers({userId: users}, true);
		}
	};

	BX.Call.VoximplantCall.prototype.pingBackend = function()
	{
		if (this.ready)
		{
			this.signaling.sendPingToBackend();
		}
	};

	BX.Call.VoximplantCall.prototype.createPeer = function (userId)
	{
		return new BX.Call.VoximplantCall.Peer({
			call: this,
			userId: userId,
			ready: userId == this.initiatorId,

			onStreamReceived: function(e)
			{
				this.runCallback(BX.Call.Event.onStreamReceived, e);
			}.bind(this),
			onStreamRemoved: function(e)
			{
				this.runCallback(BX.Call.Event.onStreamRemoved, e);
			}.bind(this),
			onStateChanged: this.__onPeerStateChanged.bind(this)
		})
	};

	BX.Call.VoximplantCall.prototype.getUsers = function ()
	{
		var result = {};
		for (var userId in this.peers)
		{
			result[userId] = this.peers[userId].calculatedState;
		}
		return result;
	};

	BX.Call.VoximplantCall.prototype.getClient = function()
	{
		return new Promise(function(resolve, reject)
		{
			BX.Voximplant.getClient().then(function(client)
			{
				client.enableSilentLogging();
				client.setLoggerCallback(function(e)
				{
					this.log(e.label + ": " + e.message);
				}.bind(this));

				this.bindClientEvents();

				resolve(client);
			}.bind(this)).catch(function (err)
			{
				reject(err);
			});
		}.bind(this));
	};

	BX.Call.VoximplantCall.prototype.bindClientEvents = function()
	{
		var streamManager = VoxImplant.Hardware.StreamManager.get();

		if(!this.clientEventsBound)
		{
			streamManager.on(VoxImplant.Hardware.HardwareEvents.DevicesUpdated, this.__onLocalDevicesUpdatedHandler);
			streamManager.on(VoxImplant.Hardware.HardwareEvents.MediaRendererAdded, this.__onLocalMediaRendererAddedHandler);
			streamManager.on(VoxImplant.Hardware.HardwareEvents.BeforeMediaRendererRemoved, this.__onBeforeLocalMediaRendererRemovedHandler);
			this.clientEventsBound = true;
		}
	};

	BX.Call.VoximplantCall.prototype.removeClientEvents = function()
	{
		var streamManager = VoxImplant.Hardware.StreamManager.get();
		streamManager.off(VoxImplant.Hardware.HardwareEvents.DevicesUpdated, this.__onLocalDevicesUpdatedHandler);
		streamManager.off(VoxImplant.Hardware.HardwareEvents.MediaRendererAdded, this.__onLocalMediaRendererAddedHandler);
		streamManager.off(VoxImplant.Hardware.HardwareEvents.BeforeMediaRendererRemoved, this.__onBeforeLocalMediaRendererRemovedHandler);
		this.clientEventsBound = false;
	};

	BX.Call.VoximplantCall.prototype.setMuted = function(muted)
	{
		if(this.muted == muted)
		{
			return;
		}

		this.muted = muted;

		if(this.voximplantCall)
		{
			if(this.muted)
			{
				this.voximplantCall.muteMicrophone();
			}
			else
			{
				this.voximplantCall.unmuteMicrophone();
			}
			this.signaling.sendMicrophoneState(!this.muted);
		}
	};

	BX.Call.VoximplantCall.prototype.setVideoEnabled = function(videoEnabled)
	{
		videoEnabled = (videoEnabled === true);
		if(this.videoEnabled == videoEnabled)
		{
			return;
		}

		this.videoEnabled = videoEnabled;
		if(this.voximplantCall)
		{
			if(videoEnabled)
			{
				this._showLocalVideo();
			}
			else
			{
				if(this.localVideoShown)
				{
					VoxImplant.Hardware.StreamManager.get().hideLocalVideo().then(function()
					{
						this.localVideoShown = false;
						this.runCallback(BX.Call.Event.onLocalMediaReceived, {
							tag: "main",
							stream: new MediaStream(),
						});
					}.bind(this));
				}
			}

			this.voximplantCall.sendVideo(this.videoEnabled);
		}
	};

	BX.Call.VoximplantCall.prototype.setCameraId = function(cameraId)
	{
		this.cameraId = cameraId;
		var cameraParams = {
			cameraId: this.cameraId,
			videoQuality: this.videoHd ? VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_HD : VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_nHD
		};
		if(this.voximplantCall)
		{
			VoxImplant.Hardware.CameraManager.get().setCallVideoSettings(this.voximplantCall, cameraParams);
		}

		VoxImplant.Hardware.CameraManager.get().setDefaultVideoSettings(cameraParams);
		//this._hideLocalVideo().then(this._showLocalVideo.bind(this));
	};

	BX.Call.VoximplantCall.prototype.setMicrophoneId = function(microphoneId)
	{
		if(this.microphoneId == microphoneId)
		{
			return;
		}

		this.microphoneId = microphoneId;
		if(this.voximplantCall)
		{
			VoxImplant.Hardware.AudioDeviceManager.get().setCallAudioSettings(this.voximplantCall, {
				inputId: this.microphoneId
			});
		}
	};

	BX.Call.VoximplantCall.prototype.getCurrentMicrophoneId = function()
	{
		if (this.voximplantCall.peerConnection.impl.getTransceivers)
		{
			var transceivers = this.voximplantCall.peerConnection.impl.getTransceivers();
			if(transceivers.length > 0)
			{
				var audioTrack = transceivers[0].sender.track;
				var audioTrackSettings = audioTrack.getSettings();
				return audioTrackSettings.deviceId;
			}
		}
		return this.microphoneId;
	};

	BX.Call.VoximplantCall.prototype.useHdVideo = function(flag)
	{
		this.videoHd = (flag === true);

		if(this.voximplantCall)
		{
			var cameraParams = {
				cameraId: this.cameraId,
				videoQuality: this.videoHd ? VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_HD : VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_nHD
			};
			VoxImplant.Hardware.CameraManager.get().setCallVideoSettings(this.voximplantCall, cameraParams);
		}
	};

	BX.Call.VoximplantCall.prototype._showLocalVideo = function()
	{
		return new Promise(function(resolve, reject)
		{
			VoxImplant.Hardware.StreamManager.get().showLocalVideo().then(
				function()
				{
					this.localVideoShown = true;
					resolve();
				}.bind(this),
				function()
				{
					this.localVideoShown = true;
					resolve();
				}.bind(this)
			)
		}.bind(this))
	};

	BX.Call.VoximplantCall.prototype._hideLocalVideo = function()
	{
		return new Promise(function(resolve, reject)
		{
			VoxImplant.Hardware.StreamManager.get().hideLocalVideo().then(
				function()
				{
					this.localVideoShown = false;
					resolve();
				}.bind(this),
				function()
				{
					this.localVideoShown = false;
					resolve();
				}.bind(this)
			);
		})
	};

	BX.Call.VoximplantCall.prototype.startScreenSharing = function()
	{
		if(!this.voximplantCall)
		{
			return;
		}

		this.voximplantCall.shareScreen(true, true).then(function()
		{
			this.log("Screen shared");
			this.screenShared = true;

			setTimeout(function()
			{
				if(this.voximplantCall.peerConnection.impl.getTransceivers)
				{
					this.runCallback(BX.Call.Event.onLocalMediaReceived, {
						tag: "screen",
						stream: new MediaStream([this.voximplantCall.peerConnection.impl.getTransceivers()[1].sender.track])
					});
				}
			}.bind(this), 1000);

		}.bind(this));
	};

	BX.Call.VoximplantCall.prototype.stopScreenSharing = function()
	{
		if(!this.voximplantCall)
		{
			return;
		}

		this.voximplantCall.stopSharingScreen().then(function()
		{
			this.log("Screen is no longer shared");
			this.screenShared = false;
		}.bind(this));
	};

	BX.Call.VoximplantCall.prototype.isScreenSharingStarted = function()
	{
		return this.screenShared;
	};

	/**
	 * Invites users to participate in the call.
	 *
	 * @param {Object} config
	 * @param {int[]} [config.users] Array of ids of the users to be invited.
	 */
	BX.Call.VoximplantCall.prototype.inviteUsers = function(config)
	{
		var self = this;
		this.ready = true;
		if(!BX.type.isPlainObject(config))
		{
			config = {};
		}
		var users = BX.type.isArray(config.users) ? config.users : this.users;

		this.attachToConference().then(function()
		{
			return self.signaling.inviteUsers({
				userIds: users,
				video: self.videoEnabled ? 'Y' : 'N'
			})
		}).then(function(response)
		{
			self.runCallback(BX.Call.Event.onJoin, {
				local: true
			});
			for (var i = 0; i < users.length; i++)
			{
				var userId = parseInt(users[i], 10);
				if(!self.users.includes(userId))
				{
					self.users.push(userId);
				}
				if(!self.peers[userId])
				{
					self.peers[userId] = self.createPeer(userId);

					self.runCallback(BX.Call.Event.onUserInvited, {
						userId: userId
					});
				}
				self.peers[userId].onInvited();
			}
		}).catch(function(error)
		{
			self.runCallback(BX.Call.Event.onCallFailure, {
				error: error
			});
		});
	};

	/**
	 * @param {Object} config
	 * @param {bool} [config.useVideo]
	 */
	BX.Call.VoximplantCall.prototype.answer = function(config)
	{
		this.ready = true;
		if(!BX.type.isPlainObject(config))
		{
			config = {};
		}
		this.videoEnabled = (config.useVideo == true);

		this.signaling.sendAnswer();
		this.attachToConference().then(function ()
		{
			this.log("Attached to conference");
			this.runCallback(BX.Call.Event.onJoin, {
				local: true
			});
		}.bind(this)).catch(function(error)
		{
			this.runCallback(BX.Call.Event.onCallFailure, {
				error: error
			});
		}.bind(this));
	};

	BX.Call.VoximplantCall.prototype.decline = function()
	{
		this.ready = false;

		BX.ajax.runAction(ajaxActions.decline, {
			data: {
				callId: this.id,
				callInstanceId: this.instanceId,
			}
		});
	};

	BX.Call.VoximplantCall.prototype.hangup = function(code, reason)
	{
		var data = {};
		this.ready = false;
		if(typeof(code) != 'undefined')
		{
			data.code = code;
		}
		if(typeof(reason) != 'undefined')
		{
			data.reason = reason;
		}
		this.runCallback(BX.Call.Event.onLeave, {local: true});

		data.userId = this.users;
		this.signaling.sendHangup(data);

		// for future reconnections
		this.reinitPeers();

		if(this.voximplantCall)
		{
			this.voximplantCall._replaceVideoSharing = false;
			try
			{
				this.voximplantCall.hangup();
			}
			catch (e)
			{
				console.error(e);
			}
		}

		this.screenShared = false;

		this._hideLocalVideo();
	};

	BX.Call.VoximplantCall.prototype.attachToConference = function()
	{
		var self = this;

		// workaround to set default video settings before starting call. ugly, but I do not see another way
		var cameraParams = {};
		if (this.cameraId)
		{
			cameraParams.cameraId = this.cameraId;
		}
		cameraParams.videoQuality = this.videoHd ? VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_HD : VoxImplant.Hardware.VideoQuality.VIDEO_SIZE_nHD;
		VoxImplant.Hardware.CameraManager.get().setDefaultVideoSettings(cameraParams);
		if (this.microphoneId)
		{
			VoxImplant.Hardware.AudioDeviceManager.get().setDefaultAudioSettings({
				inputId: this.microphoneId
			});
		}

		return new Promise(function(resolve, reject)
		{
			if(self.voximplantCall && self.voximplantCall.state() === "CONNECTED")
			{
				return resolve();
			}

			self.getClient().then(function(voximplantClient)
			{
				if(self.videoEnabled)
				{
					self._showLocalVideo();
				}

				try
				{
					self.voximplantCall = voximplantClient.callConference({
						number: "bx_conf_" + self.id,
						video: {sendVideo: self.videoEnabled, receiveVideo: true},
						customData: null
					});
				}
				catch (e)
				{
					console.error(e);
					return reject(e);
				}

				if(!self.voximplantCall)
				{
					console.error("could not create voximplant call");
					return reject("could not create voximplant call");
				}

				self.bindCallEvents();

				var onCallConnected = function()
				{
					self.log("Call connected");
					self.voximplantCall.removeEventListener(VoxImplant.CallEvents.Connected, onCallConnected);
					self.voximplantCall.removeEventListener(VoxImplant.CallEvents.Failed, onCallFailed);

					self.voximplantCall.addEventListener(VoxImplant.CallEvents.Failed, self.__onCallDisconnectedHandler);

					if(self.deviceList.length === 0)
					{
						navigator.mediaDevices.enumerateDevices().then(function(deviceList)
						{
							self.deviceList = deviceList;
							self.runCallback(BX.Call.Event.onDeviceListUpdated, {
								deviceList: self.deviceList
							})
						});
					}
					else
					{
						self.runCallback(BX.Call.Event.onDeviceListUpdated, {
							deviceList: self.deviceList
						})
					}
					self.signaling.sendMicrophoneState(!self.muted);

					resolve();
				};

				var onCallFailed = function(e)
				{
					self.log("Could not attach to conference", e);
					self.voximplantCall.removeEventListener(VoxImplant.CallEvents.Connected, onCallConnected);
					self.voximplantCall.removeEventListener(VoxImplant.CallEvents.Failed, onCallFailed);

					var client = VoxImplant.getInstance();
					client.enableSilentLogging(false);
					client.setLoggerCallback(null);

					reject(e);
				};

				self.voximplantCall.addEventListener(VoxImplant.CallEvents.Connected, onCallConnected);
				self.voximplantCall.addEventListener(VoxImplant.CallEvents.Failed, onCallFailed);
			}).catch(function(err)
			{
				var error;
				console.error(err);
				if(typeof(err) === "string")
				{
					// backward compatibility
					self.runCallback(BX.Call.Event.onCallFailure, {error: err})
				}
				else if(BX.type.isPlainObject(err))
				{
					if(err.hasOwnProperty('status') && err.status == 401)
					{
						error = "AUTHORIZE_ERROR";
					}
					else if(err.name === "AuthResult")
					{
						error = "AUTHORIZE_ERROR";
					}
					else
					{
						error = "UNKNOWN_ERROR";
					}

					self.runCallback(BX.Call.Event.onCallFailure, {error: error})
				}
			});
		});
	};

	BX.Call.VoximplantCall.prototype.bindCallEvents = function()
	{
		this.voximplantCall.addEventListener(VoxImplant.CallEvents.Disconnected, this.__onCallDisconnectedHandler);
		this.voximplantCall.addEventListener(VoxImplant.CallEvents.MessageReceived, this.__onCallMessageReceivedHandler);

		this.voximplantCall.addEventListener(VoxImplant.CallEvents.EndpointAdded, this.__onCallEndpointAddedHandler);
	};

	BX.Call.VoximplantCall.prototype.removeCallEvents = function()
	{
		if(this.voximplantCall)
		{
			this.voximplantCall.removeEventListener(VoxImplant.CallEvents.Disconnected, this.__onCallDisconnectedHandler);
			this.voximplantCall.removeEventListener(VoxImplant.CallEvents.MessageReceived, this.__onCallMessageReceivedHandler);
			this.voximplantCall.removeEventListener(VoxImplant.CallEvents.EndpointAdded, this.__onCallEndpointAddedHandler);
		}
	};

	BX.Call.VoximplantCall.prototype.isAnyoneParticipating = function()
	{
		for (var userId in this.peers)
		{
			if(this.peers[userId].isParticipating())
			{
				return true;
			}
		}

		return false;
	};

	BX.Call.VoximplantCall.prototype.__onPeerStateChanged = function(e)
	{
		this.runCallback(BX.Call.Event.onUserStateChanged, e);

		if(e.state == BX.Call.UserState.Failed || e.state == BX.Call.UserState.Unavailable || e.state == BX.Call.UserState.Declined)
		{
			if(!this.isAnyoneParticipating())
			{
				this.hangup();
			}
		}
	};

	BX.Call.VoximplantCall.prototype.__onPullEvent = function(command, params, extra)
	{
		var handlers = {
			'Call::answer': this.__onPullEventAnswer.bind(this),
			'Call::hangup': this.__onPullEventHangup.bind(this),
			'Call::usersInvited': this.__onPullEventUsersInvited.bind(this),
			'Call::ping': this.__onPullEventPing.bind(this),
			'Call::finish': this.__onPullEventFinish.bind(this)
		};

		if(handlers[command])
		{
			handlers[command].call(this, params);
		}
	};

	BX.Call.VoximplantCall.prototype.__onPullEventAnswer = function(params)
	{
		var senderId = params.senderId;

		if(senderId === this.userId)
		{
			return this.__onPullEventAnswerSelf(params);
		}

		if(!this.peers[senderId])
		{
			return;
		}

		this.peers[senderId].setReady(true);
	};

	BX.Call.VoximplantCall.prototype.__onPullEventAnswerSelf = function(params)
	{
		if(params.callInstanceId === this.instanceId)
		{
			return;
		}

		// call was answered elsewhere
		this.joinedElsewhere = true;
		this.runCallback(BX.Call.Event.onJoin, {
			local: false
		});
	};


	BX.Call.VoximplantCall.prototype.__onPullEventHangup = function(params)
	{
		var senderId = params.senderId;

		if(this.userId == senderId && this.instanceId != params.callInstanceId)
		{
			// Call declined by the same user elsewhere
			this.runCallback(BX.Call.Event.onLeave, {local: false});
			return;
		}

		if(!this.peers[senderId])
			return;

		this.peers[senderId].setReady(false);

		if(params.code == 603)
		{
			this.peers[senderId].setDeclined(true);
		}

		if(this.ready && !this.isAnyoneParticipating())
		{
			this.hangup();
		}
	};

	BX.Call.VoximplantCall.prototype.__onPullEventUsersInvited = function(params)
	{
		this.log('__onPullEventUsersInvited', params);
		var users = params.users;

		for(var i = 0; i < users.length; i++)
		{
			var userId = users[i];
			if(this.peers[userId])
			{
				if(this.peers[userId].calculatedState === BX.Call.UserState.Failed || this.peers[userId].calculatedState === BX.Call.UserState.Idle)
				{
					this.peers[userId].onInvited();
				}
			}
			else
			{
				this.peers[userId] = this.createPeer(userId);
				this.runCallback(BX.Call.Event.onUserInvited, {
					userId: userId
				});
				this.peers[userId].onInvited();
			}
		}
	};

	BX.Call.VoximplantCall.prototype.__onPullEventPing = function(params)
	{
		if(params.callInstanceId == this.instanceId)
		{
			// ignore self ping
			return;
		}

		if (params.senderId == this.userId)
		{
			if (!this.joinedElsewhere)
			{
				this.runCallback(BX.Call.Event.onJoin, {
					local: false
				});
				this.joinedElsewhere = true;
			}
			clearTimeout(this.lastSelfPingReceivedTimeout);
			this.lastSelfPingReceivedTimeout = setTimeout(this.__onNoSelfPingsReceived.bind(this), pingPeriod * 2.1);
		}
		clearTimeout(this.lastPingReceivedTimeout);
		this.lastPingReceivedTimeout = setTimeout(this.__onNoPingsReceived.bind(this), pingPeriod * 2.1)
	};

	BX.Call.VoximplantCall.prototype.__onNoPingsReceived = function()
	{
		if(!this.ready)
		{
			this.destroy();
		}
	};

	BX.Call.VoximplantCall.prototype.__onNoSelfPingsReceived = function()
	{
		this.runCallback(BX.Call.Event.onLeave, {
			local: false
		});
		this.joinedElsewhere = false;
	};

	BX.Call.VoximplantCall.prototype.__onPullEventFinish = function(params)
	{
		this.destroy();
	};

	BX.Call.VoximplantCall.prototype.__onLocalDevicesUpdated = function(e)
	{
		this.log("__onLocalDevicesUpdated", e);
	};

	BX.Call.VoximplantCall.prototype.__onLocalMediaRendererAdded = function(e)
	{
		var renderer = e.renderer;
		this.log("__onLocalMediaRendererAdded", renderer.kind);

		if(renderer.kind === "video")
		{
			this.runCallback(BX.Call.Event.onLocalMediaReceived, {
				tag: "main",
				stream: renderer.stream,
				//stream: new MediaStream()
			});
		}
	};

	BX.Call.VoximplantCall.prototype.__onBeforeLocalMediaRendererRemoved = function(e)
	{
		var renderer = e.renderer;
		this.log("__onBeforeLocalMediaRendererRemoved", renderer.kind);

	};

	BX.Call.VoximplantCall.prototype.__onCallDisconnected = function(e)
	{
		this.log("__onCallDisconnected", e);

		this.ready = false;

		this._hideLocalVideo();
		this.removeCallEvents();
		this.voximplantCall = null;

		var client = VoxImplant.getInstance();
		client.enableSilentLogging(false);
		client.setLoggerCallback(null);

		this.runCallback(BX.Call.Event.onLeave, {
			local: true
		});
	};

	BX.Call.VoximplantCall.prototype.__onCallFailed = function(e)
	{
		this.log("__onCallFailed", e);

		this.ready = false;

		this._hideLocalVideo();
		this.removeCallEvents();
		this.voximplantCall = null;

		var client = VoxImplant.getInstance();
		client.enableSilentLogging(false);
		client.setLoggerCallback(null);

		// for future reconnections
		this.reinitPeers();

		this.runCallback(BX.Call.Event.onLeave, {
			local: true
		});
	};

	BX.Call.VoximplantCall.prototype.__onCallEndpointAdded = function(e)
	{
		var endpoint = e.endpoint;
		var userName = endpoint.userName;
		this.log("__onCallEndpointAdded (" + userName + ")", e.endpoint);

		if(BX.type.isNotEmptyString(userName) && userName.substr(0, 4) == 'user')
		{
			// user connected to conference
			var userId = parseInt(userName.substr(4));
			if(this.peers[userId])
			{
				this.peers[userId].setEndpoint(endpoint);
			}
		}
		else
		{
			endpoint.addEventListener(VoxImplant.EndpointEvents.InfoUpdated, function(e)
			{
				var endpoint = e.endpoint;
				var userName = endpoint.userName;
				this.log("VoxImplant.EndpointEvents.InfoUpdated (" + userName + ")", e.endpoint);

				if(BX.type.isNotEmptyString(userName) && userName.substr(0, 4) == 'user')
				{
					// user connected to conference
					var userId = parseInt(userName.substr(4));
					if(this.peers[userId])
					{
						this.peers[userId].setEndpoint(endpoint);
					}
				}
			}.bind(this));

			this.log('Unknown endpoint ' + userName);
		}
	};

	BX.Call.VoximplantCall.prototype.__onCallMessageReceived = function(e)
	{
		var message;

		try
		{
			message = JSON.parse(e.text);
		}
		catch(err)
		{
			this.log("Could not parse scenario message.", err);
			return;
		}

		var eventName = message.eventName;
		if(eventName === clientEvents.voiceStarted)
		{
			this.runCallback(BX.Call.Event.onUserVoiceStarted, {
				userId: message.senderId
			});
		}
		else if(eventName === clientEvents.voiceStopped)
		{
			this.runCallback(BX.Call.Event.onUserVoiceStopped, {
				userId: message.senderId
			});
		}
		else if (eventName === clientEvents.microphoneState)
		{
			this.runCallback(BX.Call.Event.onUserMicrophoneState, {
				userId: message.senderId,
				microphoneState: message.microphoneState === "Y"
			});
		}
		else
		{
			this.log("Unknown scenario event " + eventName);
		}
	};

	BX.Call.VoximplantCall.prototype.destroy = function()
	{
		this.ready = false;
		if(this.voximplantCall)
		{
			if(this.voximplantCall.state() != "ENDED")
			{
				this.voximplantCall.hangup();
			}
		}
		this.voximplantCall = null;

		for(var userId in this.peers)
		{
			if(this.peers.hasOwnProperty(userId) && this.peers[userId])
			{
				this.peers[userId].destroy();
			}
		}

		this.removeClientEvents();

		clearTimeout(this.lastPingReceivedTimeout);
		clearTimeout(this.lastSelfPingReceivedTimeout);
		clearInterval(this.pingUsersInterval);
		clearInterval(this.pingBackendInterval);
		this.runCallback(BX.Call.Event.onDestroy);
	};


	BX.Call.VoximplantCall.Signaling = function(params)
	{
		this.call = params.call;
	};

	BX.Call.VoximplantCall.Signaling.prototype.inviteUsers = function(data)
	{
		return this.__runAjaxAction(ajaxActions.invite, data);
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendAnswer = function(data)
	{
		return this.__runAjaxAction(ajaxActions.answer, data);
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendCancel = function(data)
	{
		return this.__runAjaxAction(ajaxActions.cancel, data);
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendHangup = function(data)
	{
		if(BX.PULL.isPublishingEnabled())
		{
			this.__sendPullEvent(pullEvents.hangup, data);
			data.retransmit = false;
			this.__runAjaxAction(ajaxActions.hangup, data);
		}
		else
		{
			data.retransmit = true;
			this.__runAjaxAction(ajaxActions.hangup, data);
		}
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendVoiceStarted = function(data)
	{
		return this.__sendMessage(clientEvents.voiceStarted, data);
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendVoiceStopped = function(data)
	{
		return this.__sendMessage(clientEvents.voiceStopped, data);
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendMicrophoneState = function(microphoneState)
	{
		return this.__sendMessage(clientEvents.microphoneState, {
			microphoneState: microphoneState ? "Y" : "N"
		});
	};


	BX.Call.VoximplantCall.Signaling.prototype.sendPingToUsers = function(data)
	{
		if (BX.PULL.isPublishingEnabled())
		{
			this.__sendPullEvent(pullEvents.ping, data, 0);
		}
	};

	BX.Call.VoximplantCall.Signaling.prototype.sendPingToBackend = function()
	{
		this.__runAjaxAction(ajaxActions.ping, {retransmit: false});
	};


	BX.Call.VoximplantCall.Signaling.prototype.__sendPullEvent = function(eventName, data, expiry)
	{
		expiry = expiry || 5;
		if(!data.userId)
		{
			throw new Error('userId is not found in data');
		}

		if(!BX.type.isArray(data.userId))
		{
			data.userId = [data.userId];
		}
		data.callInstanceId = this.call.instanceId;
		data.senderId = this.call.userId;
		data.callId = this.call.id;
		data.requestId = BX.Call.Engine.getInstance().getUuidv4();

		this.call.log('Sending p2p signaling event ' + eventName + '; ' + JSON.stringify(data));
		BX.PULL.sendMessage(data.userId, 'im', eventName, data, expiry);
	};

	BX.Call.VoximplantCall.Signaling.prototype.__sendMessage = function(eventName, data)
	{
		if(!this.call.voximplantCall)
		{
			return;
		}

		if(!BX.type.isPlainObject(data))
		{
			data = {};
		}
		data.eventName = eventName;
		data.requestId = BX.Call.Engine.getInstance().getUuidv4();

		this.call.voximplantCall.sendMessage(JSON.stringify(data));
	};

	BX.Call.VoximplantCall.Signaling.prototype.__runAjaxAction = function(signalName, data)
	{
		if(!BX.type.isPlainObject(data))
		{
			data = {};
		}

		data.callId = this.call.id;
		data.callInstanceId = this.call.instanceId;
		data.requestId = BX.Call.Engine.getInstance().getUuidv4();
		return BX.ajax.runAction(signalName, {data: data});
	};

	BX.Call.VoximplantCall.Peer = function(params)
	{
		this.userId = params.userId;
		this.call = params.call;

		this.ready = !!params.ready;
		this.calling = false;
		this.declined = false;
		this.inviteTimeout = false;
		this.endpoint = null;

		this.stream = null;

		this.tracks = {
			audio: null,
			video: null,
			sharing: null
		};

		this.callingTimeout = 0;

		this.callbacks = {
			onStateChanged: BX.type.isFunction(params.onStateChanged) ? params.onStateChanged : BX.DoNothing,
			onStreamReceived: BX.type.isFunction(params.onStreamReceived) ? params.onStreamReceived : BX.DoNothing,
			onStreamRemoved: BX.type.isFunction(params.onStreamRemoved) ? params.onStreamRemoved : BX.DoNothing
		};

		// event handlers
		this.__onEndpointRemoteMediaAddedHandler = this.__onEndpointRemoteMediaAdded.bind(this);
		this.__onEndpointRemoteMediaRemovedHandler = this.__onEndpointRemoteMediaRemoved.bind(this);
		this.__onEndpointRemovedHandler = this.__onEndpointRemoved.bind(this);

		this.calculatedState = this.calculateState();
	};

	BX.Call.VoximplantCall.Peer.prototype = {

		setReady: function(ready)
		{
			ready = !!ready;
			if (this.ready == ready)
			{
				return;
			}
			this.ready = ready;
			if(this.calling)
			{
				clearTimeout(this.callingTimeout);
				this.calling = false;
			}
			this.updateCalculatedState();
		},

		setDeclined: function(declined)
		{
			this.declined = declined;
			if(this.calling)
			{
				clearTimeout(this.callingTimeout);
				this.calling = false;
			}
			this.updateCalculatedState();
		},

		setEndpoint: function(endpoint)
		{
			this.log("Adding endpoint with " + endpoint.mediaRenderers.length + " media renderers");

			this.setReady(true);
			this.inviteTimeout = false;
			this.declined = false;

			if(this.endpoint)
			{
				this.removeEndpointEventHandlers();
				this.endpoint = null;
			}

			this.endpoint = endpoint;

			for(var i = 0; i < this.endpoint.mediaRenderers.length; i++)
			{
				this.addMediaRenderer(this.endpoint.mediaRenderers[i]);
				if(this.endpoint.mediaRenderers[i].element)
				{
					BX.remove(this.endpoint.mediaRenderers[i].element);
				}
			}

			this.bindEndpointEventHandlers();
		},

		addMediaRenderer: function(mediaRenderer)
		{
			this.log('Adding media renderer');
			if(!this.stream)
			{
				this.stream = new MediaStream();
			}

			mediaRenderer.stream.getTracks().forEach(function(track)
			{
				if (track.kind == "audio")
				{
					this.tracks.audio = track;
				}
				else if (track.kind == "video")
				{
					if(mediaRenderer.kind == "sharing")
					{
						this.tracks.sharing = track;
					}
					else
					{
						this.tracks.video = track;
					}
				}
				else
				{
					this.log("Unknown track kind " + track.kind);
				}

			}, this);

			this.updateMediaStream();
			this.updateCalculatedState();
		},

		updateMediaStream: function()
		{
			if(!this.stream)
			{
				this.stream = new MediaStream();
			}

			this.stream.getTracks().forEach(function(track)
			{
				if(!this.hasTrack(track))
				{
					this.stream.removeTrack(track);
				}
			}, this);

			if(this.tracks.audio && !this.stream.getTrackById(this.tracks.audio.id))
			{
				this.stream.addTrack(this.tracks.audio);
			}

			if(this.tracks.sharing)
			{
				if(this.tracks.video && this.stream.getTrackById(this.tracks.video.id))
				{
					this.stream.removeTrack(this.tracks.video);
				}

				if(!this.stream.getTrackById(this.tracks.sharing.id))
				{
					this.stream.addTrack(this.tracks.sharing);
				}
			}
			else
			{
				if (this.tracks.video && !this.stream.getTrackById(this.tracks.video.id))
				{
					this.stream.addTrack(this.tracks.video);
				}
			}

			this.callbacks.onStreamReceived({
				userId: this.userId,
				stream: this.stream
			});
		},

		hasTrack: function(track)
		{
			for (var kind in this.tracks)
			{
				if (!this.tracks.hasOwnProperty(kind))
				{
					continue;
				}

				if(this.tracks.kind && this.tracks.kind.id == track.id)
				{
					return true;
				}
			}

			return false;
		},

		removeTrack: function(track)
		{
			for (var kind in this.tracks)
			{
				if (!this.tracks.hasOwnProperty(kind))
				{
					continue;
				}

				var localTrackId = this.tracks[kind] ? this.tracks[kind].id : '';
				if(localTrackId == track.id)
				{
					this.tracks[kind] = null;
				}
			}
		},

		bindEndpointEventHandlers: function()
		{
			this.endpoint.addEventListener(VoxImplant.EndpointEvents.RemoteMediaAdded, this.__onEndpointRemoteMediaAddedHandler);
			this.endpoint.addEventListener(VoxImplant.EndpointEvents.RemoteMediaRemoved, this.__onEndpointRemoteMediaRemovedHandler);
			this.endpoint.addEventListener(VoxImplant.EndpointEvents.Removed, this.__onEndpointRemovedHandler);
		},

		removeEndpointEventHandlers: function()
		{
			this.endpoint.removeEventListener(VoxImplant.EndpointEvents.RemoteMediaAdded, this.__onEndpointRemoteMediaAddedHandler);
			this.endpoint.removeEventListener(VoxImplant.EndpointEvents.RemoteMediaRemoved, this.__onEndpointRemoteMediaRemovedHandler);
			this.endpoint.removeEventListener(VoxImplant.EndpointEvents.Removed, this.__onEndpointRemovedHandler);
		},

		calculateState: function()
		{
			if(this.stream)
				return BX.Call.UserState.Connected;

			if(this.endpoint)
				return BX.Call.UserState.Connecting;

			if(this.calling)
				return BX.Call.UserState.Calling;

			if(this.inviteTimeout)
				return BX.Call.UserState.Failed;

			if(this.declined)
				return BX.Call.UserState.Declined;

			if(this.ready)
				return BX.Call.UserState.Ready;

			return BX.Call.UserState.Idle;
		},

		updateCalculatedState: function()
		{
			var calculatedState = this.calculateState();

			if(this.calculatedState != calculatedState)
			{
				this.callbacks.onStateChanged({
					userId: this.userId,
					state: calculatedState,
					previousState: this.calculatedState
				});
				this.calculatedState = calculatedState;
			}
		},

		isParticipating: function()
		{
			return ((this.calling || this.ready || this.endpoint) && !this.declined);
		},

		onInvited: function()
		{
			this.ready = false;
			this.inviteTimeout = false;
			this.declined = false;
			this.calling = true;

			if(this.callingTimeout)
			{
				clearTimeout(this.callingTimeout);
			}
			this.callingTimeout = setTimeout(this.onInviteTimeout.bind(this), 30000);
			this.updateCalculatedState();
		},

		onInviteTimeout: function()
		{
			clearTimeout(this.callingTimeout);
			this.calling = false;
			this.inviteTimeout = true;
			this.updateCalculatedState();
		},

		__onEndpointRemoteMediaAdded: function(e)
		{
			this.log("VoxImplant.EndpointEvents.RemoteMediaAdded", e);

			this.addMediaRenderer(e.mediaRenderer);
		},

		__onEndpointRemoteMediaRemoved: function(e)
		{
			this.log("VoxImplant.EndpointEvents.RemoteMediaRemoved, track id: " + e.mediaRenderer.stream.getTracks()[0].id, e);

			e.mediaRenderer.stream.getTracks().forEach(function(track)
			{
				this.removeTrack(track);
			}, this);

			if(this.stream)
			{
				this.updateMediaStream();
			}

			this.updateCalculatedState();
		},

		__onEndpointRemoved: function(e)
		{
			this.log("VoxImplant.EndpointEvents.Removed", e);

			if(this.endpoint)
			{
				this.removeEndpointEventHandlers();
				this.endpoint = null;
			}
			if(this.stream)
			{
				this.stream = null;
			}
			for(var kind in this.tracks)
			{
				if(this.tracks.hasOwnProperty(kind))
				{
					this.tracks[kind] = null;
				}
			}

			this.updateCalculatedState();
		},

		log: function()
		{
			this.call.log.apply(this.call, arguments);
		},

		destroy: function()
		{
			if(this.stream)
			{
				this.stream.getTracks().forEach(function(track)
				{
					track.stop();
				});
				this.stream = null;
			}
			if(this.endpoint)
			{
				this.removeEndpointEventHandlers();
				this.endpoint = null;
			}
			for(var kind in this.tracks)
			{
				if(this.tracks.hasOwnProperty(kind))
				{
					if(this.tracks[kind] && this.tracks[kind].stop)
					{
						this.tracks[kind].stop();
					}
					this.tracks[kind] = null;
				}
			}

			this.callbacks['onStateChanged'] = BX.DoNothing;
			this.callbacks['onStreamReceived'] = BX.DoNothing;
			this.callbacks['onStreamRemoved'] = BX.DoNothing;

			clearTimeout(this.callingTimeout);
			this.callingTimeout = null;
		}
	};
})();