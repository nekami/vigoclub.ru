<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001588097569';
$dateexpire = '001624097569';
$ser_content = 'a:2:{s:7:"CONTENT";s:9175:"	
    <section class="section gift">
      <div class="container">
        <h1 class="section__title gift__title">Подарочный сертификат 585*ЗОЛОТОЙ</h1>
        <div class="gift__wrap">
          <div class="gift__img">
			   				
					<img src="/upload/iblock/e56/e562494dcc2b5f508c585aee9aeb13d7.png" alt="585*ЗОЛОТОЙ" title="585*ЗОЛОТОЙ" width="200" />
							
			             </div>
          <div class="gift__type">
		  
			 	
			 
				<div class="gift__type-wrap">
				  <p class="gift__type-title">Электронный сертификат</p>
				  <button type="button" class="gift__details-btn" aria-label="Показать детальное описание"></button>
				  <div class="gift__type-details-wrap">
					<div class="gift__type-details">Электронные сертификаты доставляются на электронную почту в течение 10 минут при наличии. Пластиковые сертификаты комплектуются в течение 5 рабочих дней и доставлюются курьерской службой в сроки указанные при оформлении заказа.</div>
				  </div>
				</div>
				
			 	
			 
          </div>
          <form class="form gift__form" action="index.html" method="post">
            <input type="hidden" name="TITLE" value="Подарочный сертификат S7 Airlines">
            <div class="form__item">
              <label class="form__label" for="chain">Номинал</label>			  
			  <select class="form__select gift__select" name="VALUE" id="chain">			  
								
									
						<option data-bonus="500" value="500">500</option>
										
					
										
					
								
									
						<option data-bonus="1000" value="1000">1000</option>
										
					
										
					
								
									
						<option data-bonus="3000" value="3000">3000</option>
										
					
										
					
								
									
						<option data-bonus="5000" value="5000">5000</option>
										
					
										
					
								
              </select>				  
            </div>
			
            <p class="gift__bonus">
									<br/> 
								
				<span>бонусов</span>
			</p>
			
            <button type="submit" class="btn gift__form-btn" id="take">Получить</button>
          </form>
        </div>
        <div class="gift__info-wrap">
          <div class="gift__info-buttons">
            <button type="button" class="gift__info-button gift__info-button--active" data-id="#description">Описание</button>
            <button type="button" class="gift__info-button" data-id="#howto">Как воспользоваться сертификатом?</button>
            <button type="button" class="gift__info-button" data-id="#rules">Правила</button>
          </div>
          <div class="gift__detailes-wrap">
            
							<div class="gift__detail gift__detail--time">
				  <p class="gift__detail-title">Срок действия:</p>
				  <p class="gift__detail-text">500 р - 3 мес / 1000 р - 6 мес / 3000 р - 9 мес / 5000 р - 12 мес / ЭПС: 1год</p>
				</div>
						
					
			
            <div class="gift__detail gift__detail--doc">
              <p class="gift__detail-text"><a>Пользовательское соглашение</a></p>
            </div>
          </div>
          <div class="gift__descriprion content">
            <div class="gift__descriprion-item gift__descriprion-item--active" id="description">
              <p class="gift__descriprion-title">Описание</p>
              <div class="gift__descriprion-text">                
				<p>Подарочный сертификат сети ювелирных салонов "585*Золотой", где вы найдете стильные и модные украшения по самым привлекательным ценам.</p>

<p>Сеть ювелирных салонов "585*Золотой" основана в Санкт-Петербурге в 2000 году и уже более 18 лет по праву занимает лидирующую позицию на ювелирном рынке России, радуя своих покупателей качественными золотыми и серебряными украшениями, низкими ценами и постоянно действующими акционными предложениями! Ювелирная сеть "585*Золотой" является почетным членом Гильдии Ювелиров России: поддерживаем традиции качества российского золота, которое по праву считается лучшим в мире – за это его любят наши покупатели! Миссия компании – делать золото доступным: с чем мы с успехом справляемся!</p>

<p>"585*Золотой" предлагает всем любителям драгоценных украшений большой выбор ювелирных изделий из белого, красного и желтого золота, из серебра, в продаже можно найти украшения с бриллиантами, драгоценными и полудрагоценными камнями и эмалью, обручальные кольца, серьги, цепи, браслеты, подвески и ювелирную косметику.
Для клиентов всегда доступны удобные формы оплаты, гибкие системы скидок и специальные акции.</p>

<p>Мы делаем все для того, чтобы вы могли порадовать себя и своих близких красивыми и желанными ювелирными подарками!</p>				
			  </div>
            </div>
            <div class="gift__descriprion-item" id="howto">
              <p class="gift__descriprion-title">Как воспользоваться сертификатом?</p>
              <div class="gift__descriprion-text">
				<p>Подарочный сертификат (ПС) принимаются к оплате во всех магазинах Компании.</p>

<p>﻿Если стоимость выбранного товара превышает номинал ПС, возможна доплата наличным или безналичным (банковская карточка) расчетом. В случае выбора товара на сумму меньше номинала ПС остаток не возвращается.</p>

<p>﻿ПС в случае утери или повреждения не восстанавливается. Обмену и возврату не подлежит.
ПС активируется в течение 5 (пяти) дней с момента покупки и может быть использован не позднее срока.
В случае возврата товара, приобретенного с участием ПС, сумма оплаченного ПС, возвращается клиенту.
﻿ПС является платежным средством для единовременной покупки товара, после чего возвращается продавцу.
﻿ПС можно свободно передавать третьему лицу</p>

<p>﻿Гарантийное обслуживание, обмен и возврат товара, приобретенных с использованием ПС, осуществляется в общем порядке. По истечению срока действия ПС к оплате не принимается, его стоимость не возвращается.</p>

<p>На товары, приобретенные с использованием ПС, распространяются  все рекламные предложения и скидки согласно действующему положению по рекламной компании, а также скидки по Клубной Карте сети на основании действующего Положения по программе Лояльности Клуба 585*Золотой.</p>			  </div>
            </div>
            <div class="gift__descriprion-item" id="rules">
              <p class="gift__descriprion-title">Правила</p>
              <div class="gift__descriprion-text">
				<p>Электронный подарочный сертификат принимается в интернет-магазине https://www.gold585.ru</p>				
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	<script>		
		BX.message({
			TEMPLATE_PATH: \'/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default\'
		});
	</script>";s:4:"VARS";a:2:{s:8:"arResult";a:1:{s:3:"SEO";a:1:{s:18:"ELEMENT_META_TITLE";s:18:"585*ЗОЛОТОЙ";}}s:18:"templateCachedData";a:4:{s:12:"additionalJS";s:100:"/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default/script.js";s:9:"frameMode";N;s:12:"frameModeCtx";s:103:"/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default/template.php";s:16:"component_epilog";a:5:{s:10:"epilogFile";s:111:"/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default/component_epilog.php";s:12:"templateName";s:8:".default";s:12:"templateFile";s:103:"/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default/template.php";s:14:"templateFolder";s:90:"/local/templates/vigo-club/components/bitrix/catalog/prizy/bitrix/catalog.element/.default";s:12:"templateData";b:0;}}}}';
return true;
?>