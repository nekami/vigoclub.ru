<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001588094847';
$dateexpire = '001624094847';
$ser_content = 'a:2:{s:7:"CONTENT";s:3794:"    <section class="section prize">
      <div class="container prize__container">
        <h2 class="section__title prize__title">Призы</h2>
        <ul class="prize__list list">
		
								
			  <li class="prize__item">
				<a href="/prizy/sunlight/">
					<div class="prize__item-top">
					  <div class="prize__image" aria-hidden="true">
						<picture>
						  <source data-srcset="/upload/iblock/cb3/cb384012e44c08ecf049f454a76044e8.png" media="(min-width: 780px)">
						  <img data-src="/upload/iblock/cb3/cb384012e44c08ecf049f454a76044e8.png" alt="SUNLIGHT" class="lazyload">
						</picture>
					  </div>
					  <p class="prize__text">SUNLIGHT</p>
					</div>
				</a>
				<div class="prize__item-bottom">
				  <p class="prize__bonuses">
					<span class="prize__quantity">					
					
												
							300 - 30000						
							
					
					</span><span class="prize__bonus">бонусов</span>
				  </p>
				  <a href="" class="prize__btn btn">Получить</a>
				</div>
			  </li>
		  
								
			  <li class="prize__item">
				<a href="/prizy/585-zolotoy/">
					<div class="prize__item-top">
					  <div class="prize__image" aria-hidden="true">
						<picture>
						  <source data-srcset="/upload/iblock/e56/e562494dcc2b5f508c585aee9aeb13d7.png" media="(min-width: 780px)">
						  <img data-src="/upload/iblock/e56/e562494dcc2b5f508c585aee9aeb13d7.png" alt="585*ЗОЛОТОЙ" class="lazyload">
						</picture>
					  </div>
					  <p class="prize__text">585*ЗОЛОТОЙ</p>
					</div>
				</a>
				<div class="prize__item-bottom">
				  <p class="prize__bonuses">
					<span class="prize__quantity">					
					
												
							500 - 5000						
							
					
					</span><span class="prize__bonus">бонусов</span>
				  </p>
				  <a href="" class="prize__btn btn">Получить</a>
				</div>
			  </li>
		  
								
			  <li class="prize__item">
				<a href="/prizy/tsum/">
					<div class="prize__item-top">
					  <div class="prize__image" aria-hidden="true">
						<picture>
						  <source data-srcset="/upload/iblock/b21/b21a256631ddc60ec48b17cdabe52b40.png" media="(min-width: 780px)">
						  <img data-src="/upload/iblock/b21/b21a256631ddc60ec48b17cdabe52b40.png" alt="ЦУМ" class="lazyload">
						</picture>
					  </div>
					  <p class="prize__text">ЦУМ</p>
					</div>
				</a>
				<div class="prize__item-bottom">
				  <p class="prize__bonuses">
					<span class="prize__quantity">					
					
												
							3000 - 100000						
							
					
					</span><span class="prize__bonus">бонусов</span>
				  </p>
				  <a href="" class="prize__btn btn">Получить</a>
				</div>
			  </li>
		  
								
			  <li class="prize__item">
				<a href="/prizy/ozon/">
					<div class="prize__item-top">
					  <div class="prize__image" aria-hidden="true">
						<picture>
						  <source data-srcset="/upload/iblock/def/def1b38bfb99febbacb7ffbde8245ff5.png" media="(min-width: 780px)">
						  <img data-src="/upload/iblock/def/def1b38bfb99febbacb7ffbde8245ff5.png" alt="ОЗОН" class="lazyload">
						</picture>
					  </div>
					  <p class="prize__text">ОЗОН</p>
					</div>
				</a>
				<div class="prize__item-bottom">
				  <p class="prize__bonuses">
					<span class="prize__quantity">					
					
												
							500 - 10000						
							
					
					</span><span class="prize__bonus">бонусов</span>
				  </p>
				  <a href="" class="prize__btn btn">Получить</a>
				</div>
			  </li>
		  
				
        </ul>
        <div class="link-wrap">
          <a href="/prizy/" class="link-next">Показать все</a>
        </div>
      </div>
    </section>
	
	<script>
	BX.message({
		TEMPLATE_PATH: \'/local/templates/vigo-club/components/bitrix/news.list/prizes\'
	});
	</script>";s:4:"VARS";a:2:{s:8:"arResult";a:7:{s:2:"ID";s:1:"6";s:14:"IBLOCK_TYPE_ID";s:6:"prizes";s:13:"LIST_PAGE_URL";s:17:"#SITE_DIR#/prizy/";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:10:"Призы";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:4:{i:0;s:4:"2214";i:1;s:4:"2216";i:2;s:4:"2204";i:3;s:2:"37";}}s:18:"templateCachedData";a:4:{s:12:"additionalJS";s:71:"/local/templates/vigo-club/components/bitrix/news.list/prizes/script.js";s:9:"frameMode";b:1;s:17:"__currentCounters";a:1:{s:28:"bitrix:system.pagenavigation";i:1;}s:13:"__editButtons";a:8:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:4:"2214";i:2;s:172:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=6&type=prizes&ID=2214&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2F%3Fclear_cache%3DY";i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:4:"2214";i:2;s:123:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=6&type=prizes&lang=ru&action=delete&ID=E2214&return_url=%2F%3Fclear_cache%3DY";i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:4:"2216";i:2;s:172:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=6&type=prizes&ID=2216&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2F%3Fclear_cache%3DY";i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:4:"2216";i:2;s:123:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=6&type=prizes&lang=ru&action=delete&ID=E2216&return_url=%2F%3Fclear_cache%3DY";i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:4:"2204";i:2;s:172:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=6&type=prizes&ID=2204&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2F%3Fclear_cache%3DY";i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:4:"2204";i:2;s:123:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=6&type=prizes&lang=ru&action=delete&ID=E2204&return_url=%2F%3Fclear_cache%3DY";i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"37";i:2;s:170:"/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=6&type=prizes&ID=37&lang=ru&force_catalog=&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2F%3Fclear_cache%3DY";i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"37";i:2;s:121:"/bitrix/admin/iblock_list_admin.php?IBLOCK_ID=6&type=prizes&lang=ru&action=delete&ID=E37&return_url=%2F%3Fclear_cache%3DY";i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>