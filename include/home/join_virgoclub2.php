	 <section class="section about">
      <div class="container">
        <h2 class="section__title">О программе</h2>
        <div class="about__text-container">
          <p class="about__text">Зарегистрируйтесь и заполните анкету участника.</p>
          <p class="about__text">Совершайте продажи продукции и регистрируйте их на нашем сайте. За продажи и другие активности вы получаете бонусы, которые можно обменять на ценные призы.</p>
        </div>
        <div class="about__link-wrap">
          <a class="about__link" title="Регистрация" href="/login/?register=yes">Вступай в VigoClub</a>
        </div>
      </div>
    </section>