    <section class="section balance balance--inner balance--gift">
      <div class="container balance__container balance__container--inner">
        <div class="balance__account balance__account--inner">
          <div class="balance__img avatar">
            <img width="100" src="<?=GetPersonalPhoto($USER->GetId());?>" alt="<?=$USER->GetFullName();?>" class="poly--cover">
          </div>
          <p class="balance__name"><?=$USER->GetFullName();?></p>
        </div>
        <p class="balance__inner-text balance__inner-text--middle">Вы можете выбрать приз в <b>Лиге новчиков</b> 
				<? if ($USER->IsAuthorized()) { ?> на <b><?=GetSumBonus($USER->GetID());?></b> Бонусов <? } ?>
		</p>		
        <? if ($USER->IsAuthorized())  { ?>
				<p class="balance__inner-text"><a class="balance__link" title="Личный кабинет" href="/account/">Мой профиль</a></p>
		<? } else { ?>
				<p class="balance__inner-text"><a class="balance__link" title="Авторизация" href="/login/">Авторизация</a></p>
		<? } ?>
      </div>
    </section>