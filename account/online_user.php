<section class="section balance">
      <div class="container balance__container">
        <div class="balance__account">
          <div class="balance__img avatar">
            <img width="100" src="<?=GetPersonalPhoto($USER->GetId());?>" alt="<?=$USER->GetFullName();?>" class="poly--cover">
          </div>
          <p class="balance__name"><?=$USER->GetFullName();?></p>
        </div>
        <p class="balance__quantity">Баланс: <?=GetSumBonus($USER->GetID());?> Бонусов</p>
        <div class="balance__persent">
          <p class="balance__text">Заполненность профиля:</p>
          <div class="balance__persentage-quantity">
            <p class="balance__persent-text"></p>
            <div class="balance__bar">
              <svg class="progress" width="120" height="120" viewBox="0 0 120 120">
                <circle class="progress__meter" cx="60" cy="60" r="54" />
                <circle class="progress__value" cx="60" cy="60" r="54"/>
              </svg>
            </div>
          </div>
        </div>
      </div>
</section>