<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Продажи");
?>

    <h1 class="visually-hidden">Продажи</h1>	
	
	<!-- Банер -->	
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"banner", 
	array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "prodazhi",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "blocks",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PROPERTY_CODE" => array(
			0 => "NAME_BANNER",
			1 => "DESC_BANNER",
			2 => "BANNER_PNG_PC",
			3 => "BANNER_PNG_MOB",
			4 => "BANNER_WEBP_PC",
			5 => "BANNER_WEBP_MOB",
			6 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "banner"
	),
	false
);?>
	
    <? // Вы можете выбрать приз в Лиге новчиков
	$APPLICATION->IncludeFile(
		"/include/common/choice_prize.php",
		Array(),
		Array("MODE"=>"php")
	);
	?>	
	
	<section class="section sale-menu">
      <nav class="sale-menu__contaiter container">
        <ul class="sale-menu__list list">
          <li class="sale-menu__item">
            <a class="sale-menu__nav-link sale-menu__nav-link--active">Добавить продажу</a>
          </li>
          <li class="sale-menu__item">
            <a class="sale-menu__nav-link">Мои продажи</a>
          </li>
          <li class="sale-menu__item">
            <a class="sale-menu__nav-link">Правила</a>
          </li>
        </ul>
      </nav>
    </section>
	
    
			<?$APPLICATION->IncludeComponent(
				"bitrix:infoportal.element.add.form", 
				"transactions", 
				array(
					"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
					"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
					"CUSTOM_TITLE_DETAIL_PICTURE" => "Загрузить фото чека",
					"CUSTOM_TITLE_DETAIL_TEXT" => "",
					"CUSTOM_TITLE_IBLOCK_SECTION" => "",
					"CUSTOM_TITLE_NAME" => "",
					"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
					"CUSTOM_TITLE_PREVIEW_TEXT" => "",
					"CUSTOM_TITLE_TAGS" => "",
					"DEFAULT_INPUT_SIZE" => "30",
					"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
					"ELEMENT_ASSOC" => "CREATED_BY",
					"GROUPS" => array(
						0 => "2",
					),
					"IBLOCK_ID" => "7",
					"IBLOCK_TYPE" => "points",
					"LEVEL_LAST" => "Y",
					"LIST_URL" => "",
					"MAX_FILE_SIZE" => "0",
					"MAX_LEVELS" => "100000",
					"MAX_USER_ENTRIES" => "100000",
					"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
					"PROPERTY_CODES" => array(
						0 => "16",
						1 => "18",
						2 => "34",
						3 => "35",
						4 => "NAME",
						5 => "IBLOCK_SECTION",
						6 => "DETAIL_PICTURE",
					),
					"PROPERTY_CODES_REQUIRED" => array(
						0 => "16",
						1 => "18",
						2 => "NAME",
						3 => "IBLOCK_SECTION",
						4 => "DETAIL_PICTURE",
					),
					"RESIZE_IMAGES" => "N",
					"SEF_MODE" => "N",
					"STATUS" => "ANY",
					"STATUS_NEW" => "N",
					"USER_MESSAGE_ADD" => "Транзакция успешно добавлена",
					"USER_MESSAGE_EDIT" => "Транзакция успешно добавлена",
					"USE_CAPTCHA" => "N",
					"COMPONENT_TEMPLATE" => "transactions"
				),
				false
			);?>
	
<section class="section sale">
	<div class="container">
		Содержимое 2
	</div>
</section>
<section class="section sale">
	<div class="container">
		Содержимое 3
	</div>
</section>
	
	
<script>
  $(".section.sale").not(":first").hide();
  $('.sale-menu__list.list  .sale-menu__item').click(function() 
  {	
	$(this).children('a.sale-menu__nav-link').addClass('sale-menu__nav-link--active');	
	$('.sale-menu__list.list  .sale-menu__item').children('a.sale-menu__nav-link').not($(this).children('a.sale-menu__nav-link')).removeClass('sale-menu__nav-link--active');
	$(".section.sale").hide().eq($(this).index()).fadeIn();
  });
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>