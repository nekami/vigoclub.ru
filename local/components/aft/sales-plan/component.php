<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('main');

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;


$arNavParams = array("nPageSize" => $arParams["NEWS_COUNT"]);

$arNavigation = CDBResult::GetNavParams($arNavParams);

if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
	$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

$arParams["PERIOD"] = "MONTH";

if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation)))
{	
	$arResult["ITEMS"] = [];	
	
	$arFilter = ['IBLOCK_ID' => SALES_PLAN_IBLOCK_ID, 'IBLOCK_SECTION_CODE' => date('Y'), 'ACTIVE'=>'Y']; 
			 
	$res = CIBlockElement::GetList(['SORT' => 'ASC'], $arFilter, false, false, ['ID', 'SORT', "CREATED_DATE", 'NAME', "PROPERTY_PLAN_BONUSES"]);   
	while ($ar_fields = $res->Fetch()) 
	{
		if (strlen($ar_fields['SORT']) == 1) $ar_fields['SORT'] = '0'.$ar_fields['SORT'];
		
		$arResult["ITEMS"][$ar_fields['SORT']] = 
		[
			"PLAN_BONUSES" => $ar_fields["PROPERTY_PLAN_BONUSES_VALUE"],
			"MONTH"        => $ar_fields["NAME"]
		];	
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$arFilter = ['IBLOCK_ID' => PRIZES_IBLOCK_ID, 'ACTIVE'=>'Y'];			 
	$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID', "CREATED_DATE", "PROPERTY_POINTS"]);   
	while ($ar_fields = $res->Fetch()) 
	{
		$arResult["ITEMS"][explode(".",$ar_fields["CREATED_DATE"])[1]]["SALES_FACT"][] = [
			"ID" => $ar_fields["ID"],
			"FACT_BONUSES" => $ar_fields["PROPERTY_POINTS_VALUE"]
		];
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	foreach ($arResult["ITEMS"] as $key => $item) 
	{
		
		s($item["SALES_FACT"]);
		
		
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$this->setResultCacheKeys(array("ITEMS"));
	$this->includeComponentTemplate();
}