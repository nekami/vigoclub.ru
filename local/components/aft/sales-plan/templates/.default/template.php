<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

    <section class="section filter filter--plan">
      <div class="container">
        <form class="form filter__form filter__form--block" action="index.html" method="post">
          <fieldset class="form__fieldset filter__fieldset">
            <ul class="filter__list list">
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="period" value="" id="month" checked>
                <label class="filter__check-label" for="month">Месяц</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="period" value="" id="quarter">
                <label class="filter__check-label" for="quarter">Квартал</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="period" value="" id="year">
                <label class="filter__check-label" for="year">Год</label>
              </li>
            </ul>
          </fieldset>
        </form>
      </div>
    </section>
    <section class="section plan">
      <div class="container plan__container">
        <table class="plan__table table">
          <thead>
            <tr>
              <th>Период</th>
              <th>План, бонусы</th>
              <th>Факт, бонусы</th>
              <th>Осталось, бонусы</th>
              <th>Ста&shy;тус</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Январь, 2020</td>
              <td>10000</td>
              <td>11070</td>
              <td>0</td>
              <td><span class="success">ОК😎</span></td>
            </tr>
            <tr class="active">
              <td>Февраль, 2020</td>
              <td>12000</td>
              <td>9500</td>
              <td>2500</td>
              <td><span class="fail">Не ОК</span></td>
            </tr>
            <tr>
              <td>Март, 2020</td>
              <td>15000</td>
              <td>0</td>
              <td>15000</td>
              <td></td>
            </tr>
            <tr>
              <td>Апрель, 2020</td>
              <td>20000</td>
              <td>0</td>
              <td>20000</td>
              <td></td>
            </tr>
            <tr>
              <td>Май, 2020</td>
              <td>25000</td>
              <td>0</td>
              <td>25000</td>
              <td></td>
            </tr>
            <tr>
              <td>Июнь, 2020</td>
              <td>30000</td>
              <td>0</td>
              <td>30000</td>
              <td></td>
            </tr>
            <tr>
              <td>Июль, 2020</td>
              <td>35000</td>
              <td>0</td>
              <td>35000</td>
              <td></td>
            </tr>
            <tr>
              <td>Август, 2020</td>
              <td>40000</td>
              <td>0</td>
              <td>40000</td>
              <td></td>
            </tr>
            <tr>
              <td>Сентябрь, 2020</td>
              <td>45000</td>
              <td>0</td>
              <td>45000</td>
              <td></td>
            </tr>
            <tr>
              <td>Октябрь, 2020</td>
              <td>50000</td>
              <td>0</td>
              <td>50000</td>
              <td></td>
            </tr>
            <tr>
              <td>Ноябрь, 2020</td>
              <td>55000</td>
              <td>0</td>
              <td>55000</td>
              <td></td>
            </tr>
            <tr>
              <td>Декабрь, 2020</td>
              <td>70000</td>
              <td>0</td>
              <td>70000</td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>

<?=$arResult["NAV_STRING"]?>