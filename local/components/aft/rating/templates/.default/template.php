<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="section rating">
	<div class="container rating__container">
        <table class="rating__table">
          <thead>
            <tr>
              <th><?=GetMessage('USER_POSITION')?></th>
              <th></th>
              <th><?=GetMessage('USER_LOGIN')?></th>
              <th><?=GetMessage('USER_BONUSES')?></th>
            </tr>
          </thead>
          <tbody>

			<?  $i = 0;
			foreach($arResult["ITEMS"] as $arUser) { ?>		  
						<tr>
						  <td><?=++$i?></td>
						  <td class="rating__up"><?=$arUser["UF_RATING"]?></td>
						  <td>
							<span class="rating__person">
							  <span class="rating__image avatar">
								<? $foto = "/upload/vigo/no_foto.jpg";
								   if (!empty($arUser["PERSONAL_PHOTO"])) $foto = CFile::GetPath($arUser["PERSONAL_PHOTO"]);
								?>					  
								<img width="100" src="<?=$foto?>" alt="<?=$arUser['LOGIN']?>" class="poly--cover">
							  </span>
							  
							  
							  <span class="rating__name">					  
								<? if (!empty($arUser['LAST_NAME']) && !empty($arUser['LAST_NAME']))  
								{ 
									echo $arUser['LAST_NAME'].' '.$arUser['NAME'];	
								
								} else echo $arUser['LOGIN']; ?>						
							  </span>
							</span>
						  </td>
						  <td><?=$arUser["UF_SUM_BONUS"]?></td>
						</tr>	
				<? } ?>

			</tbody>
        </table>
	</div>
</section>	

<?=$arResult["NAV_STRING"]?>