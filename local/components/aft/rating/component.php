<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('main');

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;


$arNavParams = array("nPageSize" => $arParams["NEWS_COUNT"]);

$arNavigation = CDBResult::GetNavParams($arNavParams);

if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
	$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];


if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $arNavigation)))
{	
	
	$rsElement = CUser::GetList(($by="UF_SUM_BONUS"), ($order="desc"), [], 
	[
		'FIELDS' => ['ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'],
		'NAV_PARAMS' => ["nPageSize"=>$arParams["NEWS_COUNT"]],
		'SELECT'     => ['UF_RATING' , 'UF_SUM_BONUS']
	
	]);
	
	$arResult["ITEMS"] = array();
	
	while($obElement = $rsElement->Fetch())  
	{
		$arResult["ITEMS"][$obElement["ID"]] = $obElement;
	}
	
	$navComponentParameters = array();
	if ($arParams["PAGER_BASE_LINK_ENABLE"] === "Y")
	{
		$pagerBaseLink = trim($arParams["PAGER_BASE_LINK"]);
		if ($pagerBaseLink === "")
		{
			if (
				$arResult["SECTION"]
				&& $arResult["SECTION"]["PATH"]
				&& $arResult["SECTION"]["PATH"][0]
				&& $arResult["SECTION"]["PATH"][0]["~SECTION_PAGE_URL"]
			)
			{
				$pagerBaseLink = $arResult["SECTION"]["PATH"][0]["~SECTION_PAGE_URL"];
			}
			elseif (
				isset($arItem) && isset($arItem["~LIST_PAGE_URL"])
			)
			{
				$pagerBaseLink = $arItem["~LIST_PAGE_URL"];
			}
		}

		if ($pagerParameters && isset($pagerParameters["BASE_LINK"]))
		{
			$pagerBaseLink = $pagerParameters["BASE_LINK"];
			unset($pagerParameters["BASE_LINK"]);
		}

		$navComponentParameters["BASE_LINK"] = CHTTP::urlAddParams($pagerBaseLink, $pagerParameters, array("encode"=>true));
	}

	$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx(
		$navComponentObject,
		"",
		"vigo_club",
		false,
		$this,
		$navComponentParameters
	);
	$arResult["NAV_CACHED_DATA"] = null;
	$arResult["NAV_RESULT"] = $rsElement;
	$arResult["NAV_PARAM"] = $navComponentParameters;

	$this->setResultCacheKeys(array("ITEMS"));
	$this->includeComponentTemplate();
}