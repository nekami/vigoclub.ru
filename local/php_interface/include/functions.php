<?
function s($var, $userID = 0)
{
    global $USER;
    
    if (($userID > 0 && $USER->GetID() == $userID) || $userID == 0) {
        if ($var === false) {
            $var = "false";
        }
        echo "<xmp>";
        print_r($var);
        echo "</xmp>";
    }
}

function p($var, $url = false, $delimiter = false)
{
    if (empty($url)) {
        $url = $_SERVER["DOCUMENT_ROOT"] . "/logs/log.txt";
    }

    if (!empty($delimiter)) {
        $delimiter = "\n\n--- " . $delimiter . " ---\n\n";
    } else {
        $delimiter = "\n\n-------------------------------------------------\n\n";
    }
    file_put_contents($url, $delimiter . print_r($var, true), FILE_APPEND);
}

// получение фотографиии пользователя
function GetPersonalPhoto($IdUser)
{
	if (!empty($IdUser)) 
	{
		CModule::IncludeModule('main');	
		
		$phpCache = new CPHPCache();
			
		if ($phpCache->InitCache(3600, $IdUser, '/user_foto')) 
		{
			$data = $phpCache->GetVars();    
			$PersonalPhoto = $data['PersonalPhoto'];
		} 
		else 
		{
			$rsUser = CUser::GetList(($by="active"), ($order="desc"), ['ID' => $IdUser], ['FIELDS'=>['ID','PERSONAL_PHOTO']]);
		
			$PersonalPhoto = "/upload/vigo/no_foto.jpg";
			
			if ($User = $rsUser->Fetch())
			{
				if ( !empty($User['PERSONAL_PHOTO']) )
					$PersonalPhoto = CFile::GetPath($User['PERSONAL_PHOTO']);	
			}
			
			$phpCache->StartDataCache();
			$phpCache->EndDataCache(['PersonalPhoto' => $PersonalPhoto]);
		}
		
		return $PersonalPhoto;
		
	} else return false;
}


// получение директории пользователя

function GetIdDirUser($IdUser)
{
	if (!empty($IdUser)) 
	{
		CModule::IncludeModule('iblock'); 
	  
		$phpCache = new CPHPCache();
			
		if ($phpCache->InitCache(3600, $IdUser, '/diruser')) 
		{
			$data = $phpCache->GetVars();    
			$Dir  = $data['Dir'];
		} 
		else 
		{
			$arFilter = ['IBLOCK_ID'=>PRIZES_IBLOCK_ID, 'UF_USER_ID' => $IdUser];

			$Dir = "";
		  
			$db_list = CIBlockSection::GetList([], $arFilter, false, ['ID'], false);
		  
			if ($ar_fields = $db_list->GetNext()) 
				$Dir = $ar_fields["ID"];
			
			
			$phpCache->StartDataCache();
			$phpCache->EndDataCache(['Dir' => $Dir]);
		}
		
		return $Dir;
		
	} else return false;
	
}

// рейтинг вчерашнего и сегодняшенего дня

function GetRating($IdUser)
{  
  CModule::IncludeModule('iblock'); 
  
  $TodayDate = date("d.m.Y");  
  $YesterdayDate = date('d.m.Y', time() - 86400);
		
  $arFilter = ["IBLOCK_ID"=>PRIZES_IBLOCK_ID, "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => GetIdDirUser($IdUser)];
  $res = CIBlockElement::GetList( array(), $arFilter, false, false, ["ID", "CREATED_DATE"]);

  $CountYester = 0;
  $CountToday = 0;
  $rating = '';
		
  while($ar_fields = $res->GetNext())
  {  
	  $CreateDate = date("d.m.Y", strtotime($ar_fields["CREATED_DATE"]));
	  
	  if ($CreateDate == $YesterdayDate) { $CountYester = $CountYester + 1; }	  
	  if ($CreateDate == $TodayDate)     { $CountToday  = $CountToday +1;   }
  }
  
  $rating  = $CountToday - $CountYester;
  
  if ($rating > 0) $rating = '+'.$rating;
		
  return $rating;
}


// получение общих бонусов пользователя
function GetSumBonus($IdUser)
{  
	if (!empty($IdUser)) 
	{
		CModule::IncludeModule('iblock'); 
	  
		$phpCache = new CPHPCache();
			
		if ($phpCache->InitCache(3600, $IdUser, '/sumbonus')) 
		{
			$data = $phpCache->GetVars();    
			$summa = $data['summa'];
		} 
		else 
		{
			$arFilter = ["IBLOCK_ID"=>PRIZES_IBLOCK_ID, "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => GetIdDirUser($IdUser)];	

			$arSelect = array("ID", "PROPERTY_POINTS");
				 
			$res = CIBlockElement::GetList( array(), $arFilter, false, false, $arSelect );

			$summa = 0;
		  
			while ($ar_fields = $res->Fetch()) 
			{
				if (!empty($ar_fields["PROPERTY_POINTS_VALUE"]))
					$summa = $summa + $ar_fields["PROPERTY_POINTS_VALUE"];
			}
			
			$phpCache->StartDataCache();
			$phpCache->EndDataCache(['summa' => $summa]);
		}
	  
		return $summa;
		
	} else return false;
}

function GetCityTtAddress(&$city, &$company, &$street)
{  
  CModule::IncludeModule('iblock'); 
  
  $id_city = $city;
  $id_outlet = $company;
  $id_street = $street;
		
  // город  
  $arFilter = ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ACTIVE' => 'Y', 'ID' => $city];	
  $rsSections = CIBlockSection::GetList([], $arFilter, false, ['ID', 'NAME'], false);		
  if ($arSection = $rsSections->Fetch()) {
	$city = $arSection['NAME'];
  }
  
  // торговая точка
  $rsElements = CIBlockElement::GetList([], 
  ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $id_city, 'ACTIVE' => 'Y', 'ID' => $company], false, false, ['ID', 'NAME']);
  if ($arElement = $rsElements->Fetch()) {
	 $company = $arElement['NAME'];	
  }
  
  // адрес  
  $rsElements = CIBlockElement::GetList([], 
  ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ID' => $id_outlet], false, false, ['ID', 'PROPERTY_ADDRESSES']);
  while ($arElement = $rsElements->Fetch())
  {
	  if ($arElement['PROPERTY_ADDRESSES_VALUE_ID'] == $id_street)
	  {
		  $street = $arElement['PROPERTY_ADDRESSES_VALUE'];
	  }
  }
}

function isUserPassword($userId, $password)
{
    $userData = CUser::GetByID($userId)->Fetch();

    $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

    $realPassword = substr($userData['PASSWORD'], -32);
    $password = md5($salt.$password);

    return ($password == $realPassword);
}

function GetNameCategoryAndModel($IdCategory, $IdModel)
{  
  CModule::IncludeModule('iblock'); 
  
  $name = ""; 
  
  $arFilter = ['IBLOCK_ID' => CATALOG_IBLOCK_ID, 'ACTIVE' => 'Y', 'ID' => $IdCategory];	  
  $rsSections = CIBlockSection::GetList([], $arFilter, false, ['ID', 'NAME'], false);  
  if ($arSection = $rsSections->Fetch()) {
	   $name .= $arSection['NAME'];
  }
    
   		
  $arFilter = ['IBLOCK_ID' => CATALOG_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $IdCategory, 'ID' => $IdModel, 'ACTIVE'=>'Y']; 		 
  $res = CIBlockElement::GetList([], $arFilter, false, false, ['ID', 'NAME']);   
  if ($ar_fields = $res->Fetch()) {	  
	  $name .= ' ('.$ar_fields['NAME'].')';
  }
  
  return $name;
}

function ClearCart($ID_ISER, $ID_NUM_SCRATCH_CARD = false)
{
	CModule::IncludeModule('main');
    CModule::IncludeModule('iblock');   
    $user = new CUser;
	
	if (empty($ID_NUM_SCRATCH_CARD))
	{
		$arFilter = ['IBLOCK_ID' => CARDS_IBLOCK_ID, 'ACTIVE'=>'Y', 'PROPERTY_USER' => $ID_ISER];
				 
		$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID']);
		
		if ($ar_fields = $res->Fetch()) 
		{
			if ( !empty($ar_fields['ID']) )
				$ID_NUM_SCRATCH_CARD = $ar_fields['ID'];
		}
	}
	
	if (!empty($ID_NUM_SCRATCH_CARD))
	{			
		CIBlockElement::SetPropertyValuesEx($ID_NUM_SCRATCH_CARD, false, ["USER" => 0, "DATE_TIME" => ""]);
		$user->Update($ID_ISER, ["UF_PROMO_CODE" => false, "UF_NUM_SCRATCH_CARD" => false, "UF_SCRATCH_CARD" => false]);
	}
}