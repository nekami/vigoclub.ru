<?
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementAddHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementAddHandler");

function OnAfterIBlockElementAddHandler(&$arFields)
{
	if (!empty($arFields["ID"])) 
	{
		CModule::IncludeModule('main');
			
		global $USER; 
		if (!is_object($USER)) $USER = new CUser;

		$user = new CUser;	
		
		$arFields = Array(
			  "UF_RATING"     => GetRating($USER->GetID()),
			  "UF_SUM_BONUS"  => GetSumBonus($USER->GetID()),	   
		);
		
		$user->Update($USER->GetID(), $arFields);
	}	
}
?>