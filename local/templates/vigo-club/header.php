<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? IncludeTemplateLangFile(__FILE__); ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta charset=<?=LANG_CHARSET?>>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />	
	<? CJSCore::Init(array("jquery", "popup")); ?>	
	<?$APPLICATION->ShowHead();?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/lazysizes.min.js" );?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/main.js" );?>	
	<title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>

  <?$APPLICATION->ShowPanel();?> 
  
  <header class="header">
    <div class="container">
      <div class="header__top">
        <div class="header__left">
          <button type="button" name="button" class="header__burger hamburger" aria-label="Открыть меню">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
          <a href="/" class="header__logo">
            <img src="/upload/vigo/VigoClub-logo.png" alt="Vigo club логотип" width="120">
          </a>
        </div>
		
		<div class="header__right">		 
			<? if ($USER->IsAuthorized())  { ?>		 
			 
			  <a href="?logout=yes" class="header__enter-btn header__enter-btn--logout">Выйти</a>
			  
			  <a href="/account/" title="Личный кабинет" class="header__enter-btn header__enter-btn--authorized header__enter-btn--gray">
				<span class="header__avatar avatar">				
				  <img width="100" src="<?=GetPersonalPhoto($USER->GetId());?>" alt="<?=$USER->GetFullName();?>" class="poly--cover">
				</span>				
				<span>
					<?=$USER->GetFullName();?>
				</span>				
			  </a>
			  
			<? } else { ?>			  
			  
			  <a href="/login/" title="Авторизоваться" class="header__enter-btn">
				Войти
			  </a>
			  <a href="/login/?register=yes" title="Зарегистрироваться" class="header__enter-btn header__enter-btn--gray">
				Зарегистрироваться
			  </a>		
			  
			<? } ?> 
        </div>		
      </div>
      <div class="header__bottom">
        <div class="header__bottom-container">	
		
			<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu2", Array(
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"MAX_LEVEL" => "2",	// Уровень вложенности меню
					"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_TYPE" => "N",	// Тип кеширования
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"MENU_THEME" => "site",
					"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
					"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
					"COMPONENT_TEMPLATE" => "horizontal_multilevel"
				),
				false
			);?>
			
			<?$APPLICATION->IncludeComponent("bitrix:search.form", "vigo-club", Array(
				"PAGE" => "#SITE_DIR#search/",
					"USE_SUGGEST" => "Y",
					"COMPONENT_TEMPLATE" => "suggest"
				),
				false
			);?>			
			
        </div>
      </div>
    </div>
  </header>	
  
  <main class="block"> 
  
	  <? if ( strpos($APPLICATION->GetCurDir(), 'account') ) { ?>
	  
		  <?$APPLICATION->IncludeComponent("bitrix:menu", "account", Array(
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
				"ROOT_MENU_TYPE" => "left",	// Тип меню для первого уровня
				"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				"COMPONENT_TEMPLATE" => "grey_tabs"
			),
			false
		 );?>
		 
		 <? // информация по текущему пользователю
		   $APPLICATION->IncludeFile(
			 "/account/online_user.php",
			 Array(),
			 Array("MODE"=>"html")
		   );
		 ?>   
	  
	  <? } ?>