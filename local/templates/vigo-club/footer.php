<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
  </main>

  <footer class="footer">
    <div class="container footer__container">
      <a class="footer__logo" href="/">	  
        <img src="/upload/vigo/VigoClub-logo.png" alt="Vigo club логотип" width="120">	
	  </a>

		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"bottom_menu", 
			array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "2",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"COMPONENT_TEMPLATE" => "bottom_menu"
			),
			false
		);?>

	</div>	
	
    <div class="footer__links">
      <a class="footer__link" title="Положение о конфиденциальности" href="/kontakty/polozhenie-o-konfidentsialnosti/">
		  Положение о конфиденциальности
	  </a>
      <a class="footer__link" title="Условия пользования" href="/kontakty/usloviya-polzovaniya/">
		  Условия использования
	  </a>
    </div>	
  </footer>
  
  <?
	$APPLICATION->IncludeFile(
		"/include/other/modal.php",
		Array(),
		Array("MODE"=>"html")
    );
  ?> 
  
</body>