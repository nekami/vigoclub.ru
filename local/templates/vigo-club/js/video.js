let playButton = document.querySelector('.education__play-btn');
let videoWrap = document.querySelector('.education__video-img');
let video = document.querySelector('.education__video');
playButton.addEventListener('click', function() {
  this.style.display = "none";
  videoWrap.style.display = "none";
  video.style.display = "block";
  video.play();
});
