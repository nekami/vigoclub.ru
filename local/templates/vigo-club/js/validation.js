let submitBtn = document.querySelector('.form__submit');
let requiredInputs = document.querySelectorAll('input[required]');
let requiredSelects = document.querySelectorAll('select[required]');
let requiredText = document.querySelectorAll('textarea[required]');
let formMessage = document.querySelector('.popup--form');

submitBtn.addEventListener('click', function(evt) {
  if (navigator.onLine) {
    formMessage.style.display = 'none';
  } else {
    formMessage.style.display = 'block';
    document.querySelector('.popup__title--form').textContent = 'Ошибка';
    document.querySelector('.popup__text--form').textContent = 'Соединение интернета потеряно. Проверьте доступ и повторите попытку снова';
    stopSubmit = true;
  }

  for (var i = 0; i < requiredInputs.length; i++) {
    let input = requiredInputs[i];
    if (input.checkValidity() == false) {
      input.classList.add("error");
      let errorMessage = 'Поле не заполнено';
      if (input.classList.contains('form__input-text--date')) {
        let errorMessage = 'Неверный формат';
      }
      if (input.validity.rangeUnderflow) {
        var min = getAttributeValue(input, 'min');
        let errorMessage = 'Пароль должен содержать не менее ' + min + ' символов.';
      }
      if (input.validity.typeMismatch) {
        var type = getAttributeValue(input, 'type');
        if (type == "tel") {
          let errorMessage = 'Неверный формат номера телефона';
        } else if (type == "email") {
          let errorMessage = 'Неверный формат email';
        } else {
          let errorMessage = 'Неверный формат';
        }
      }
      if (input.type == "checkbox" || input.type == "radio") {} else {
        let errorBlock = document.createElement('span');
        errorBlock.classList.add("form__label");
        errorBlock.classList.add("form__label--error");
        errorBlock.innerHTML = errorMessage;
        input.parentElement.appendChild(errorBlock);
      }
      stopSubmit = true;
    } else {
      if (input.classList.contains('error')) {
        input.classList.remove("error");
      }
    }
  }
  if (requiredSelects.length > 0) {
    for (var i = 0; i < requiredSelects.length; i++) {
      let select = requiredSelects[i];
      if (select.checkValidity() == false) {
        select.classList.add("error");
        let errorMessage = 'Поле не заполнено';
        let errorBlock = document.createElement('span');
        errorBlock.classList.add("form__label");
        errorBlock.classList.add("form__label--error");
        errorBlock.innerHTML = errorMessage;
        select.parentElement.appendChild(errorBlock);
        stopSubmit = true;
      } else {
        if (select.classList.contains('error')) {
          select.classList.remove("error");
        }
      }
    }
  }

  if (requiredText.length > 0) {
    for (var i = 0; i < requiredText.length; i++) {
      let text = requiredText[i];
      if (text.checkValidity() == false) {
        text.classList.add("error");
        let errorMessage = 'Поле не заполнено';
        let errorBlock = document.createElement('span');
        errorBlock.classList.add("form__label");
        errorBlock.classList.add("form__label--error");
        errorBlock.innerHTML = errorMessage;
        text.parentElement.appendChild(errorBlock);
        stopSubmit = true;
      } else {
        if (text.classList.contains('error')) {
          text.classList.remove("error");
        }
      }
    }
  }

  if (stopSubmit) {
    evt.preventDefault();
  }
});
