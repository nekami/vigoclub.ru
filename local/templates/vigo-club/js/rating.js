function progress() {
  let quantityBalance = parseInt(document.querySelector('.balance__persent-text').textContent);
  let progressValue = document.querySelector('.progress__value');
  let radius = 54;
  let circumference = 2 * Math.PI * radius;
  let progress = quantityBalance / 100;
  let dashoffset = circumference * (1 - progress);
  progressValue.style.strokeDashoffset = dashoffset;
}
window.onload = function() {
  progress();
}
