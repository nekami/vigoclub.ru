$(function() 
{	
	// маска для телефона	
	$('.phone-input').inputmask("+7(999)999-99-99", {"placeholder": "_", clearMaskOnLostFocus : false});
		
	// маска email
	$('.mail-input').inputmask({
		mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]", 
		"placeholder": "_", 
		clearMaskOnLostFocus : false, 
		greedy: false,
		onBeforePaste: function (pastedValue, opts) {
			pastedValue = pastedValue.toLowerCase();
			return pastedValue.replace("mailto:", "");
		},
		definitions: {
			'*': {
				validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
				cardinality: 1,
				casing: "lower"
			}
		},
		oncomplete: function () {
			if (typeof checkSubscribeValid == 'function') {
				checkSubscribeValid();
			}
		},
		onincomplete: function () {
			if (typeof checkSubscribeValid == 'function') {
				checkSubscribeValid();
			}
		},
	});
	
});