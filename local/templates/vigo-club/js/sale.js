function chooseCategory(elem) {
  let category = $(elem).children('option:selected').data('category');
  if ($(elem).children('option:selected').val() == '' || !$(elem).children('option:selected').val()) {
    $(elem).parent('.sale__category').siblings('.sale__model').children('.form__select--model').children('option').prop("disabled", true);
  } else {
    $(elem).parent('.sale__category').siblings('.sale__model').children('.form__select--model').children('option[data-type!=' + category + ']').hide();
    $(elem).parent('.sale__category').siblings('.sale__model').children('.form__select--model').children('option[data-type=' + category + ']').prop("disabled", false);
  }
}

function addPoints(elem) {
  let points = $(elem).children('option:selected').data('points');
  if ($(elem).children('option:selected').val() == '' || !$(elem).children('option:selected').val()) {
    $(elem).parent('.sale__model').siblings('.sale__bonuses').hide();
  } else {
    $(elem).parent('.sale__model').siblings('.sale__bonuses').show().html('<span>+' + points + '</span> бонусов');
  }
}

function removeItem(elem) {
  let saleItemCount = $('.sale__item').length;
  if (saleItemCount > 1) {
    $(elem).parent('.sale__item-top').parent('.sale__item').remove();
    for (let i = 1; i < saleItemCount; i++) {
      if(i > 1) {
        $('.sale__item:nth-child(' + i + ') .sale__item-title').text('Товар' + i + ' из чека');
      } else {
        $('.sale__item:nth-child(' + i + ') .sale__item-title').text('Товар' + i + ' из чека. Заполняется по одной позиции');
      }
    }
  } else {
      $('.form__select--category').val('');
      $('.form__select--model').val('');
      $('.form__select--model').children('option').show().prop("disabled", true);
      $('.sale__item-title').text('Товар1 из чека. Заполняется по одной позиции');
      $('.sale__bonuses').hide();
  }
}

$(document).ready(function() {
  $('.sale__add-item').click(function() {
    let itemCount = $('.sale__item').length;
    let nextNumber = itemCount + 1;
    $('.sale__item').first().clone().appendTo(".sale__items");
    $('.sale__item:last-child .sale__item-title').text('Товар' + nextNumber + ' из чека');
    $('.sale__item:last-child .form__select--category').val('');
    $('.sale__item:last-child .form__select--model').val('');
    $('.sale__item:last-child .form__select--model').children('option').prop("disabled", true).show();
    $('.sale__item:last-child .sale__bonuses').hide();
	$('.sale__item:last-child .form__select--model').prev('input[name="M_ITEM"]').val($('.form__select--model').length);
  });
  $('.form__input-receipt').change(function(){
    if($(this).val() !== '' || $(this).val()) {
      $('.form__input-file').removeAttr('required');
    }
  });
});

let inputFile = document.querySelector('.form__input-file');
let inputsReceipt = document.querySelectorAll('.form__input-receipt');
inputFile.addEventListener('change', function(evt) {
  if(this.checkValidity() == true) {
    for (let inputReceipt of inputsReceipt) {
      inputReceipt.required = false;
    }
  }
});

let submitBtn = document.querySelector('.form__btn-submit');
let formMessage = document.querySelector('.popup--form');

submitBtn.addEventListener('click', function(evt) {
  let requiredInputs = document.querySelectorAll('input[required]');
  let requiredSelects = document.querySelectorAll('select[required]');

  /*if (navigator.onLine) {
    formMessage.style.display = 'none';
  } else {
    formMessage.style.display = 'block';
    document.querySelector('.popup__title--form').textContent = 'Ошибка';
    document.querySelector('.popup__text--form').textContent = 'Соединение интернета потеряно. Проверьте доступ и повторите попытку снова';
    stopSubmit = true;
  }*/
  

  for (var i = 0; i < requiredInputs.length; i++) {
    let input = requiredInputs[i];
    if (input.checkValidity() == false) {
      input.classList.add("error");
      let errorMessage = 'Поле не заполнено';
      if (input.classList.contains('form__input-text--date')) {
        let errorMessage = 'Неверный формат';
      }
      if (input.type == "form__input-file") {
      } else {
        let errorBlock = document.createElement('span');
        errorBlock.classList.add("form__label");
        errorBlock.classList.add("form__label--error");
        errorBlock.innerHTML = errorMessage;
        input.parentElement.appendChild(errorBlock);
      }
      stopSubmit = true;
    } else {
      if (input.classList.contains('error')) {
        input.classList.remove("error");
      }
    }
  }
  for (var i = 0; i < requiredSelects.length; i++) {
    let select = requiredSelects[i];
    if (select.checkValidity() == false) {
      select.classList.add("error");
      let errorMessage = 'Поле не заполнено';
      let errorBlock = document.createElement('span');
      errorBlock.classList.add("form__label");
      errorBlock.classList.add("form__label--error");
      errorBlock.innerHTML = errorMessage;
      select.parentElement.appendChild(errorBlock);
      stopSubmit = true;
    } else {
      if (select.classList.contains('error')) {
        select.classList.remove("error");
      }
    }
  }

  /*if (stopSubmit) {
    evt.preventDefault();
  } else {
    formMessage.style.display = 'block';
    document.querySelector('.popup__title--form').textContent = 'Чек зарегистрирован';
    document.querySelector('.popup__text--form').textContent = 'Чек добавлен в список ваших продаж. На его модерацию потербуется от 1 до 5 дней, после чего он изменит свой статус';
  }*/
});
