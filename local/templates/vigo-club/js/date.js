$(document).ready(function() {
  jQuery.datetimepicker.setLocale('ru');

  $('.form__input-text--date').datetimepicker({
    format:'d.m.Y',
    formatDate:'d.m.Y',
    timepicker:false,
    lang: 'ru'
  });

  $('.form__input-text--time').datetimepicker({
    datepicker:false,
    format:'H:i',
    lang: 'ru'
  });
});
