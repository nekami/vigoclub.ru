
	function ShowPopupWindow(id, contentBlockClass) {
		var popup = new BX.PopupWindow(
			id,
			null, 
			{
			  autoHide : true,
			  offsetTop : 1,
			  offsetLeft : 0,
			  lightShadow : true,
			  closeIcon : true,
			  closeByEsc : true,
			  overlay: {
				backgroundColor: '#000', opacity: '80'
			  },
			  events: {
				  onPopupShow: function() 
				  {
					 $('#'+id+' .'+contentBlockClass).show();
				  }
			  },
					content: $('.'+contentBlockClass)[0]
			}
		);
		
		popup.show();
	}  
