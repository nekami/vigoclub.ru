var burgerMenu = document.querySelector('.header__burger');
burgerMenu.addEventListener('click', function() {
  if(burgerMenu.classList.contains('is-active')) {
    burgerMenu.classList.remove('is-active');
  } else {
    burgerMenu.classList.add('is-active');
  }
})

$(function() 
{
  
  let scrollLength = $('.events__item').width();
  if ($(window).width() < '768') {
    $(".events__container").scrollLeft(scrollLength);
  }; 
  
  // плавная прокрутка якоря	
	var $page = $('html, body');
	$('a[href*="#"]').click(function(event) {
		if($.attr(this, 'href').search( /^\#/i ) !== -1) {
			$page.animate({
				scrollTop: $($.attr(this, 'href')).offset().top - 50
			}, 1000);
			
			event.preventDefault();
		}
	});	
});

function ScrollTopErrorText ()
{
	$('html, body').animate({
		scrollTop: $($('.errortext')[0]).offset().top
	}, 500);			
}

function ClearErrorText ()
{
	$(document).on('input', 'input', function () { 	  
		if ($(this).hasClass('errortext'))
		{			
			$(this).removeClass("errortext").next('div.error_text').hide();			
		}		
	 });
	 
	 $(document).on('change', 'select', function () { 	  
		if ($(this).hasClass('errortext'))
		{			
			$(this).removeClass("errortext").next('div.error_text').hide();

			if ($(this).attr('name') == 'MODEL[]') 
			{				
				$(this).parent('.sale__model').find('div.error_text').hide();
			} 		
		}		
	 });			
}		