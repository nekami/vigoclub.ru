$(function() 
{
	let inputFiles = document.querySelectorAll(".form__file-img");
	for (const inputFile of inputFiles) {
	  inputFile.addEventListener("change", function () {
		let thisInput = this;
		if (thisInput.files[0]) {
		  var fr = new FileReader();
		  fr.addEventListener("load", function () {
			thisInput.parentNode.querySelector("img").src = fr.result;
		  }, false);
		  fr.readAsDataURL(this.files[0]);
		}
	  });
	};
}); 