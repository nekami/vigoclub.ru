<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}
?>

<h1 class="visually-hidden">Данные профиля</h1> 
  
    <section class="section profile-form">
      <div class="container">
        <form class="profile-form__form form" action="" method="post">
		
		  <input type="hidden" name="ID_ISER" value='<?=$arResult["arUser"]["ID"]?>'>
		
          <fieldset class="form__fieldset">
            <legend class="form__legend">Личные данные</legend>
            <div class="form__item form__item--file">
              <div class="form__img avatar">				
				<input class="form__file-img visually-hidden" accept="image/*" type="file" name="PERSONAL_PHOTO" id="profile-picture">				
				<img src="<?=!empty($arResult["arUser"]["PERSONAL_PHOTO"]) ? $arResult["arUser"]["PERSONAL_PHOTO"] : "/upload/vigo/no_foto.jpg"?>" alt='<?=$USER->GetFullName()?>'>
              </div>			  
              <div class="form__file-wrap">
                <label class="form__file-btn" for="profile-picture">Изменить аватар</label>
                <p class="form__note form__note--text">Не более 2000х2000px, 2Мб</p>
              </div>		  
            </div>		
            <div class="form__item">
              <label class="form__label" for="email">Email</label>
              <input class="form__input-text" type="email" name="EMAIL" id="email" value="<?=$arResult["arUser"]["EMAIL"]?>">
            </div>		
            <div class="form__item">
              <label class="form__label" for="fio">Фамилия Имя Отчество</label>
              <input class="form__input-text" type="text" name="FIO" value="<?=$arResult["arUser"]["LAST_NAME"]?> <?=$arResult["arUser"]["NAME"]?> <?=$arResult["arUser"]["SECOND_NAME"]?>" id="fio">
            </div>
            <div class="form__item">
              <label class="form__label" for="phone">Мобильный телефон</label>
              <input class="form__input-text" type="tel" name="PERSONAL_MOBILE" value="<?=$arResult["arUser"]["PERSONAL_MOBILE"]?>" id="phone">
              <p class="form__note form__note--text">Требуется для модерации</p>
            </div>
			
            <div class="form__item">
              <label class="form__label" for="gender">Пол</label>
              <select class="form__select" name="PERSONAL_GENDER" id="gender">
                <option value=""></option>
                <option value="M" <? if ($arResult["arUser"]["PERSONAL_GENDER"] == "M") echo "selected" ?>>
					<?=GetMessage("USER_MALE")?>
				</option>
				<option value="F" <? if ($arResult["arUser"]["PERSONAL_GENDER"] == "F") echo "selected"?>>
					<?=GetMessage("USER_FEMALE")?>
				</option>
              </select>
            </div>		
            <div class="form__item form__item--last">
              <label class="form__label" for="birthday">Дата рождения</label>
              <input class="form__input-text form__input-text--date" type="text" name="PERSONAL_BIRTHDAY" id="birthday" value=<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>>
            </div>
          </fieldset>
          <fieldset class="form__fieldset form__fieldset--chain">
            <legend class="form__legend form__legend--chain">Торговая точка</legend>
            <div class="form__item form__item--file">
              <div class="form__img avatar">
				<input class="form__file-img visually-hidden" accept="image/*" type="file" name="WORK_LOGO" id="work-picture">
                <img src="<?=!empty($arResult["arUser"]["WORK_LOGO"]) ? $arResult["arUser"]["WORK_LOGO"] : "/upload/vigo/no_foto.jpg"?>" alt="<?=$USER->GetFullName();?>">
              </div>
              <div class="form__file-wrap">
                <label class="form__file-btn" for="work-picture">Загрузить фото с торговой точки</label>
                <p class="form__note">Ваше фото в фирменной одежде на торговой точке<br>
                Не более 2000х2000px, 2Мб</p>
              </div>
            </div>
            <div class="form__item">
              <label class="form__label" for="company-city">Город</label>			  
              <select class="form__select" name="WORK_CITY" id="company-city">
                <option value="0"></option>
                <? foreach ( $arResult['CITYES']['ITEMS'] as $key => $item ) { ?>				
					<option value="<?=$key?>" <? if ($arResult['CITYES']['ID'] == $key) 
					{ echo "selected='selected'"; } ?>>
						<?=$item?>
					</option>				
				<? } ?>		
              </select>
            </div>
            <div class="form__item">
              <label class="form__label" for="chain">Торговая сеть</label>
              <select class="form__select" name="WORK_COMPANY" id="chain">
                <option value="0"></option> 
				<? foreach ( $arResult['WORK_COMPANY']['ITEMS'] as $key => $item ) { ?>				
					<option value="<?=$key?>" <? if ($arResult['WORK_COMPANY']['ID'] == $key) 
					{ echo "selected='selected'"; } ?>>
						<?=$item?>
					</option>				
				<? } ?>
              </select>
            </div>
            <div class="form__item form__item--last">
              <label class="form__label" for="company-addres">Адрес торговой точки</label>
			  <select class="form__select" name="WORK_STREET" id="chain">
                <option value="0"></option>
				<? foreach ( $arResult['WORK_STREET']['ITEMS'] as $key => $item ) { ?>				
					<option value="<?=$key?>" <? if ($arResult['WORK_STREET']['ID'] == $key) 
					{ echo "selected='selected'"; } ?>>
						<?=$item?>
					</option>				
				<? } ?>                
              </select>
              <p class="form__note form__note--text">Не нашли вашей компании? Напишите нам на <a href="mailto:club@vigo.ru">club@vigo.ru</a></p>
            </div>
          </fieldset>
          <fieldset class="form__fieldset form__fieldset--bonus">		  
            <legend class="form__legend form__legend--bonus">Стартовый бонус</legend>
			<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) { ?>		
			
				<div class="form__item">		
					<label class="form__label"><?=$arUserField["EDIT_FORM_LABEL"]?></label>
					<input class="form__input-text" type="text" name="<?=$FIELD_NAME?>" value="<?=$arUserField['VALUE']?>">
					<? if ($FIELD_NAME == "UF_NUM_SCRATCH_CARD") { ?>
						<p class="form__note form__note--text">Минимум 6 символов</p>
					<? } ?>
				</div>
				
			<? } ?>
          </fieldset>
          <fieldset class="form__fieldset form__fieldset--last">
            <legend class="form__legend form__legend--password">Изменить пароль</legend>
            <div class="form__item">
              <label class="form__label" for="password">Старый пароль</label>
              <input class="form__input-text" type="password" name="PASSWORD" id="password">
            </div>
            <div class="form__item">
              <label class="form__label" for="confirm-password">Новый пароль</label>
              <input class="form__input-text" type="password" name="NEW_PASSWORD" id="confirm-password">
            </div>
          </fieldset>
          <button class="btn form__btn" type="submit" name="form-submit" id="update_user">Сохранить</button>
        </form>
      </div>
    </section>	 
  
	
	<script>	
		BX.WORK_COMPANY = '<?=$arResult["arUser"]["WORK_COMPANY"]?>';
		BX.WORK_STREET  = '<?=$arResult["arUser"]["WORK_STREET"]?>';
		BX.ID_ISER      = '<?=$arResult["arUser"]["ID"]?>';

		BX.message({
			TEMPLATE_PATH: '<?=$this->GetFolder()?>'
		});
	</script>