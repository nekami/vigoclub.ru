<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// город
$arFilter = ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ACTIVE' => 'Y'];
	
$rsSections = CIBlockSection::GetList(['NAME' => 'ASC'], $arFilter, false, ['ID', 'NAME'], false);
	
$arResult['CITYES'] = [];
	
while ($arSection = $rsSections->Fetch())
{
	$arResult['CITYES']['ITEMS'][$arSection['ID']] = $arSection['NAME'];
	
	if ($arResult["arUser"]["WORK_CITY"] == $arSection['NAME'])
		$arResult['CITYES']['ID'] = $arSection['ID'];
}

///////////////////////////////////////////////////////////////////////////////////////////

// компания
$arResult['WORK_COMPANY'] = [];

$arFilter = ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $arResult['CITYES']['ID'], 'ACTIVE'=>'Y']; 
		 
$res = CIBlockElement::GetList(['NAME' => 'ASC'], $arFilter, false, false, ['ID', 'NAME']);   

while ($ar_fields = $res->Fetch()) {

	$arResult['WORK_COMPANY']['ITEMS'][$ar_fields['ID']] = $ar_fields['NAME'];
	  
	if ($arResult["arUser"]["WORK_COMPANY"] == $ar_fields['NAME'])
		$arResult['WORK_COMPANY']['ID'] = $ar_fields['ID']; 	  
}

///////////////////////////////////////////////////////////////////////////////////////////

// адрес
$arResult['WORK_STREET'] = [];
	
$rsElements = CIBlockElement::GetList(["SORT"=>"PROPERTY_ADDRESSES"], ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ACTIVE' => 'Y', 'ID' => $arResult['WORK_COMPANY']['ID']], false, false, ['ID', 'PROPERTY_ADDRESSES']);
		
while ($arElement = $rsElements->Fetch())
{
	$arResult['WORK_STREET']['ITEMS'][$arElement['PROPERTY_ADDRESSES_VALUE_ID']] = $arElement['PROPERTY_ADDRESSES_VALUE'];
	
	if ($arResult["arUser"]["WORK_STREET"] == $arElement['PROPERTY_ADDRESSES_VALUE'])
		$arResult['WORK_STREET']['ID'] = $arElement['PROPERTY_ADDRESSES_VALUE_ID']; 
}

$this->__component->arResultCacheKeys = array('CITYES', 'WORK_COMPANY', 'WORK_STREET');


$arResult["arUser"]["PERSONAL_PHOTO"] = CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]);

$arResult["arUser"]["WORK_LOGO"]      = CFile::GetPath($arResult["arUser"]["WORK_LOGO"]);
?>