<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<?  
   CModule::IncludeModule('main');
   CModule::IncludeModule('iblock');   
   $user = new CUser;
   $ID_ISER = trim($_POST['ID_ISER']);
   $str = ""; // строка для всех оповещений ?>

<? if ( !empty($_POST['UF_PROMO_CODE']) && !empty($_POST['UF_NUM_SCRATCH_CARD'])  && !empty($_POST['UF_SCRATCH_CARD']) ) 
  {	  
	$PROMO_CODE           = trim($_POST['UF_PROMO_CODE']);
	$NUM_SCRATCH_CARD     = trim($_POST['UF_NUM_SCRATCH_CARD']);	
	$SCRATCH_CARD         = trim($_POST['UF_SCRATCH_CARD']);	
	
	// проверка на начичие карты	
	$arFilter = [ 'IBLOCK_ID' => CARDS_IBLOCK_ID, 'ACTIVE'=>'Y', 'PROPERTY_USER' => $ID_ISER ];		 
	$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID']); 	
	while ($ar_fields = $res->Fetch()) {
		if(!empty($ar_fields['ID'])) {
			ClearCart($ID_ISER, $ar_fields['ID']);
			break;
		}
	}
	
	$arFilter = ['IBLOCK_ID' => CARDS_IBLOCK_ID, 'ACTIVE'=>'Y', 'NAME' => $NUM_SCRATCH_CARD, 'PROPERTY_CODE_SC' => $SCRATCH_CARD]; 
			 
	$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID', 'PROPERTY_USER', 'PROPERTY_DATE_TIME']);   

	if ($ar_fields = $res->Fetch()) 
	{
		if ( empty($ar_fields['PROPERTY_USER_VALUE']) && empty($ar_fields['PROPERTY_DATE_TIME_VALUE']) )
		{
			// закрепляем карту за пользователем в ИБ Карты
			CIBlockElement::SetPropertyValuesEx($ar_fields['ID'], false, ["USER" => $ID_ISER, "DATE_TIME" => date('d.m.Y H:i:s')]);
				
			// вносим данные о карте в карточку пользователя
			$user->Update($ID_ISER, ["UF_PROMO_CODE" => $PROMO_CODE, "UF_NUM_SCRATCH_CARD" => $NUM_SCRATCH_CARD, "UF_SCRATCH_CARD" => $SCRATCH_CARD]);
			$str .= "Карта успешно активирована!<br/>";
		}
		else 
		{
			$str .= "Активмрования карты невозможно. За ней закреплен другой пользователь.<br/>";
		}
	}
	
  } else 
  {
	ClearCart($ID_ISER);
  }
?>


<? if ( !empty($_POST['EMAIL']) || !empty($_POST['FIO']) || !empty($_POST['PERSONAL_MOBILE'])|| !empty($_POST['WORK_CITY']) || !empty($_POST['WORK_COMPANY']) || !empty($_POST['WORK_STREET']) || !empty($_POST['NEW_PASSWORD']) || !empty($_POST['PERSONAL_GENDER']) || !empty($_POST['PERSONAL_BIRTHDAY']) ) 
{
	
	$EMAIL                = trim($_POST['EMAIL']);	
	$NEW_PASSWORD         = trim($_POST['NEW_PASSWORD']);	
	$FIO                  = explode(" ", trim($_POST['FIO']));	
	$PHONE                = trim($_POST['PERSONAL_MOBILE']);
	$PERSONAL_GENDER      = trim($_POST['PERSONAL_GENDER']);
	$COMPANY_CITY         = trim($_POST['WORK_CITY']);
	$COMPANY_NAME         = trim($_POST['WORK_COMPANY']);	
	$COMPANY_ADDRESS      = trim($_POST['WORK_STREET']);	
	$BIRTHDAY             = trim($_POST['PERSONAL_BIRTHDAY']);
	
	// по id получаем значение
    GetCityTtAddress($COMPANY_CITY, $COMPANY_NAME, $COMPANY_ADDRESS);
	
	$arFields = Array(
	   "EMAIL"               => !empty($EMAIL) ? $EMAIL : "",
	   "LOGIN"               => !empty($EMAIL) ? $EMAIL : "",	   
	   "LAST_NAME"           => !empty($FIO[0]) ? $FIO[0] : "",
	   "NAME"                => !empty($FIO[1]) ? $FIO[1] : "",      
	   "SECOND_NAME"         => !empty($FIO[2]) ? $FIO[2] : "",
	   "PERSONAL_BIRTHDAY"   => !empty($BIRTHDAY) ? $BIRTHDAY : "",
	   "PERSONAL_MOBILE"     => !empty($PHONE) ? $PHONE : "",
	   "PERSONAL_GENDER"     => !empty($PERSONAL_GENDER) ? $PERSONAL_GENDER : "",
	   "WORK_CITY"           => !empty($COMPANY_CITY) ? $COMPANY_CITY : "",
	   "WORK_COMPANY"        => !empty($COMPANY_NAME) ? $COMPANY_NAME : "",
	   "WORK_STREET"         => !empty($COMPANY_ADDRESS) ? $COMPANY_ADDRESS : "",	  
	   "PERSONAL_PHOTO"      => !empty($_FILES["PERSONAL_PHOTO"]) ? $_FILES["PERSONAL_PHOTO"] : CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/vigo/no_foto.jpg"),
	   "WORK_LOGO"           => !empty($_FILES["WORK_LOGO"]) ? $_FILES["WORK_LOGO"] : CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/vigo/no_foto.jpg"),
	);
			
	if (!empty($NEW_PASSWORD)) { $arFields["PASSWORD"] = $NEW_PASSWORD; }
	
	$res = $user->Update($ID_ISER, $arFields);
	
	if (!empty($res)) $str .= "Персональные данные успешно обновлены.<br/>";
}	

echo $str;

?>