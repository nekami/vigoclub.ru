<?
$MESS['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS['LAST_UPDATE'] = "Дата обновления:";
$MESS['ACTIVE'] = "Активен:";
$MESS['EMAIL'] = "Email";
$MESS['MAIN_RESET'] = "Отмена";
$MESS['LOGIN'] = "Логин";
$MESS['NEW_PASSWORD'] = "Новый пароль (мин. 6 символов):";
$MESS['NEW_PASSWORD_CONFIRM'] = "Подтверждение нового пароля:";
$MESS['SAVE'] = "Сохранить изменения";
$MESS['RESET'] = "Сбросить";
$MESS['LAST_LOGIN'] = "Последняя авторизация:";
$MESS['NEW_PASSWORD_REQ'] = "Новый пароль:";
$MESS['USER_GENDER'] = "Пол";
$MESS['USER_FEMALE'] = "Женский";
$MESS['USER_MALE'] = "Мужской";
$MESS['USER_BIRTHDAY_DT'] = "Дата рождения";
$MESS['FIO'] = "Фамилия Имя Отчество";
$MESS['USER_MOBILE'] = "Мобильный телефон";
$MESS['USER_CITY'] = "Город";
$MESS['TRADING_NETWORK'] = "Торговая сеть";
$MESS['ADDRESS'] = "Адрес торговой точки";
$MESS['REQUIRED_MODERATION'] = "Требуется для модерации";



