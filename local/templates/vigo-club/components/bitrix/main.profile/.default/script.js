$(function() 
{
	$('#update_user').click(function(event)
	{
		    event.preventDefault();	

			formData = new FormData($('form.profile-form__form').get(0)); 
						
			$.ajax({
				  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
				  data: formData,
				  contentType: false,
				  processData: false,
				  method: 'POST',
				  type: 'POST',
				  dataType: 'html',
				  success: function(msg)
				  {	
				  
					$('.alert .body_modal p.msg').html(msg);					
					ShowPopupWindow("success", "alert");		 
					
				  },         
				  error:  function(data){
					console.log('Ошибка ajax-скрипта');
				  }
			});
	});
	
}); 