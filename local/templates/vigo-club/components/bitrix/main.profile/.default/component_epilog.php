<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->getTemplate()->addExternalCss(SITE_TEMPLATE_PATH."/css/jquery.datetimepicker.min.css"); ?>
<? $this->getTemplate()->addExternalJs(SITE_TEMPLATE_PATH."/js/jquery.datetimepicker.full.min.js");?>
<? $this->getTemplate()->addExternalJs('/ajax/scripts/select_address.js'); ?>
<? $this->getTemplate()->addExternalJs(SITE_TEMPLATE_PATH.'/js/files.js'); ?>
<? $this->getTemplate()->addExternalJs(SITE_TEMPLATE_PATH.'/js/rating.js'); ?>
<? $this->getTemplate()->addExternalJs(SITE_TEMPLATE_PATH.'/js/modal.js'); ?>

<script>
	$(function() 
	{	
		var count    = -4;
		var count_val = 0;
		var percent   = 0;
	
		$("form.profile-form__form input").each(function(index, value){
			++count;			
			if ($(value).val()) ++count_val;
		});
		$("form.profile-form__form select option:selected").each(function(index, value){
			++count;			
			if ($(value).val()) ++count_val;
		});		
		percent = (count_val * 100) / count;
		percent = percent.toFixed();
		$(".balance__persent-text").html(percent+'%');
		
		BX.setCookie('percent', percent);
	});
</script>