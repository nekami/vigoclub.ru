<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

    <section class="section events events--inner inner-top">
      <div class="container">
        <h1 class="section__title events__main-title"><?=GetMessage("NEWS_TITLE")?></h1>
		
		<? if (!empty($arResult["ITEMS"])) { ?>		
			<ul class="events__inner-list list">
			
				<? foreach($arResult["ITEMS"] as $arItem) { ?>
				
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_DELETE_CONFIRM')));
				?>
					
				  <li class="events__inner-item">
					<div class="events__item-container">
					  <h3 class="events__title"><?=$arItem['NAME']?></h3>
					  <p class="events__text"><?=$arItem['PREVIEW_TEXT']?></p>
					  <p class="events__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></p>
					</div>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="<?=$arItem['NAME']?>" class="events__link">
						<?=GetMessage("NEWS_DETAIIL")?>
					</a>
				  </li>
				  
				<? } ?> 
			  
			</ul>			
		<? } ?>		
      </div>
    </section>
	
	<?=$arResult["NAV_STRING"]?>