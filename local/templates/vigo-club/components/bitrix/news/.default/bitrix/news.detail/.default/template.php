<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="section news">
	<div class="container">
        <h1 class="section__title news__title"><?=$arResult["NAME"]?></h1>
        <p class="news__date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></p>
        <div class="news__text content">
          <? if (!empty($arResult["DETAIL_TEXT"]))		
                 echo $arResult["DETAIL_TEXT"];
		    else echo $arResult["PREVIEW_TEXT"];?>
		</div>
    </div>
</section>
   
