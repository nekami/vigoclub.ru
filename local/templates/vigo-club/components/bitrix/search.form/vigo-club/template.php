<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>

<div class="header__search">
    <form class="header__search-form" action="<?=$arResult["FORM_ACTION"]?>">
		<?$APPLICATION->IncludeComponent(
			"bitrix:search.suggest.input",
			"",
			array(
				"NAME" => "q",
				"VALUE" => "",
				"INPUT_SIZE" => 15,
				"DROPDOWN_SIZE" => 10,
			),
			$component, array("HIDE_ICONS" => "Y")
		);?>		  
        <button class="header__search-btn header__search-btn--search" type="submit" aria-label="Искать"></button>
    </form>			
    <button class="header__search-btn header__search-btn--open" type="button" name="button" aria-label="Открыть поиск"></button>			
</div>
<a class="header__nav-link header__bottom-enter" href="">Войти</a>