$(function() 
{
	let openSearch = document.querySelector('.header__search-btn--open');
	let searchForm = document.querySelector('.header__search-form');
	let searchWrap = document.querySelector('.header__search');
	let searchInput = document.querySelector('.header__search-input');
	openSearch.addEventListener('click', function() {
	  searchForm.classList.add('header__search-form--opened');
	  searchInput.focus();
	});

	document.addEventListener('click', function(event) {
	  var isClickInside = searchWrap.contains(event.target);
	  if (!isClickInside) {
		searchForm.classList.remove('header__search-form--opened');
	  }
	});
});