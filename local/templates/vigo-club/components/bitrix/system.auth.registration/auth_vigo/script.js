$(function() 
{		 
	ClearErrorText();
	
	// регистрация пользователя
	$('#reg_user').click(function(event)
	{
		event.preventDefault();	
		
		if ($('input[name="AGREE"]').val() == "N") 
		{ 
			$('input[name="AGREE"]').addClass("errortext").next('div.error_text').show();			
		}
		else
		{			
			var login            = $('input[name="EMAIL"]').val();		
			var pass             = $('input[name="PASSWORD"]').val();			
			var fio              = $('input[name="FIO"]').val();
			var phone            = $('input[name="PERSONAL_MOBILE"]').val();
			var city             = $('select[name="WORK_CITY"] option:selected').text();
			var company          = $('select[name="WORK_COMPANY"] option:selected').text();	
			var company_address  = $('select[name="WORK_STREET"] option:selected').text();
			var promo_code       = $('input[name="UF_PROMO_CODE"]').val();
			var num_scratch_card = $('input[name="UF_NUM_SCRATCH_CARD"]').val();	
			var scratch_card     = $('input[name="UF_SCRATCH_CARD"]').val();
			var sessid           = $('input[name="sessid"]').val();	

			if (!login || !pass || !fio || !phone || !city || !company || !company_address)
			{				
				if (!login)
				{ 
					$('input[name="EMAIL"]').addClass("errortext").next('div.error_text').show();				
				}
				
				if (!pass) 
				{ 
					$('input[name="PASSWORD"]').addClass("errortext").next('div.error_text').show();            
				}
				
				if (!fio)              
				{ 
					$('input[name="FIO"]').addClass("errortext").next('div.error_text').show();                
				}
				
				if (!phone) 
				{ 
					$('input[name="PERSONAL_MOBILE"]').addClass("errortext").next('div.error_text').show();      
				}
				
				if ($('select[name="WORK_CITY"] option:selected').val() == 0) 
				{ 
					$('select[name="WORK_CITY"]').addClass("errortext").next('div.error_text').show();          
				}
				
				if ($('select[name="WORK_COMPANY"] option:selected').val() == 0)          
				{ 
					$('select[name="WORK_COMPANY"]').addClass("errortext").next('div.error_text').show();      
				}
				
				if ($('select[name="WORK_STREET"] option:selected').val() == 0)  
				{ 
					$('select[name="WORK_STREET"]').addClass("errortext").next('div.error_text').show();       
				}			    
										
				ScrollTopErrorText();				
			} 
			else 
			{			
				$.ajax({
				  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
				  data: { 
				    'EMAIL': login, 
				    'PASSWORD': pass, 
					'FIO': fio,
					'PERSONAL_MOBILE': phone,					
					'WORK_CITY': city, 
					'WORK_COMPANY': company,
					'WORK_STREET' : company_address,					
					'UF_PROMO_CODE': promo_code,
					'UF_NUM_SCRATCH_CARD': num_scratch_card,
					'UF_SCRATCH_CARD': scratch_card,					
					'sessid': sessid
				  },
				  method: 'POST',
				  type: 'POST',
				  dataType: 'json',
				  success: function(data)
				  {	
					if (data.error) { 
						$('.alert .body_modal p.msg').html(data.error);
						ShowPopupWindow("error", "alert");
					}					
					
					if (data.success) { 
						$(location).attr('href', '/login/?register=yes'); 
					}				
					
				  },         
				  error:  function(data){
					console.log('Ошибка ajax-скрипта');
				  }
				});
			}
		}	
	});	
	
	$(document).on('click', 'input[name="AGREE"]', function() 
	{
		if ($(this).val() == "N") $(this).val("Y");
		else $(this).val("N");
	});		
	
}); 