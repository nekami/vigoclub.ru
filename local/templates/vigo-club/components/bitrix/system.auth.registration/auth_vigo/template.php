<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

	<? $APPLICATION->IncludeFile(
		"/login/intro_register.php",
		Array(),
		Array("MODE"=>"html")
	); ?>	
	
    <section class="section register">
      <div class="container">	  
        <h2 class="section__title register__title"><?=GetMessage("JOIN_VIRGOCLUB")?></h2>
		
        <form class="register__form form" action="" method="post">
		
		  <?//=bitrix_sessid_post()?>
		
          <!--  личные данные -->

		  <div class="form__item form__item--last">
			   <label class="form__label">Email</label>				   														
			   <input class="form__input-text mail-input" type="text" name="EMAIL" value="">
			   <div class="error_text">Поле обязательно для заполнения</div>
		  </div>
		  
		  
		  <div class="form__item form__item--last">
			   <label class="form__label" for="password">Пароль</label>			   
			   <p class="form__note">Минимум 6 символов</p>						
			   <input class="form__input-text" type="password" name="PASSWORD" value="">
			   <div class = "error_text">Поле обязательно для заполнения</div>
		  </div>
		  


		  <div class="form__item form__item--last">
            <label class="form__label">ФИО</label>
            <input class="form__input-text" type="text" name="FIO" value="">
			<div class = "error_text">Поле обязательно для заполнения</div>			
          </div>
		  
				  
		  <div class="form__item form__item--last">
			   <label class="form__label" for="text">Мобильный телефон</label>			   
			   <p class="form__note">Требуется для модерации</p>			   
			   <input class="form__input-text phone-input" type="text" name="PERSONAL_MOBILE" value="">
			   <div class = "error_text">Поле обязательно для заполнения</div>
		  </div>
		  
		  <!--  работа -->		  
		   
		  <div class="form__item">
             <label class="form__label" for="company-city">Город</label>
             <select class="form__select" name="WORK_CITY" id="company-city">
				<option value="0"></option>
				<? foreach ($arResult['CITYES'] as $key => $item) { ?>				
					<option value="<?=$key?>"><?=$item?></option>			
				<? } ?>
             </select>
			 <div class = "error_text">Поле обязательно для заполнения</div>
          </div>		  
		  
		  <div class="form__item">
             <label class="form__label" for="outlet">Торговая сеть</label>
             <select class="form__select" name="WORK_COMPANY" id="outlet">
				<option value="0"></option>			 
             </select>
			 <div class = "error_text">Поле обязательно для заполнения</div>
          </div>
		   
		  <div class="form__item">
             <label class="form__label" for="outlet_address">Адрес торговой точки</label>			 
			 <p class="form__note">Не нашли вашей компании?<br>Напишите нам на club@vigo.ru</p>
             <select class="form__select" name="WORK_STREET" id="outlet_address">
				<option value="0"></option>
             </select>
			 <div class = "error_text">Поле обязательно для заполнения</div>
          </div>
		   
		   <!--  пользовательские поля --> 

		  <fieldset class="form__fieldset form__fieldset--bonus">
			<legend class="form__legend form__legend--bonus">Стартовый бонус</legend>		   
					
					  
			<div class="form__item">		
				<label class="form__label">Код промоутера</label>
				<input class="form__input-text" type="text" name="UF_PROMO_CODE" value="">
			</div>	
					  
			<div class="form__item">		
				<label class="form__label">Номер скретч-карты</label>
				<input class="form__input-text" type="text" name="UF_NUM_SCRATCH_CARD" value="">
				<p class="form__note form__note--text">Минимум 6 символов</p>
			</div>	
					  
			<div class="form__item">		
				<label class="form__label">Код скретч-карты</label>
				<input class="form__input-text" type="text" name="UF_SCRATCH_CARD" value="">
			</div>  
		  </fieldset>
		
		<div class="form__item form__item--checkbox form__item--bottom">
            <input class="form__input-checkbox" type="checkbox" name="AGREE" id="agree" value="N" required="">
			<div class = "error_text">Вы не приняли условия и правила мотивационной программы</div>
            <label class="form__label-checkbox" for="agree">Я принимаю <a href="/o-programme/pravila-uchastiya/">условия и правила мотивационной программы</a></label>
            <p class="form__note">Обязательное условие</p>
        </div>
		
		  
        <button class="btn form__btn" type="submit" id="reg_user" name="form-submit"><?=GetMessage("REGISTER")?></button>
		  
	</form>
	
	
    <p class="register__enter"><?=GetMessage("ALREADY_REGISTERED")?> <a href="/login/" title="<?=GetMessage("AUTH_AUTH")?>"><?=GetMessage("AUTH_AUTHORIZE")?><?=GetMessage("ENTER")?></a></p>
  </div>
</section>
	
<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});	
</script>