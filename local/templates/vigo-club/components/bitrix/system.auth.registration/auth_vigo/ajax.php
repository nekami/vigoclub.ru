<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
CModule::IncludeModule('main');
CModule::IncludeModule('iblock');
//global $USER;
//if (!is_object($USER)) $USER = new CUser;  
?>

<? if ( !empty($_POST['EMAIL']) && !empty($_POST['PASSWORD']) && !empty($_POST['FIO'])  && !empty($_POST['PERSONAL_MOBILE'])  
  && !empty($_POST['WORK_CITY']) && !empty($_POST['WORK_COMPANY'])  && !empty($_POST['WORK_STREET']) ) 
  {	
	$LOGIN           = trim($_POST['EMAIL']);	
	$PASSWORD        = trim($_POST['PASSWORD']);
	$FIO             = explode(" ", trim($_POST['FIO']));
	$PHONE           = trim($_POST['PERSONAL_MOBILE']);		
	$COMPANY_CITY    = trim($_POST['WORK_CITY']);
	$COMPANY_NAME    = trim($_POST['WORK_COMPANY']);	
	$COMPANY_ADDRESS = trim($_POST['WORK_STREET']);			
		
	// проверка на наличие такого же логина или телефона	
	$obUser = \Bitrix\Main\UserTable::getList(array(
		'select' => ['ID'],
		'filter' => ['ACTIVE' => 'Y', ['LOGIC' => 'OR','LOGIN' => $LOGIN, 'PERSONAL_MOBILE' => $PHONE, 'EMAIL' => $LOGIN]]
	));
	if ($arUser = $obUser->fetch()) 
	{
		if (!empty($arUser["ID"])) { echo json_encode(['error' => 'Такой пользователь уже существует']); die; }
	}		
	
	// добавление пользователя
	$user = new CUser;	
	
	$arFields = Array(
	  "ACTIVE"              => "Y",	
	  "EMAIL"               => $LOGIN,
	  "LOGIN"               => $LOGIN,  	  
	  "PASSWORD"            => $PASSWORD,
	  "CONFIRM_PASSWORD"    => $PASSWORD,
	  "LAST_NAME"           => $FIO[0],
	  "NAME"                => $FIO[1],      
	  "SECOND_NAME"         => $FIO[2],
	  "PERSONAL_MOBILE"     => $PHONE,
	  "PERSONAL_PHOTO"      => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/vigo/no_foto.jpg"),	  
	  "WORK_CITY"           => $COMPANY_CITY,
	  "WORK_COMPANY"        => $COMPANY_NAME,
	  "WORK_STREET"         => $COMPANY_ADDRESS,
      "WORK_LOGO"           => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']."/upload/vigo/no_foto.jpg") 
	);	
	$IdUser = $user->Add($arFields);
	
	if (!empty($IdUser)) 
	{		
		if ( !empty($_POST['UF_PROMO_CODE']) && !empty($_POST['UF_NUM_SCRATCH_CARD'])  && !empty($_POST['UF_SCRATCH_CARD']) ) 
		{	  
			$PROMO_CODE           = trim($_POST['UF_PROMO_CODE']);
			$NUM_SCRATCH_CARD     = trim($_POST['UF_NUM_SCRATCH_CARD']);	
			$SCRATCH_CARD         = trim($_POST['UF_SCRATCH_CARD']);	
				
			$arFilter = ['IBLOCK_ID' => CARDS_IBLOCK_ID, 'ACTIVE'=>'Y', 'NAME' => $NUM_SCRATCH_CARD, 'PROPERTY_CODE_SC' => $SCRATCH_CARD]; 					 
			$res = CIBlockElement::GetList([], $arFilter, false, false, ['ID', 'PROPERTY_USER', 'PROPERTY_DATE_TIME']);   

			if ($ar_fields = $res->Fetch()) 
			{
				if ( empty($ar_fields['PROPERTY_USER_VALUE']) && empty($ar_fields['PROPERTY_DATE_TIME_VALUE']) )
				{
					// закрепляем карту за пользователем в ИБ Карты
					CIBlockElement::SetPropertyValuesEx($ar_fields['ID'], CARDS_IBLOCK_ID, ["USER" => $IdUser, "DATE_TIME" => date('d.m.Y H:i:s')]);
						
					// вносим данные о карте в карточку пользователя
					$user->Update($IdUser, ["UF_PROMO_CODE" => $PROMO_CODE, "UF_NUM_SCRATCH_CARD" => $NUM_SCRATCH_CARD, "UF_SCRATCH_CARD" => $SCRATCH_CARD]);
				}
				else 
				{
					echo json_encode(['error' => 'Активирование карты невозможно. За ней закреплен другой пользователь.']); die;
				}			
			}			
		}
		
		// добавление директории юзера для отслежтвания его продаж		
		$bs = new CIBlockSection;		
		$arFields = Array(
		  "ACTIVE" => "Y",		  
		  "IBLOCK_ID" => PRIZES_IBLOCK_ID,
		  "NAME" => $FIO[0].' '.$FIO[1].' '.$FIO[2],		  
		  "UF_USER_ID" => $IdUser		  
		);		
		$IdSection = $bs->Add($arFields);
	}	
	
	// авторизация пользователя		
	$arAuthResult = $user->Login($LOGIN, $PASSWORD, "Y");
	$APPLICATION->arAuthResult = $arAuthResult;	
		
	echo json_encode(['success' => $APPLICATION->arAuthResult]);	
}	
?>