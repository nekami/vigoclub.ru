<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
CModule::IncludeModule('iblock');

// город

$arFilter = ['IBLOCK_ID' => TOGRE_IBLOCK_ID, 'ACTIVE' => 'Y'];
	
$rsSections = CIBlockSection::GetList(['NAME' => 'ASC'], $arFilter, false, ['ID', 'NAME'], false);
	
$arResult['CITYES'] = [];
	
while ($arSection = $rsSections->Fetch())
{
	$arResult['CITYES'][$arSection['ID']] = $arSection['NAME'];
}

$this->__component->arResultCacheKeys = array('CITYES');
?>