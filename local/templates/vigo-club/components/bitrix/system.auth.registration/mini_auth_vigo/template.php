<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

		
        <form class="steps__form form" action="" method="post" id="write_to_us">
		  
		  <input type="hidden" name="TYPE" value="REGISTRATION" />
		
          <div class="form__item">
            <label class="form__label" for="email">Email</label>
            <input class="form__input-text" type="email" name="EMAIL" id="email" value="">
			<div class = "error_text">Поле обязательно для заполнения</div>
          </div>
          <div class="form__item">
            <label class="form__label" for="name">ФИО</label>
            <input class="form__input-text" type="text" name="FIO" id="name" value="">
			<div class = "error_text">Поле обязательно для заполнения</div>
          </div>
          <div class="form__item form__item--last">
            <label class="form__label" for="password">Пароль</label>
            <input class="form__input-text" type="password" name="PASSWORD" id="password" value="">
			<div class = "error_text">Поле обязательно для заполнения</div>
          </div>
          <div class="form__item form__item--checkbox form__item--bottom">
            <input class="form__input-checkbox" type="checkbox" name="AGREE" id="agree" value="N">
			<div class = "error_text">Вы не приняли условия и правила мотивационной программы</div>
            <label class="form__label-checkbox" for="agree">Я принимаю <a href="/o-programme/pravila-uchastiya/" target="__blank" title="Условия и правила мотивационной программы">условия и правила мотивационной программы</a></label>
            <p class="form__note">Обязательное условие</p>
          </div>
          <button class="btn form__btn form__submit" type="submit" name="form-submit" id="reg_user">Зарегистрироваться</button>
        </form>
        <p class="steps__enter">Уже зарегистрированы? <a href="/login/?login=yes" title="Авторизация">Войти</a></p>
		
		<script>
			BX.message({
				TEMPLATE_PATH: '<?=$this->GetFolder()?>'
			});	
		</script>