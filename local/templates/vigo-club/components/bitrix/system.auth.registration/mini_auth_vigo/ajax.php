<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<? if ( !empty($_POST['EMAIL']) && !empty($_POST['PASSWORD']) && !empty($_POST['FIO']) ) 
  {	
	$LOGIN           = trim($_POST['EMAIL']);	
	$PASSWORD        = trim($_POST['PASSWORD']);
	$FIO             = explode(" ", trim($_POST['FIO']));
	
	CModule::IncludeModule('main');
	
	// проверка логина и телефона	
	$obResult = \Bitrix\Main\UserTable::getList(array(
		'select' => ['ID'],
		'filter' => ['ACTIVE' => 'Y', 		
						[
						  'LOGIC' => 'OR',
						  'LOGIN' => $LOGIN,
						  'EMAIL' => $LOGIN
						]
					]
	));

	if ($arUser = $obResult->fetch()) 
	{
		if (!empty($arUser["ID"])) { echo json_encode(['login' => 'Такой пользователь уже существует']); die; }
	}
		
	
	// добавление пользователя
	$user = new CUser;	
	
	$arFields = Array(
	  "ACTIVE"              => "Y",	
	  "EMAIL"               => $LOGIN,
	  "LOGIN"               => $LOGIN,  	  
	  "PASSWORD"            => $PASSWORD,
	  "CONFIRM_PASSWORD"    => $PASSWORD,
	  "LAST_NAME"           => $FIO[0],
	  "NAME"                => $FIO[1],      
	  "SECOND_NAME"         => $FIO[2]	  
	);
	
	$IdUser = $user->Add($arFields);	
	
	if (!empty($IdUser)) 
	{
		$bs = new CIBlockSection;
		
		$NameDir = $LOGIN;
		if ( !empty($FIO[0]) && !empty($FIO[1]) ) $NameDir = $FIO[0].' '.$FIO[1].' '.$FIO[3];
		
		$arFields = Array(
		  "ACTIVE" => "Y",		  
		  "IBLOCK_ID" => PRIZES_IBLOCK_ID,
		  "NAME" => $NameDir,		  
		  "UF_USER_ID" => $IdUser		  
		);
		
		$IdSection = $bs->Add($arFields);
	}	
	
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;		
	$arAuthResult = $USER->Login($LOGIN, $PASSWORD, "Y");
	$APPLICATION->arAuthResult = $arAuthResult;	
		
	echo json_encode(array('success' => $APPLICATION->arAuthResult));
	
}	
?>