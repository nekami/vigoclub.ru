$(function() 
{
	ClearErrorText();
	
	$(document).on('click', '#reg_user', function(event) 
	{
		event.preventDefault();	
		
		if ($('input[name="AGREE"]').val() == "N") 
		{ 
			$('input[name="AGREE"]').addClass("errortext").next('div.error_text').show();			
		}
		else
		{			
			var login = $('input[name="EMAIL"]').val();		
			var pass  = $('input[name="PASSWORD"]').val();			
			var fio   = $('input[name="FIO"]').val();		
			
			if (!login || !pass || !fio)
			{				
				if (!login)
				{ 
					$('input[name="EMAIL"]').addClass("errortext").next('div.error_text').show(); 
				}
				
				if (!pass) 
				{ 
					$('input[name="PASSWORD"]').addClass("errortext").next('div.error_text').show();            
				}
				
				if (!fio)              
				{ 
					$('input[name="FIO"]').addClass("errortext").next('div.error_text').show();                
				}

				ScrollTopErrorText();
			} 
			else 
			{
				$.ajax({
				  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
				  data: { 
				    'EMAIL': login, 
				    'PASSWORD': pass, 
					'FIO': fio,	
				  },
				  method: 'POST',
				  type: 'POST',
				  dataType: 'json',
				  success: function(data)
				  {				  
					if (data.login) 
					{ 
						$('.alert .body_modal p.msg').html('Такой пользователь уже существует!');
						ShowPopupWindow("error", "alert");
					}					
					
					if (data.success) 
					{ 
						$(location).attr('href', '/login/?register=yes'); 
					}				
					
				  },         
				  error:  function(data){
					console.log('Ошибка ajax-скрипта');
				  }
				});
			}
		}	
	});
	
	
	
	$(document).on('click', 'input[name="AGREE"]', function() 
	{
		if ($(this).val() == "N") $(this).val("Y");
		else $(this).val("N");
	});	
	
	
}); 