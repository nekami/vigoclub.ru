$(function() 
{	
	$('#changepasswd_user').click(function(event)
	{
		event.preventDefault();		
		
		var login        = $('input[name="USER_LOGIN"]').val();
		var pass         = $('input[name="USER_PASSWORD"]').val();		
		var confirm_pass = $('input[name="USER_CONFIRM_PASSWORD"]').val();		
		var sessid       =  $('input[name="sessid"]').val();

		if (!pass || !confirm_pass)
		{
			if (!pass)
			{ 
				$('input[name="USER_PASSWORD"]').addClass("errortext").next('div.error_text').show(); 
			}
					
			if (!confirm_pass) 
			{ 
				$('input[name="USER_CONFIRM_PASSWORD"]').addClass("errortext").next('div.error_text').show();            
			}			
		} 
		else 
		{		
			$.ajax({
			  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
			  data: { 'USER_PASSWORD': pass, 
					  'USER_CONFIRM_PASSWORD': confirm_pass,
					  'USER_LOGIN': login,
					  'sessid': sessid
			  },
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				 if(data.upgate)
				 {
					ShowPopupWindow('update_pass', 'transaction');
					$('#update_pass .body_modal .message').text(data.upgate);
				 }
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 