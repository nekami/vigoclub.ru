<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$APPLICATION->IncludeFile(
	"/login/intro_auth.php",
	Array(),
	Array("MODE"=>"html")
);
?>

<section class="section sign">
	<div class="container">
        <h2 class="section__title sign__title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h2>
		
        <form class="sign__form form" action="" method="post">
		
			<?=bitrix_sessid_post()?>

			<input type="hidden" name="USER_LOGIN" value="<?=$_GET['USER_LOGIN']?>" />
		
			<div class="form__item">		
				<label class="form__label"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?></label>
				<input class="form__input-text" type="password" name="USER_PASSWORD" value="<?=$arResult["USER_PASSWORD"]?>">
				<div class="error_text">Поле обязательно для заполнения</div>
			</div>
					  
			<div class="form__item form__item--last">
				<label class="form__label"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
				<input class="form__input-text" type="password" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>">
				<div class="error_text">Поле обязательно для заполнения</div>
			</div>
		  
          <div class="btn form__btn" name="form-submit" id="changepasswd_user"><?=GetMessage("AUTH_CHANGE")?></div>
        </form>
	</div>
</section>

<div id="update_pass" class="transaction">
	<div class="body_modal">
		<p class="message"><p>
	</div>
</div>

<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
</script>