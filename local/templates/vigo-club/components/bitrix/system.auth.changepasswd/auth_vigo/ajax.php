<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if(!check_bitrix_sessid()) {
	echo json_encode(array('success' => 0));
	die();
}

if ( !empty($_POST['USER_PASSWORD']) && !empty($_POST['USER_CONFIRM_PASSWORD']) ) {	
	
	CModule::IncludeModule('main');
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;	

	$LOGIN        = $_POST['USER_LOGIN'];
	$PASS         = trim($_POST['USER_PASSWORD']);	
	$CONFIRM_PASS = trim($_POST['USER_CONFIRM_PASSWORD']);
	
	if ($PASS != $CONFIRM_PASS) { echo json_encode(['upgate' => 'Пароли не совпадают. Попоробуйте еще раз.']); die; }
	
	// по логину ищем ID пользователя

	$obIdUser = \Bitrix\Main\UserTable::GetList(
	[
		'select' => ['ID'],
		'filter' => ['LOGIN' => $LOGIN]
	]);
	
	$ID_ISER = false;

	if ($IdUser = $obIdUser->fetch())
	{
		if (!empty($IdUser['ID']))
			$ID_ISER = $IdUser['ID'];		
	}
	
	if (!empty($ID_ISER))
	{
		$user = new CUser;	
	
		$arFields = ["PASSWORD" => $PASS, "CONFIRM_PASSWORD" => $CONFIRM_PASS];	
		
		$res = $user->Update($ID_ISER, $arFields);
		
		if (!empty($res)) { echo json_encode(['upgate' => 'Обновление пароля успешно завершено.']); die; }
		else { echo json_encode(['upgate' => 'При обновлении пароля произошла ощибка. Попоробуйте еще раз.']); die; }
	}
	else { echo json_encode(['upgate' => 'Пользоля с данным логином не существует.']); die; }
} 	
?>