<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>	

<section class="section pagination">
	<div class="container pagination__container">

<?//=$arResult["NavTitle"]?> 

<?if($arResult["bDescPageNumbering"] === true):?>

	<?/*=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]*/?>

	

	<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["bSavePage"]):?>
		
			<a class="pagination__btn pagination__btn--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">
				<span><?=GetMessage("nav_begin")?></span>
			</a>	
			
			<?/*<a class="pagination__btn pagination__btn--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
				<span><?=GetMessage("nav_prev")?><span>
			</a>*/?>
			
			
		<?else:?>
		
			<a class="pagination__btn pagination__btn--prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
				<span><?=GetMessage("nav_begin")?></span>
			</a>
			
			<?/*if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):?>
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
				
			<?else:?>
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_prev")?></a>
				
			<?endif*/?>
			
		<?endif?>
	<?else:?>	
		<a class="pagination__btn pagination__btn--prev">
			<span><?=GetMessage("nav_begin")?></span>
		</a>
		<?/*<span><?=GetMessage("nav_prev")?></span>*/?>
	<?endif?>

	<?while($arResult["nStartPage"] >= $arResult["nEndPage"]):?>
		<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
			<b><?=$NavRecordGroupPrint?></b>
		<?elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$NavRecordGroupPrint?></a>
		<?else:?>
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$NavRecordGroupPrint?></a>
		<?endif?>

		<?$arResult["nStartPage"]--?>
	<?endwhile?>
	

	<?if ($arResult["NavPageNomer"] > 1):?>		
		
		<?/*<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_next")?></a>*/?>
				
		<a class="pagination__btn pagination__btn--next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">
			<span><?=GetMessage("nav_end")?></span>
		</a>
		
	<?else:?>
	
		<a class="pagination__btn pagination__btn--next">
			<span><?=GetMessage("nav_end")?></span>
		</a>
	
		<?/*=GetMessage("nav_next")*/?>
	<?endif?>

<?else:?>

	<?/*=$arResult["NavFirstRecordShow"]?> <?=GetMessage("nav_to")?> <?=$arResult["NavLastRecordShow"]?> <?=GetMessage("nav_of")?> <?=$arResult["NavRecordCount"]*/?>



	<?if ($arResult["NavPageNomer"] > 1):?>

		<?if($arResult["bSavePage"]):?>
		
			<a class="pagination__btn pagination__btn--prev" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">
				<span><?=GetMessage("nav_begin")?></span>
			</a>
			
			<?/*<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>*/?>
			
		<?else:?>
		
			<a class="pagination__btn pagination__btn--prev" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
				<span><?=GetMessage("nav_begin")?></span>
			</a>
			
			<?/*if ($arResult["NavPageNomer"] > 2):?>
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><?=GetMessage("nav_prev")?></a>
			<?else:?>
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=GetMessage("nav_prev")?></a>
			<?endif*/?>
			
		<?endif?>

	<?else:?>
		<a class="pagination__btn pagination__btn--prev">
			<span><?=GetMessage("nav_begin")?></span>
		</a>
		<?/*<span><?=GetMessage("nav_prev")?></span>*/?>
	<?endif?>
	
	<ul class="pagination__link list">

		<?while($arResult["nStartPage"] <= $arResult["nEndPage"]) { ?>	

			<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]) { ?>
			
				<li class="pagination__item pagination__item--active">
				   <a><?=$arResult["nStartPage"]?></a>
				</li>
				
			<? } elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false) { ?>
				<li class="pagination__item">
					<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
				</li>			
			<?} else { ?>
				<li class="pagination__item">
					<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>">
						<?=$arResult["nStartPage"]?>
					</a>
				</li>
			<? } ?>
			
			<?$arResult["nStartPage"]++?>
			
		<? } ?>

	</ul>

	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?/*<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><?=GetMessage("nav_next")?></a>*/?>
				
		<a class="pagination__btn pagination__btn--next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">
			<span><?=GetMessage("nav_end")?></span>
		</a>
		
	<?else:?>
	
		<a class="pagination__btn pagination__btn--next">
			<span><?=GetMessage("nav_end")?></span>
		</a>	
		<?/*=GetMessage("nav_next")*/?>
	<?endif?>

<?endif?>


<?/*if ($arResult["bShowAll"]):?>
<noindex>
	<?if ($arResult["NavShowAll"]):?>
		|&nbsp;<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><?=GetMessage("nav_paged")?></a>
	<?else:?>
		|&nbsp;<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><?=GetMessage("nav_all")?></a>
	<?endif?>
</noindex>
<?endif*/?>

	</div>
</section>