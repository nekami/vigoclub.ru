$(function() 
{	
	$('button[name="send_account_info"]').click(function(event)
	{
		event.preventDefault();	
		
		var email  = $('input[name="USER_EMAIL"]').val();
		var sessid = $('input[name="sessid"]').val();

		if (!email)
		{
			$('input[name="USER_EMAIL"]').addClass("errortext").next('div.error_text').show(); 
		} 
		else 
		{		
			$.ajax({
			  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
			  data: {'EMAIL': email, 'sessid': sessid},
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				  if (data.success)
				  {
					 ShowPopupWindow('forgotpasswd', 'transaction');
				  }
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 