<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?	$APPLICATION->IncludeFile(		"/login/intro_forgot.php",		Array(),		Array("MODE"=>"html")	);?>
		
		<section class="section remind">	<div class="container">        
		<h2 class="section__title remind__title">Забыли пароль?</h2>        
		<p class="remind__message">На вашу электронную почту мы вышлем все подробности</p>        
		<form class="remind__form form" action="" method="post">		  <?=bitrix_sessid_post()?>	  		          <div class="form__item form__item--last">            
		<label class="form__label" for="email">Email</label>            
		<input class="form__input-text mail-input" type="email" name="USER_EMAIL" id="email" value="" required>			<div class="error_text">Поле обязательно для заполнения</div>          
		</div>          
		<button class="btn form__btn form__submit" type="submit" name="send_account_info">Сбросить пароль</button>        
		</form>        
		<p class="remind__enter">Еще не зарегистрированы? 			<a href="/login/?register=yes" title="Зарегистрироваться">				Зарегистрироваться			</a>		</p>	
		</div>
		</section>
		<div id="forgotpasswd" class="transaction">	<div class="body_modal">		
		<p>			На ваш e-mail отправлено письмо со ссылкой для смены пароля. 		<p>	</div></div>
		<script>			
		BX.message({		TEMPLATE_PATH: '<?=$this->GetFolder()?>'	});</script>
		
