<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
if(!check_bitrix_sessid()) {
	echo json_encode(array('success' => 0));
	die();
}

if ( !empty($_POST['EMAIL']) ) 
{
	$EMAIL = trim($_POST['EMAIL']);	
	
	CModule::IncludeModule('main');	
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;
	
	$rsUsers = CUser::GetList(($by="UF_SUM_BONUS"), ($order="desc"), ['ID' => $USER->GetID()], ['FIELDS' => ['ID', 'CHECKWORD']]);	
	
	$checkword = '';

	if ($arUser = $rsUsers->Fetch()) 
	{
		$checkword = $arUser['CHECKWORD'];
	}
	
	if (!empty($checkword))
	{	
		CEvent::Send("USER_PASS_REQUEST", "s1", ["EMAIL" => $EMAIL, "URL_LOGIN" => $EMAIL, "CHECKWORD" => $checkword]);
		
		echo json_encode(['success' => true]);		
	}
	else
	{
		echo json_encode(['error' => true]);	
	}
} 	
?>