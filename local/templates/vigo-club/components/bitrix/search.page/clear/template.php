<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>  

<section class="section search">
	<div class="container">
        <h1 class="section__title search__title">Поиск</h1>
		
		<form class="search__form form" action="" method="get">
          <div class="form__item search__wrap">
            <label class="visually-hidden" for="search">Поиск</label>		
			<input class="header__search-btn header__search-btn2" type="submit" value="">			
            <input class="form__input-text search__input" type="text" name="q" id="search" value="<?=$arResult["REQUEST"]["QUERY"]?>">
          </div>
          <button class="visually-hidden" type="submit" name="form-submit"></button>
          <button type="button" class="search__clear" aria-label="Очистить поле поиска"></button>
        </form>	
		<div class="search__result">			
		
			<? if (count($arResult["SEARCH"])>0) { ?>	
			
				<? foreach($arResult["SEARCH"] as $arItem) { ?>		
					<div class="search__item">			
						<a href="<?=$arItem["URL"]?>" class="search__link"><?=$arItem["TITLE_FORMATED"]?></a>				
						<p class="search__text"><?echo $arItem["BODY_FORMATED"]?></p>				
					</div>		  
				<? } ?> 
				
			<? } else { ShowNote(GetMessage("CT_BSP_NOTHING_TO_FOUND")); }?>
			
		</div>		
	</div>
</section>

<?=$arResult["NAV_STRING"];?>