<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<h1 class="visually-hidden">Vigo Club Главная</h1>	
	
<section class="section hero">
	<div class="hero__carousel">
	  
	  <? if ( count($arResult["PROPERTIES"]["SLIDES_PNG_PC"]["VALUE"]) > 0 ) { ?>
		
		  <? $i = 0;			 
			 foreach($arResult["PROPERTIES"]["SLIDES_PNG_PC"]["VALUE"] as $item) { ?> 
		  
				<div class="hero__item">
				  <div class="hero__text-wrap">
				  
				    <? if (!empty($arResult["PROPERTIES"]["SLIDES_TITLE"]["VALUE"][$i])) { ?>	
						<h2 class="section__title hero__title"><?=$arResult["PROPERTIES"]["SLIDES_TITLE"]["VALUE"][$i]?></h2>
					<? } ?>
					
					<? if (!empty($arResult["PROPERTIES"]["SLIDES_DESC"]["VALUE"][$i])) { ?>	
						<p class="hero__text"><?=$arResult["PROPERTIES"]["SLIDES_DESC"]["VALUE"][$i]?></p>
					<? } ?>	
					
				  </div>
				  <div class="hero__img" aria-hidden="true">
					<picture>
					  <source type="image/webp" media="(min-width:780px)" data-srcset="<?=$arResult["PROPERTIES"]["SLIDES_WEBP_PC"]["VALUE"][$i]?>">
					  <source type="image/webp" data-srcset="<?=$arResult["PROPERTIES"]["SLIDES_WEBP_MOB"]["VALUE"][$i]?>">
					  <source data-srcset="<?=$arResult["PROPERTIES"]["SLIDES_PNG_PC"]["VALUE"][$i]?>" media="(min-width: 780px)">
					  <img data-src="<?=$arResult["PROPERTIES"]["SLIDES_PNG_MOB"]["VALUE"][$i]?>" alt="Vigo" width="800" class="poly--cover-left lazyload">
					</picture>
				  </div>
				  <div class="hero__btn-wrap">
					<a class="hero__btn btn btn--filled" type="button" name="button" href="/login/?register=yes">Вступай в VigoClub<span>!</span></a>
				  </div>
				</div>
				
			<? ++$i; ?>				  
		 <? } ?>
	  <? } ?>
	  
	</div>
</section>