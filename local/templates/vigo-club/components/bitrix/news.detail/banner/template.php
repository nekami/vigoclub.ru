<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="section hero">
      <div class="container hero__container hero__container--inner">
        <div class="hero__wrap">
          <h2 class="section__title hero__title"><?=$arResult["PROPERTIES"]["NAME_BANNER"]["VALUE"]?></h2>
          <p class="hero__text"><?=$arResult["PROPERTIES"]["DESC_BANNER"]["VALUE"]?></p>
        </div>
        <div class="hero__img hero__img--inner hero__img--sale" aria-hidden="true">
          <picture>
            <source type="image/webp" media="(min-width:780px)" srcset="<?=$arResult["PROPERTIES"]["BANNER_WEBP_PC"]["VALUE"]?>">
            <source type="image/webp" srcset="<?=$arResult["PROPERTIES"]["BANNER_WEBP_MOB"]["VALUE"]?>">
            <source srcset="<?=$arResult["PROPERTIES"]["BANNER_PNG_PC"]["VALUE"]?>" media="(min-width: 780px)">
            <img src="<?=$arResult["PROPERTIES"]["BANNER_PNG_MOB"]["VALUE"]?>" alt="Vigo" width="800" class="poly poly--bottom">
          </picture>
        </div>
      </div>
</section>