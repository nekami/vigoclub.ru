<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="section hero hero--contacts">
	<div class="hero__item">
        <div class="hero__text-wrap">
          <h2 class="section__title hero__title"><?=$arResult["PROPERTIES"]["NAME_BANNER"]["VALUE"]?></h2>
          <p class="hero__text"><?=$arResult["PROPERTIES"]["DESC_BANNER"]["VALUE"]?></p>
        </div>
        <div class="hero__img" aria-hidden="true">
          <picture>
            <source type="image/webp" media="(min-width:780px)" data-srcset="<?=$arResult["PROPERTIES"]["BANNER_WEBP_PC"]["VALUE"]?>">
            <source type="image/webp" data-srcset="<?=$arResult["PROPERTIES"]["BANNER_WEBP_MOB"]["VALUE"]?>">
            <source data-srcset="<?=$arResult["PROPERTIES"]["BANNER_PNG_PC"]["VALUE"]?>" media="(min-width: 780px)">
            <img data-src="<?=$arResult["PROPERTIES"]["BANNER_PNG_MOB"]["VALUE"]?>" alt="Vigo" width="800" class="poly--cover lazyload">
          </picture>
        </div>
        <div class="hero__btn-wrap">
          <a class="hero__btn btn btn--filled" type="button" name="button" href="#write_to_us"><?=$arResult["PROPERTIES"]["BUTTON_TEXT"]["VALUE"]?></a>
        </div>
	</div>
</section>