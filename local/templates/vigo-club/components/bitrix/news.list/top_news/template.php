<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<section class="section events">
	<div class="container events__container">
		<h2 class="visually-hidden">Новости</h2>
        <ul class="events__list list">
		
			<?foreach($arResult["ITEMS"] as $arItem) { ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				
				  <li class="events__item">
					<div class="events__item-container">
					  <h3 class="events__title"><?=$arItem["NAME"]?></h3>
					  <p class="events__text"><?=$arItem["PREVIEW_TEXT"];?></p>
					  <p class="events__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></p>
					</div>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="events__link">Перейти к новости</a>
				  </li>
		  
			<? } ?>
        </ul>
      </div>
</section>