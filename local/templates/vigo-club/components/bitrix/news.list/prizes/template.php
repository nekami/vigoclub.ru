<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
    <section class="section prize">
      <div class="container prize__container">
        <h2 class="section__title prize__title"><?=GetMessage("PRIZES")?></h2>
        <ul class="prize__list list">
		
		<?foreach($arResult["ITEMS"] as $arItem) { ?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				
				if (!empty($arItem["PREVIEW_PICTURE"]["SRC"]))
					$img = $arItem["PREVIEW_PICTURE"]["SRC"];
				else $img = "/upload/vigo/priz.jpg";
				
				?>
		
			  <li class="prize__item">
				<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
					<div class="prize__item-top">
					  <div class="prize__image" aria-hidden="true">
						<picture>
						  <source data-srcset="<?=$img;?>" media="(min-width: 780px)">
						  <img data-src="<?=$img;?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"];?>" class="lazyload">
						</picture>
					  </div>
					  <p class="prize__text"><?=$arItem["NAME"]?></p>
					</div>
				</a>
				<div class="prize__item-bottom">
				  <p class="prize__bonuses">
					<span class="prize__quantity">					
					
						<? if ( $arItem["PROPERTIES"]["PAR"]["VALUE"] != false ) { ?>
						
							<?=min($arItem["PROPERTIES"]["PAR"]["VALUE"])?> - <?=max($arItem["PROPERTIES"]["PAR"]["VALUE"])?>
						
						<? } else if ( !empty($arItem["PROPERTIES"]["MIN"]["VALUE"]) && !empty($arItem["PROPERTIES"]["MAX"]["VALUE"]) ) { ?>
					
							<?=$arItem["PROPERTIES"]["MIN"]["VALUE"]?> - <?=$arItem["PROPERTIES"]["MAX"]["VALUE"]?>
						
						<? } else if ( $arItem["PROPERTIES"]["PAR_M"]["VALUE"] != false ) { ?>
						
							<?=min($arItem["PROPERTIES"]["PAR_M"]["VALUE"])?> - <?=max($arItem["PROPERTIES"]["PAR_M"]["VALUE"])?>
						
						<? } ?>	
					
					</span><span class="prize__bonus"><?=GetMessage("BONUSES")?></span>
				  </p>
				  <a href="" class="prize__btn btn"><?=GetMessage("RECEIVE")?></a>
				</div>
			  </li>
		  
		<? } ?>
		
        </ul>
        <div class="link-wrap">
          <a href="/prizy/" class="link-next"><?=GetMessage("SHOW_ALL")?></a>
        </div>
      </div>
    </section>
	
	<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
	</script>