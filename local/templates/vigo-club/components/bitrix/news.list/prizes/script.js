$(function() {

	$('.prize__item .btn').click(function(event)
	{
		event.preventDefault();
		
		var bonuses = $(this).attr('data-bonuses');	
		
		if (bonuses) 
		{
			$.ajax({
				url: BX.message('TEMPLATE_PATH')+'/ajax.php',
				data: {'BONUSES': bonuses},
				method: 'POST',
				type: 'POST',
				dataType: 'json',
				/*success: function(data)
				{
					console.log(data);
				},*/
				error: function(data)
				{
					console.log("Ошибка ajax-запроса");
				}
				
			});
		}
	});

});