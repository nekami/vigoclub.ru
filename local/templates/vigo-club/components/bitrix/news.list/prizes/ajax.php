<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?	
if ( !empty($_POST['BONUSES']) )
{
	global $USER;
	global $USER_FIELD_MANAGER;
	
	$Bonuses = $_POST['BONUSES'];
	$idUser = '';
	$SectionId = '';
	$SumPoints = 0;
	
    CModule::IncludeModule('iblock');	
	
	$arFilter = Array('IBLOCK_ID' => PRIZES_IBLOCK_ID, 'UF_USER_ID' => $USER->GetId(), 'ACTIVE' => 'Y');
	
	$arSelect = Array('ID', 'UF_USER_ID');	
	
	$rsUserSection = CIBlockSection::GetList(Array(), $arFilter, false, $arSelect, false);
					
	if ($arUserSection = $rsUserSection->Fetch())
	{
		$idUser = $arUserSection['UF_USER_ID'];	
		$SectionId = $arUserSection['ID'];
	}

	if ( !empty($idUser) )
	{
		$arSelectFields = Array('ID', 'PROPERTY_POINTS');
		
		$arFilter = Array('IBLOCK_ID' => PRIZES_IBLOCK_ID, 'ACTIVE' => 'Y');		
		
		$rsUserElements = CIBlockElement::GetList( Array(),$arFilter, false, false, $arSelectFields );
		
		while($arUserElements = $rsUserElements->Fetch())
		{
			if ( $Bonuses > 0 )
			{
				if ($arUserElements['PROPERTY_POINTS_VALUE'] <= $Bonuses)
				{
					$Bonuses = $Bonuses - $arUserElements['PROPERTY_POINTS_VALUE'];					
					CIBlockElement::Delete($arUserElements['ID']);
				}			
					
				if ($arUserElements['PROPERTY_POINTS_VALUE'] > $Bonuses)
				{
					$arUserElements['PROPERTY_POINTS_VALUE'] = $arUserElements['PROPERTY_POINTS_VALUE'] - $Bonuses;
					CIBlockElement::SetPropertyValuesEx($arUserElements['ID'], false, array('POINTS' => $arUserElements['PROPERTY_POINTS_VALUE']));				
				}
			}
			
			if ( !empty($arUserElements['PROPERTY_POINTS_VALUE']) && !empty($arUserElements['ID']))
			{
				$SumPoints = $SumPoints + $arUserElements['PROPERTY_POINTS_VALUE'];
			}
		}
		
		$USER_FIELD_MANAGER->Update( 'IBLOCK_7_SECTION', $SectionId, array('UF_SUM_POINTS'  => $SumPoints));		
	}
	
	echo json_encode(array('success' => 1);
}
else 
{
	echo json_encode(array('success' => 0);
}
?>