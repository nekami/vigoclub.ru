$(function()
{			 
	ClearErrorText();

	// вывод имени фото	
    $('.form__input-file').change(function()
	{  	
        var value = $(this).val().replace(/.*\\/, "");
        $(this).next('.form__note--file-name').text(value).show();
    });


	// вывод моделей в зависимости от выбранной категории	
	$(document).on('change', 'select[name="CATEGORY[]"]', function() 
	{				
		var id_category = $(this).val();		
		var me = this;

		if (id_category) 
		{	
			$.ajax({
				url: BX.message('TEMPLATE_PATH')+'/ajax.php',
				data: {'id_category': id_category, 'sessid': $('input[name="sessid"]').val(), 'action' : 'model'},
				method: 'POST',
				type: 'POST',
			    dataType: 'json',
				success: function(data)
				{	
					$(me).parent('div.form__item.sale__category').next('div.form__item.sale__model').find('select[name="MODEL[]"]').html('<option value=""></option>'); 
										  
					$.each(data, function(key, value) 
					{							 
						$(me).parent('div.form__item.sale__category').next('div.form__item.sale__model').find('select[name="MODEL[]"]').append('<option value="'+key+'" data-bonuses="'+value.BONUSES+'">'+value.NAME+'</option>');
					});
				},         
				error:  function(data){
					console.log('Ошибка ajax-скрипта');
				}
			})
		}						
	});	
	
	
	BX.BONUSES = {};
		
	// при выборе модели
	$(document).on('change', 'select[name="MODEL[]"]', function()
	{
		var index = $(this).prev('input[name="M_ITEM"]').val();
		
		BX.BONUSES[index] = $(this).find('option:selected').attr('data-bonuses');		
		
		$(this).next('input[name="BONUSES[]"]').val(BX.BONUSES[index]);
		
	});
	
	
	// отправка формы
	$(document).on('click', '#iblock_submit', function (event)
	{
		event.preventDefault();
		
		var category    = $('select[name="CATEGORY[]"]').val();		
		var model       = $('select[name="MODEL[]"]').val();		
		var foto_chek   = $('input[name="DETAIL_PICTURE"]').val();		
		var date        = $('input[name="SALE_DATE"]').val();		
		var fn          = $('input[name="SALE_FN"]').val();		
		var fd          = $('input[name="SALE_FD"]').val();		
		var fp          = $('input[name="SALE_FP"]').val();		
		var sum         = $('input[name="SALE_SUM"]').val();		
		var time        = $('input[name="SALE_TIME"]').val();		
		var type_chek   = $('select[name="SALE_RECEIPT_TYPE"]').val();		
		
		var error = false;

		// валидация

		$('.sale__item select[name="CATEGORY[]"]').each(function(index, value) 
		{
			if(!$(value).val()) 
			{
				$(value).addClass("errortext");
				$(value).parent('.sale__category').find('div.error_text').show();
				error = true;
				ScrollTopErrorText();
			}
		});
				
		$('.sale__item select[name="MODEL[]"]').each(function(index, value) 
		{
			if(!$(value).val()) 
			{
				$(value).addClass("errortext");
				$(value).parent('.sale__model').find('div.error_text').show();
				error = true;
				ScrollTopErrorText();
			}
		});			
		
		if ((!date || !fn || !fd || !sum || !time || !type_chek) && !foto_chek && (error == false))
		{			
			$('.alert .body_modal p.msg').html('Пожалуйста, загрузите фото чека или заполните все его параметры.');
			ShowPopupWindow("error", "alert");
			error = true;
		}
		
		if (error == false)
		{			
			var summa_bonusov = 0;	
		
			$.each(BX.BONUSES, function(key, value) 
			{
				summa_bonusov = Number(summa_bonusov) + Number(value);			
			});	
			
			formData = new FormData($('form[name="iblock_add"]').get(0)); 
					
			$.ajax({
				url: BX.message('TEMPLATE_PATH')+'/ajax.php',			
				data: formData,			
				contentType: false,
				processData: false,
				method: 'POST',
				type: 'POST',
				dataType: 'html',
				success: function(msg)
				{
					if (msg == 1) 
					{ 				
						$('.alert .body_modal p.msg').html('Поздравляем, вам начислено '+summa_bonusov+' бонусов!<br>Модератор после проверки их активизирует.');
						ShowPopupWindow("success", "alert");
					}	 					  
				},         
				error:  function(data){
					console.log('Ошибка ajax-скрипта');
				}
			});		
		}
	});	
	
});