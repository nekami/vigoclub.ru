<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>

<? CModule::IncludeModule('iblock'); ?>

<? 
if ( ( count($_REQUEST['CATEGORY']) > 0 && count($_REQUEST['MODEL']) > 0 && count($_REQUEST['BONUSES']) > 0 && !empty($_REQUEST['M_ITEM']) ) &&
	(( !empty($_REQUEST['SALE_DATE']) && !empty($_REQUEST['SALE_FN']) && !empty($_REQUEST['SALE_FD']) && !empty($_REQUEST['SALE_FP']) && !empty($_REQUEST['SALE_SUM']) && !empty($_REQUEST['SALE_TIME']) && !empty($_REQUEST['SALE_RECEIPT_TYPE']) ) || (!empty($_FILES['DETAIL_PICTURE']))))		
{

	global $USER; 
	if (!is_object($USER)) $USER = new CUser;	
	
	// формируем чек, получая его id для зальнейшей его привязки к продаже
	$arFieldsСheck = [
	   "IBLOCK_ID"          => CHEQUES_IBLOCK_ID,
	   "PROPERTY_VALUES"    => $PROP,
	   "NAME"               => $USER->GetFullName().' '.date("d.m.Y"),
	   "DETAIL_PICTURE"     => !empty($_FILES['DETAIL_PICTURE']) ? $_FILES['DETAIL_PICTURE'] : "",
	   "ACTIVE"             => "Y",
	   "PROPERTY_VALUES" => [
		   "SALE_DATE"         => $_REQUEST['SALE_DATE'], 
		   "SALE_FN"           => $_REQUEST['SALE_FN'], 
		   "SALE_FD"           => $_REQUEST['SALE_FD'],
		   "SALE_FP"           => $_REQUEST['SALE_FP'], 
		   "SALE_SUM"          => $_REQUEST['SALE_SUM'], 
		   "SALE_TIME"         => $_REQUEST['SALE_TIME'],
		   "SALE_RECEIPT_TYPE" => $_REQUEST['SALE_RECEIPT_TYPE']
	   ]
	];
	$oElement = new CIBlockElement();
	$idСheck = $oElement->Add($arFieldsСheck, false, false, true); 
	
	if (!empty($idСheck))
	{
		// перебираем все позиции в чеке и на каждую создаем запись в папке пользоватя в ИБ Продажи
		
		for ($i = 0; $i < $_REQUEST['M_ITEM']; $i++) 
		{
			$arFieldsTransaction = [
			   "IBLOCK_SECTION_ID"  => $_REQUEST['IBLOCK_SECTION'],
			   "IBLOCK_ID"          => PRIZES_IBLOCK_ID,			   
			   "NAME"               => GetNameCategoryAndModel($_REQUEST['CATEGORY'][$i], $_REQUEST['MODEL'][$i]),
			   "ACTIVE"             => "Y",
			   "PROPERTY_VALUES" => [
				   "PRODUCT_CATEGORY" => $_REQUEST['CATEGORY'][$i], 
				   "MODEL"            => $_REQUEST['MODEL'][$i], 
				   "POINTS"           => $_REQUEST['BONUSES'][$i],
				   "CHECK"            => $idСheck				   
			   ]
			];
			
			$idTransaction = $oElement->Add($arFieldsTransaction, false, false, true); 
			
			if (empty($idTransaction)) { echo "Произошла ошибка при добавлении товаров. Попоробуйте еще раз."; die; }			
		}	
		
		echo true;
	} 
	else 
	{ 
		echo "Произошла ошибка при добавлении чека. Попоробуйте еще раз."; die;
	}
}

if ( !empty($_POST['id_category']) && ($_POST['action'] == 'model')) 
{
	$id_category = $_POST['id_category'];
	
	$arFilter = Array('IBLOCK_ID'=> CATALOG_IBLOCK_ID, 'IBLOCK_SECTION_ID' => $id_category, 'ACTIVE' => 'Y');
	
	$arSelectFields = Array('ID', 'NAME', 'PROPERTY_BONUSES');
	
	$rsElemebts = CIBlockElement::GetList(Array("NAME"=>"ASC"), $arFilter, false, false, $arSelectFields);

	$res = array();
	
	while($arElements = $rsElemebts->GetNext())
	{
		$res[$arElements['ID']] = array('NAME' => $arElements['NAME'], 'BONUSES' => $arElements['PROPERTY_BONUSES_VALUE']);
	}
	
	echo json_encode($res);
} 
?>