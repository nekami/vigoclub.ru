<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
  // запрос на категории объекта   
   
  $arFilter = array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y');
  
  $arSelect = array('ID', 'NAME');  
  
  $db_list = CIBlockSection::GetList(Array('NAME'=>'ASC'), $arFilter, false, $arSelect, false); 
  
  $arResult["CATEGORY"] = [];  
  
  while($ar_result = $db_list->GetNext())
  {	
	$arResult["CATEGORY"][$ar_result['ID']] = $ar_result['NAME'];
  }

  // запрос видов чека 
  
  $property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC"), Array("IBLOCK_ID"=>CHEQUES_IBLOCK_ID, "CODE"=>"SALE_RECEIPT_TYPE"));
  
  $arResult["FORM_CHECK"] = [];
  
  while ($enum_fields = $property_enums->GetNext()) 
  {
	   $arResult["FORM_CHECK"][$enum_fields['ID']] = $enum_fields['VALUE'];
  } 

  $this->__component->setResultCacheKeys(array("CATEGORY", "FORM_CHECK"));
?>