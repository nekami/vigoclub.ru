<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<? global $USER; ?>

    <section class="section sale">
      <div class="container">
        <form name="iblock_add" class="sale__form form" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		
		  <?=bitrix_sessid_post()?>		  	
		   
		  <input type="hidden" name="IBLOCK_SECTION" value="<?=GetIdDirUser($USER->GetId())?>">		  
		
          <fieldset class="form__fieldset form__fieldset--sale-items">
            <legend class="form__legend sale__title">О продаже</legend>
            <div class="sale__items">
              <div class="sale__item">
                <div class="sale__item-top">
                  <p class="sale__item-title">Товар1 из чека. Заполняется по одной позиции</p>
                  <button type="button" class="sale__delete-btn" onclick="removeItem(this)">Удалить</button>
                </div>
							
                <div class="form__item sale__category">
                  <label class="form__label">Категория товара</label>
                  <select class="form__select form__select--category" name="CATEGORY[]">
						<option value=""></option>					
					<? foreach ( $arResult["CATEGORY"] as $key => $item ) { ?>					
						<option value="<?=$key?>"><?=$item?></option>					
					<? } ?>
                  </select>	
				 <div class="error_text">Поле обязательно для заполнения</div>				  
                </div>		
				
                <div class="form__item sale__model">
                  <label class="form__label">Модель</label>
				  
				  <input type="hidden" name="M_ITEM" value="1">
				  
				  <select class="form__select form__select--model" name="MODEL[]">
					   <option value=""></option>
				  </select>
				  
				  <input type="hidden" name="BONUSES[]" value="">
				  
				  <div class="error_text">Поле обязательно для заполнения</div>
                </div>
				
                <p class="sale__bonuses"></p>
              </div>
            </div>
            <button type="button" class="btn sale__add-item">Добавить еще товар</button>
          </fieldset>
          <fieldset class="form__fieldset form__fieldset--receipt">
            <legend class="form__legend sale__title sale__title--receipt">Информация с чека</legend>
            <div class="form__item form__item--file-btn">			
			  <label class="btn form__btn form__btn--file" for="receipt">Загрузить фото чека</label>
              <input class="form__input-file visually-hidden" accept="image/*" type="file" name="DETAIL_PICTURE" id="receipt">
              <p class="form__note form__note--file-name"></p>
			 
              <p class="form__note form__note--text">Чек на фото должен быть виден полностью, изображение – четким и хорошо читаемым</p>
            </div>
            <p class="sale__conjunction"><span>или</span></p>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-date">Дата продажи</label>
              <input class="form__input-text form__input-receipt form__input-text--date" type="text" name="SALE_DATE" 
			  id="sale-date" pattern="[0-9]{2}.[0-9]{2}.[0-9]{4}" value="">
			</div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-fn">ФН</label>
              <input class="form__input-text form__input-receipt" type="text" name="SALE_FN" id="sale-fn" placeholder="">			 
            </div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-fd">ФД</label>
              <input class="form__input-text form__input-receipt" type="text" name="SALE_FD" id="sale-fd" placeholder="">			  
            </div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-fp">ФП</label>
              <input class="form__input-text form__input-receipt" type="text" name="SALE_FP" id="sale-fp" placeholder="">			  
            </div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-sum">Итог</label>
              <input class="form__input-text form__input-receipt" type="text" name="SALE_SUM" id="sale-sum" placeholder="">			  
            </div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="sale-time">Время</label>
              <input class="form__input-text form__input-receipt form__input-text--time" type="text" name="SALE_TIME" id="sale-time" pattern="[0-9]{2}:[0-9]{2}">              
			</div>
            <div class="form__item form__item--receipt">
              <label class="form__label" for="receipt-type">Вид чека</label>
              <select class="form__select form__input-receipt" name="SALE_RECEIPT_TYPE" id="receipt-type">
                <option value=""></option>                
				<? foreach ($arResult["FORM_CHECK"] as $key => $item) { ?>				
					<option value="<?=$key?>"><?=$item?></option>				
				<? } ?>				
              </select>			  
            </div>
          </fieldset>	  
          <button class="btn form__btn form__btn-submit" type="submit" id="iblock_submit"><?=GetMessage("IBLOCK_FORM_SUBMIT")?></button>
        </form>
      </div>
    </section>			

	<script>
		BX.message({
			TEMPLATE_PATH: '<?=$this->GetFolder()?>'
		});
	</script>