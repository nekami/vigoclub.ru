<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<? if ($arResult["isFormNote"] == "Y") { ?>	
	<div id="result" class="alert">
		<div class="body_modal">
			<p>
				<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
				<?=$arResult["FORM_NOTE"]?>
			<p>
		</div>
	</div>
	
	<script>
		ShowPopupWindow("result", "alert");
	</script>
	
<? } ?>

<?=$arResult["FORM_HEADER"]?>

<section class="section contact" id="write_to_us">		  

	<div class="container">			

		<h2 class="section__title contact__title"><?=$arResult["FORM_TITLE"]?></h2>							

		<p class="contact__text"><?=$arResult["FORM_DESCRIPTION"]?> 👋</p>							

		<div class="contact__form form">									 

		<? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) { ?>

			<? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea') { ?>
			
			<div class="form__item form__item--last">
				<label class="form__label" for="text"><?=$arQuestion["CAPTION"]?></label>
				<?=$arQuestion["HTML_CODE"]?>					  
			</div>		
			
			<? } else { ?>
			
				<div class="form__item">						
					<label class="form__label"><?=$arQuestion["CAPTION"]?></label>						
					<?=$arQuestion["HTML_CODE"]?>					  
				</div>	
				
			<? } ?>	
			
		<? } ?>	

		<div class="form__item form__item--checkbox form__item--bottom">
            <input class="form__input-checkbox" type="checkbox" name="AGREE" id="agree" required="">
            <label class="form__label-checkbox" for="agree">Я принимаю <a href="#">условия положения о конфиденциальности </a></label>
            <p class="form__note">Обязательное условие</p>
        </div>

		<input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" class="btn form__btn form__submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />			  			
		
		</div>
	</div>	
</section>

<?=$arResult["FORM_FOOTER"]?>