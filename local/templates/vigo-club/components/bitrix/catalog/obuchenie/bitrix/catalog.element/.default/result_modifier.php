<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arResult["PROPERTIES"]["PREV_PC_VIDEOS"]["~VALUE"] = CFile::GetPath($arResult["PROPERTIES"]["PREV_PC_VIDEOS"]["~VALUE"]);
$arResult["PROPERTIES"]["PREV_PC_VIDEOS_WEBP"]["~VALUE"] = CFile::GetPath($arResult["PROPERTIES"]["PREV_PC_VIDEOS_WEBP"]["~VALUE"]);
$arResult["PROPERTIES"]["PREV_M_VIDEOS"]["~VALUE"] = CFile::GetPath($arResult["PROPERTIES"]["PREV_M_VIDEOS"]["~VALUE"]);
$arResult["PROPERTIES"]["PREV_M_VIDEOS_WEBP"]["~VALUE"]  = CFile::GetPath($arResult["PROPERTIES"]["PREV_M_VIDEOS_WEBP"]["~VALUE"]);

/***********************************************************************************************************************/

$ipropElementValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult['IBLOCK_ID'], $arResult['ID']);
$arResult["SEO"] = $ipropElementValues->getValues();
$this->__component->arResultCacheKeys = array('SEO');

?>