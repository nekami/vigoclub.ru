<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arIdSEction = array_column($arResult['SECTIONS'], 'ID');

$rsSections = CIBlockSection::GetList([], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arIdSEction], false, ["ID", "UF_NUMBER_COURS", "UF_IMG_WEBP"], false);

$i = 0;
while ($arSection = $rsSections->Fetch())
{
	$arResult['SECTIONS'][$i]['IPROPERTY_VALUES']['UF_NUMBER_COURS'] = $arSection['UF_NUMBER_COURS'];	
	$arResult['SECTIONS'][$i]['IPROPERTY_VALUES']['UF_IMG_WEBP'] = CFile::GetPath($arSection['UF_IMG_WEBP']);
	++$i;
}
?>