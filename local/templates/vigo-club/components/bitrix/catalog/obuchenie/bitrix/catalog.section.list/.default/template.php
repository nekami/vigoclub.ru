<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>

<section class="section education">
      <div class="container">
        <ul class="education__list list">
		
		<? foreach ($arResult['SECTIONS'] as $arSection) {
			$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
			$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);?>				
				
			<li class="education__item">
				<div class="education__img">
				  <picture>
					<source type="image/webp" data-srcset="<?=$arSection['IPROPERTY_VALUES']['UF_IMG_WEBP']?>">
					<img data-src="<?=$arSection['PICTURE']['SRC']?>" alt="<?=$arSection['PICTURE']['ALT']?>" width="400" class="lazyload">
				  </picture>
				</div>
				<h3 class="education__title"><?=$arSection['NAME']?></h3>
				<div class="link-wrap education__link">
				  <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="link-next"><?=$arSection['IPROPERTY_VALUES']['UF_NUMBER_COURS']?> курсов</a>
				</div>
            </li>
			
		<? } unset($arSection); ?>
		
        </ul>
    </div>
</section>