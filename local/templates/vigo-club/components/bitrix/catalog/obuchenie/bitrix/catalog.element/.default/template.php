<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	
<h1 class="section__title section__title--top"><?=$arResult["NAME"]?></h1>

<section class="section inner-menu">
    <nav class="inner-menu__contaiter container">
        <ul class="inner-menu__list list">
          <li class="inner-menu__item">		  
            <a class="inner-menu__nav-link inner-menu__nav-link--active" href="">
              <span class="inner-menu__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                  <g fill="none" fill-rule="evenodd" transform="translate(0 -2)">
                    <path fill-rule="nonzero" d="M12,3 L1,9 L5,11.18 L5,17.18 L12,21 L19,17.18 L19,11.18 L21,10.09 L21,17 L23,17 L23,9 L12,3 Z M18.82,9 L12,12.72 L5.18,9 L12,5.28 L18.82,9 Z M17,15.99 L12,18.72 L7,15.99 L7,12.27 L12,15 L17,12.27 L17,15.99 Z"/>
                  </g>
                </svg>
              </span>
              <span class="inner-menu__text">Обучающий курс</span>
            </a>			
          </li>
          <li class="inner-menu__item">            
			<a class="inner-menu__nav-link" href="">
              <span class="inner-menu__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <g fill="none" fill-rule="evenodd" transform="translate(-3 -3)">
                    <path fill-rule="nonzero" d="M19,3 L5,3 C3.9,3 3,3.9 3,5 L3,19 C3,20.1 3.9,21 5,21 L19,21 C20.1,21 21,20.1 21,19 L21,5 C21,3.9 20.1,3 19,3 Z M19,19 L5,19 L5,5 L19,5 L19,19 Z M17.99,9 L16.58,7.58 L9.99,14.17 L7.41,11.6 L5.99,13.01 L9.99,17 L17.99,9 Z"/>
                  </g>
                </svg>
              </span>
              <span class="inner-menu__text">Тестирование</span>
            </a>			
          </li>
          <li class="inner-menu__item">		  
            <a class="inner-menu__nav-link" href="">
              <span class="inner-menu__icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <g fill="none" fill-rule="evenodd" transform="translate(-4 -2)">
                    <path fill-rule="nonzero" d="M15,8 L15,16 L5,16 L5,8 L15,8 L15,8 Z M16,6 L4,6 C3.45,6 3,6.45 3,7 L3,17 C3,17.55 3.45,18 4,18 L16,18 C16.55,18 17,17.55 17,17 L17,13.5 L21,17.5 L21,6.5 L17,10.5 L17,7 C17,6.45 16.55,6 16,6 Z"/>
                  </g>
                </svg>
              </span>
              <span class="inner-menu__text">Вебинары</span>
            </a>			
          </li>
        </ul>
    </nav>
</section>

<section class="section education education--video">
    <div class="container education__container">
        <div class="education__video-cover">
          <button type="button" class="play education__play-btn" aria-label="Запустить видео"></button>
          <div class="education__video-img">
            <picture>
			
              <source type="image/webp" media="(min-width:780px)" data-srcset="<?=$arResult["PROPERTIES"]["PREV_PC_VIDEOS_WEBP"]["~VALUE"]?>">
			  
              <source type="image/webp" data-srcset='<?=$arResult["PROPERTIES"]["PREV_M_VIDEOS_WEBP"]["~VALUE"]?>'>
			  
              <source data-srcset="<?=$arResult["PROPERTIES"]["PREV_PC_VIDEOS"]["~VALUE"]?>" media="(min-width: 780px)">
			  
              <img data-src="<?=$arResult["PROPERTIES"]["PREV_M_VIDEOS"]["~VALUE"]?>" alt="Серии <?=$arResult["NAME"]?> 600, 800, 1000" width="800" class="lazyload">
			  
            </picture>
          </div>
          <video class="education__video" width="500" controls controlsList="nodownload" preload="metadata">		  
            <source src="<?=$arResult["PROPERTIES"]["VIDEO"]["VALUE"]["path"]?>" type="video/mp4">
            <p>Ваш браузер не поддерживает тэг video</p>
          </video>
        </div>
        <div class="education__info-wrap">
          <p class="education__info education__info--level">Уровень подготовки: <?=$arResult["PROPERTIES"]["LEVEL_TRAINING"]["~VALUE"]?></p>
          <p class="education__info education__info--time"><?=$arResult["PROPERTIES"]["VIDEO_TIME"]["~VALUE"]?></p>
        </div>
        <a href="#" class="btn education__btn">Начать!</a>
    </div>
</section>

<section class="section education">
	<div class="container education__container">
		Содержимое 2
	</div>
</section>

<section class="section education">
	<div class="container education__container">
		Содержимое 3
	</div>
</section>