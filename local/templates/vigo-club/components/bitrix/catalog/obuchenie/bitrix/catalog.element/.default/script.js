$(function() {

  $(".section.education").not(":first").hide();
  
  $('.inner-menu__list.list  .inner-menu__item').click(function(event) 
  {	
	event.preventDefault();  
  
	$(this).children('a.inner-menu__nav-link').addClass('inner-menu__nav-link--active');	
	
	$('.inner-menu__list.list  .inner-menu__item').children('a.inner-menu__nav-link').not($(this).children('a.inner-menu__nav-link')).removeClass('inner-menu__nav-link--active');
	
	$(".section.education").hide().eq($(this).index()).fadeIn();
  });

}); 