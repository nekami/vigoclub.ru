//$(function() {	

  $(document).on('change', '.gift__select', function() 
  {
    let bonusAmmount = $(this).children('option:selected').data('bonus');
    $('.gift__bonus-number').text(bonusAmmount);
  });
  
  
  $(document).on('click', '.gift__info-button', function()  
  {
    let blockTitle = $(this).data('id');
    if(!$(this).hasClass('.gift__descriprion-item--active')) {
      $(this).addClass('gift__info-button--active');
      $('.gift__info-button').not(this).removeClass('gift__info-button--active');
      $('.gift__descriprion-item').removeClass('gift__descriprion-item--active');
      $('.gift__descriprion-item' + blockTitle).addClass('gift__descriprion-item--active');
    }
  });
  
  $(document).on('click', '#take', function()  
  {    
	$.ajax({
		url: BX.message('TEMPLATE_PATH')+'/ajax.php',
		method: 'POST',
		type: 'POST',
		dataType: 'html',
		success: function(msg)
		{		  
			console.log('Успех');
		},         
		error:  function(data){
			console.log('Ошибка ajax-скрипта');
		}
	});	
  });
	
//});
