<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	
    <section class="section gift">
      <div class="container">
        <h1 class="section__title gift__title">Подарочный сертификат <?=$arResult["NAME"];?></h1>
        <div class="gift__wrap">
          <div class="gift__img">
			   <? if ( is_array($arResult["DETAIL_PICTURE"]) ) { ?>
				
					<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"  alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" width="200" />
							
			   <? } else if ( is_array($arResult["PREVIEW_PICTURE"]) ) { ?>
				
					<img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" width="200" />
							
			   <? } else { ?> 
				
					<img src="/upload/vigo/priz.jpg" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" width="200" />
					
			   <? } ?>
          </div>
          <div class="gift__type">
		  
			 <? if (in_array("APS", $arResult['PROPERTIES']['TYPE_CERTIFICATE']['VALUE_XML_ID'])) { ?>	
			 
				<div class="gift__type-wrap">
				  <p class="gift__type-title">Электронный сертификат</p>
				  <button type="button" class="gift__details-btn" aria-label="Показать детальное описание"></button>
				  <div class="gift__type-details-wrap">
					<div class="gift__type-details">Электронные сертификаты доставляются на электронную почту в течение 10 минут при наличии. Пластиковые сертификаты комплектуются в течение 5 рабочих дней и доставлюются курьерской службой в сроки указанные при оформлении заказа.</div>
				  </div>
				</div>
				
			 <? } ?>	
			 
          </div>
          <form class="form gift__form" action="index.html" method="post">
            <input type="hidden" name="TITLE" value="Подарочный сертификат S7 Airlines">
            <div class="form__item">
              <label class="form__label" for="chain">Номинал</label>			  
			  <select class="form__select gift__select" name="VALUE" id="chain">			  
				<? foreach ($arResult["BONUSES"]["ITEMS"] as $key => $item) { ?>
				
					<? if ( ($arResult["BONUSES"]["TYPE"] == "FIX") || ($arResult["BONUSES"]["TYPE"] == "STEP") ) { ?>				
						<option data-bonus="<?=$item?>" value="<?=$item?>"><?=$item?></option>
					<? } ?>					
					
					<? if ( $arResult["BONUSES"]["TYPE"] == "KOL" ) { ?>				
						<option data-bonus="<?=$key?>" value="<?=$key?>"><?=$item?></option>
					<? } ?>
					
					
				<? } ?>				
              </select>				  
            </div>
			
            <p class="gift__bonus">
				<? if ( ($arResult["BONUSES"]["TYPE"] != "FIX") && ($arResult["BONUSES"]["TYPE"] != "STEP") ) { ?>
					<span class="gift__bonus-number"><?=key($arResult["BONUSES"]["ITEMS"]);?></span>
				<? } else { ?>
					<br/> 
				<? } ?>				
				<span>бонусов</span>
			</p>
			
            <button type="submit" class="btn gift__form-btn" id="take">Получить</button>
          </form>
        </div>
        <div class="gift__info-wrap">
          <div class="gift__info-buttons">
            <button type="button" class="gift__info-button gift__info-button--active" data-id="#description">Описание</button>
            <button type="button" class="gift__info-button" data-id="#howto">Как воспользоваться сертификатом?</button>
            <button type="button" class="gift__info-button" data-id="#rules">Правила</button>
          </div>
          <div class="gift__detailes-wrap">
            
			<? if ( !empty($arResult['PROPERTIES']['VALIDITY']['~VALUE']) ) { ?>
				<div class="gift__detail gift__detail--time">
				  <p class="gift__detail-title">Срок действия:</p>
				  <p class="gift__detail-text"><?=$arResult['PROPERTIES']['VALIDITY']['~VALUE']?></p>
				</div>
			<? } ?>
			
			<? if ( ($arResult['PROPERTIES']['USE_ONLINE_STORE']['VALUE_XML_ID'] == "Yes") || (in_array("APS", $arResult['PROPERTIES']['TYPE_CERTIFICATE']['VALUE_XML_ID']) && ($arResult['PROPERTIES']['USE_ONLINE_STORE']['VALUE_XML_ID'] == "Yes_APS")))  { ?>
			
				<div class="gift__detail gift__detail--place">
				  <p class="gift__detail-title">Где принимается:</p>
				  <p class="gift__detail-text">Интернет-магазин</p>
				</div>	
				
			<? } ?>		
			
            <div class="gift__detail gift__detail--doc">
              <p class="gift__detail-text"><a>Пользовательское соглашение</a></p>
            </div>
          </div>
          <div class="gift__descriprion content">
            <div class="gift__descriprion-item gift__descriprion-item--active" id="description">
              <p class="gift__descriprion-title">Описание</p>
              <div class="gift__descriprion-text">                
				<?				
				if($arResult["PREVIEW_TEXT"])
					echo $arResult["PREVIEW_TEXT"];				
				?>				
			  </div>
            </div>
            <div class="gift__descriprion-item" id="howto">
              <p class="gift__descriprion-title">Как воспользоваться сертификатом?</p>
              <div class="gift__descriprion-text">
				<?
				if($arResult["DETAIL_TEXT"])
					echo $arResult["DETAIL_TEXT"];
				?>
			  </div>
            </div>
            <div class="gift__descriprion-item" id="rules">
              <p class="gift__descriprion-title">Правила</p>
              <div class="gift__descriprion-text">
				<?
				if($arResult['PROPERTIES']['LIMITATIONS']['~VALUE']['TEXT'])
					echo $arResult['PROPERTIES']['LIMITATIONS']['~VALUE']['TEXT'];
				?>				
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	<script>		
		BX.message({
			TEMPLATE_PATH: '<?=$this->GetFolder()?>'
		});
	</script>