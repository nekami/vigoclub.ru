<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if ($arResult["SEO"]['ELEMENT_META_TITLE'] != false) {
	$APPLICATION->SetPageProperty("title", $arResult["SEO"]['ELEMENT_META_TITLE']);
}
if ($arResult["SEO"]['ELEMENT_META_KEYWORDS'] != false) {
	$APPLICATION->SetPageProperty("keywords", $arResult["SEO"]['ELEMENT_META_KEYWORDS']);
}
if ($arResult["SEO"]['ELEMENT_META_DESCRIPTION'] != false) {
	$APPLICATION->SetPageProperty("description", $arResult["SEO"]['ELEMENT_META_DESCRIPTION']);
}
?>