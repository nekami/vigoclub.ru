<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
// hack
if (!is_array($arResult['SECTION']))
{
	$dbRes = CIBlock::GetByID($arResult['IBLOCK_ID']);
	if ($arIBlock = $dbRes->GetNext())
	{
		$arIBlock["~LIST_PAGE_URL"] = str_replace(
			array("#SERVER_NAME#", "#SITE_DIR#", "#IBLOCK_TYPE_ID#", "#IBLOCK_ID#", "#IBLOCK_CODE#", "#IBLOCK_EXTERNAL_ID#", "#CODE#"),
			array(SITE_SERVER_NAME, SITE_DIR, $arIBlock["IBLOCK_TYPE_ID"], $arIBlock["ID"], $arIBlock["CODE"], $arIBlock["EXTERNAL_ID"], $arIBlock["CODE"]),
			strlen($arParams["IBLOCK_URL"])? trim($arParams["~IBLOCK_URL"]): $arIBlock["~LIST_PAGE_URL"]
		);
		$arIBlock["~LIST_PAGE_URL"] = preg_replace("'/+'s", "/", $arIBlock["~LIST_PAGE_URL"]);
		$arIBlock["LIST_PAGE_URL"] = htmlspecialcharsbx($arIBlock["~LIST_PAGE_URL"]);
		
		$arResult['IBLOCK'] = $arIBlock;
	}
}

$arResult['PRICES']['PRICE']['PRINT_VALUE'] = number_format($arResult['PROPERTIES']['PRICE']['VALUE'], 0, '.', ' ');
$arResult['PRICES']['PRICE']['PRINT_VALUE'] .= ' '.$arResult['PROPERTIES']['PRICECURRENCY']['VALUE_ENUM'];


/***********************************************************************************************************************/

$arResult["BONUSES"] = [];

if ( $arResult["PROPERTIES"]["PAR"]["VALUE"] != false ) 
{ 
	foreach ( $arResult["PROPERTIES"]["PAR"]["VALUE"] as $item )		
	{
		$arResult["BONUSES"]["ITEMS"][] = $item;
	}	

	$arResult["BONUSES"]["TYPE"] = "FIX";
	
} 
else if ( !empty($arResult["PROPERTIES"]["MIN"]["VALUE"]) && !empty($arResult["PROPERTIES"]["MAX"]["VALUE"]) ) 
{
	for ( $i = $arResult["PROPERTIES"]["MIN"]["VALUE"]; $i <= $arResult["PROPERTIES"]["MAX"]["VALUE"]; $i += $arResult["PROPERTIES"]["STEP"]["VALUE"] )
	{		
		$arResult["BONUSES"]["ITEMS"][] = $i;		
	}
	
	$arResult["BONUSES"]["ITEMS"][] = $arResult["PROPERTIES"]["MAX"]["VALUE"];
	
	$arResult["BONUSES"]["TYPE"] = "STEP";	
	
} 
else if ( $arResult["PROPERTIES"]["PAR_M"]["VALUE"] != false ) 
{	
	foreach ( $arResult["PROPERTIES"]["PAR_M"]["DESCRIPTION"] as $key => $item )		
	{
		$arResult["BONUSES"]["ITEMS"][$arResult["PROPERTIES"]["PAR_M"]["VALUE"][$key]] = $item;
	}
	
	$arResult["BONUSES"]["TYPE"] = "KOL";	
	
}	

/***********************************************************************************************************************/

$ipropElementValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult['IBLOCK_ID'], $arResult['ID']);
$arResult["SEO"] = $ipropElementValues->getValues();
$this->__component->arResultCacheKeys = array('SEO');

?>