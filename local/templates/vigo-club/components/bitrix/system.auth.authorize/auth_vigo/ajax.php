<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');?>

<?
/*if(!check_bitrix_sessid()) {
	echo json_encode(array('success' => 0));
	die();
}*/

if ( !empty($_POST['USER_LOGIN']) && !empty($_POST['USER_PASSWORD']) ) {	
	
	CModule::IncludeModule('main');
	
	global $USER;
	if (!is_object($USER)) $USER = new CUser;	

	$LOGIN =    trim($_POST['USER_LOGIN']);	
	$PASSWORD = trim($_POST['USER_PASSWORD']);	
	
	$result = \Bitrix\Main\UserTable::getList(array(
		'select' => array('ID', 'LOGIN'),
		'filter' => ['ACTIVE' => 'Y', 'LOGIN' => $LOGIN],
	));

	$idUser = '';	
	$loginUser = '';	

	if ($arUser = $result->fetch()) 
	{
		$idUser = $arUser['ID'];
		$loginUser = $arUser['LOGIN'];
	}
	
	if ( empty($idUser) ) { echo json_encode(['login' => 'Логин введен не верно']); die; }
	
	// проверка пароля
	$PasswordError = isUserPassword($idUser, $PASSWORD);	
	if ( empty($PasswordError) ) { echo json_encode(['pass' => 'Пароль введен не верно']); die; }
	
	$arAuthResult = $USER->Login($loginUser, $PASSWORD, "Y");
	$APPLICATION->arAuthResult = $arAuthResult;		
	echo json_encode(['success' => $APPLICATION->arAuthResult]);
} 	
?>