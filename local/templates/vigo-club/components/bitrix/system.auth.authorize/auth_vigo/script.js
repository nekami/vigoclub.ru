$(function() 
{	
	$(document).on('click', '#authorize_user', function(event) 
	{
		event.preventDefault();		
		
		var auth_form = $('input[name="AUTH_FORM"]').val();		
		var login     = $('input[name="USER_LOGIN"]').val();
		var pass      = $('input[name="USER_PASSWORD"]').val();
		//var sessid   =  $('input[name="sessid"]').val();

		if (!login || !pass)
		{
			if (!login)
			{ 
				$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').show(); 
			}
					
			if (!pass) 
			{ 
				$('input[name="USER_PASSWORD"]').addClass("errortext").next('div.error_text').show();            
			}			
		} 
		else 
		{		
			$.ajax({
			  url: BX.message('TEMPLATE_PATH')+'/ajax.php',
			  data: { 'AUTH_FORM': auth_form, 
					  'USER_LOGIN': login, 					  
					  'USER_PASSWORD': pass, 
					  //'sessid': sessid
			  },
			  method: 'POST',
			  type: 'POST',
			  dataType: 'json',
			  success: function(data)
			  {
				if (data.login) 
				{ 
					$('input[name="USER_LOGIN"]').addClass("errortext").next('div.error_text').html(data.login).show();
				}
				
				if (data.pass) 
				{ 
					$('input[name="USER_PASSWORD"]').addClass("errortext").next('div.error_text').html(data.pass).show();
				}		
				  
				if (data.success) 
				{ 
					$(location).attr('href', '/login/?login=yes'); 
				}
				
			  },         
			  error:  function(data){
				console.log('Ошибка ajax-скрипта');
			  }
			});
		}		
	});
}); 