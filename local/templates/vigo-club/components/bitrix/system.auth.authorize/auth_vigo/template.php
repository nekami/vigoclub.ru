<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

	<?
	$APPLICATION->IncludeFile(
		"/login/intro_auth.php",
		Array(),
		Array("MODE"=>"html")
	);
	?>
		
    <section class="section sign">
      <div class="container">
        <h2 class="section__title sign__title"><?=GetMessage("JOIN_VIRGOCLUB")?></h2>
		
		<div class="errortext"></div>
		
         <form class="sign__form form" action="" method="post">
		
		  <?=bitrix_sessid_post()?>
		  
		  <input type="hidden" name="AUTH_FORM" value="Y" />
          <input type="hidden" name="TYPE" value="AUTH" />
		  <?if (strlen($arResult["BACKURL"]) > 0) { ?>
              <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
          <? } ?>
		
		   <div class="form__item">
				<label class="form__label" for="email"><?=GetMessage("AUTH_EMAIL")?></label>
				<input class="form__input-text mail-input" type="email" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>">
				<div class="error_text">Поле обязательно для заполнения</div>
		   </div>
			  
		   <div class="form__item form__item--last">
				<label class="form__label" for="password"><?=GetMessage("AUTH_PASSWORD")?></label>
				<input class="form__input-text" type="password" name="USER_PASSWORD" id="password">
				<div class="error_text">Поле обязательно для заполнения</div>
				
				<p class="form__note form__note--text"><a href="/login/?forgot_password=yes" rel="nofollow">Забыли пароль? Напомнить</a></p>
				
		   </div>
		  
          <div class="btn form__btn" name="form-submit" id="authorize_user">Войти</div>
		  
        </form>
        <p class="sign__register">Еще не зарегистрированы? 
			<a href="/login/?register=yes" title="Зарегистрироваться">Зарегистрироваться</a>
		</p>
      </div>
    </section>

	
<script>
	BX.message({
		TEMPLATE_PATH: '<?=$this->GetFolder()?>'
	});
</script>