<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	
    <section class="section filter">
      <div class="container">
        <button type="button" class="filter__button">Показать фильтр</button>
        <form class="form filter__form" action="index.html" method="post">
          <fieldset class="form__fieldset filter__fieldset">
            <legend class="visually-hidden">Сортировка</legend>
            <ul class="filter__list list">
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="sort" value="popular" id="По популярности" checked>
                <label class="filter__check-label" for="По популярности">По популярности</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="sort" value="price" id="price">
                <label class="filter__check-label" for="price">По стоимости</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="radio" name="sort" value="new" id="new">
                <label class="filter__check-label" for="new">По новизне</label>
              </li>
            </ul>
          </fieldset>
          <fieldset class="form__fieldset filter__fieldset">
            <legend class="visually-hidden">Тип магазина</legend>
            <ul class="filter__list list">
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="checkbox" name="NAME" value="e-store" id="e-store">
                <label class="filter__check-label" for="e-store">Интернет-магазин</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="checkbox" name="store" value="store-retail" id="store-retail">
                <label class="filter__check-label" for="store-retail">Интернет-магазин и торговая точка</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="checkbox" name="store" value="app" id="app">
                <label class="filter__check-label" for="app">Приложение</label>
              </li>
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="checkbox" name="store" value="retail" id="retail">
                <label class="filter__check-label" for="retail">Торговая точка</label>
              </li>
            </ul>
          </fieldset>
          <fieldset class="form__fieldset filter__fieldset--last">
            <legend class="visually-hidden">Тип приза</legend>
            <ul class="filter__list list">
              <li class="filter__item">
                <input class="filter__check visually-hidden" type="checkbox" name="type" value="e-certificate" id="e-certificate">
                <label class="filter__check-label" for="e-certificate">Электронный сертификат</label>
              </li>
            </ul>
          </fieldset>
          <fieldset class="form__fieldset filter__range">
            <div class="filter__range-list">
              <div class="form__item filter__price filter__price--start">
                <label class="form__label" for="start-price">Бонусов от</label>
                <input class="form__input-text filter__price-start" type="number" name="start-price" id="start-price" placeholder="100" value="">
              </div>
              <div class="form__item filter__price filter__price--end">
                <label class="form__label" for="end-price">Бонусов до</label>
                <input class="form__input-text filter__price-end" type="number" name="end-price" id="end-price" placeholder="55000" value="">
              </div>
            </div>
          </fieldset>
          <div class="filter__buttons">
            <button type="submit" class="btn filter__btn filter__btn--submit">Показать</button>
            <button type="reset" class="btn filter__btn filter__btn--reset">Сбросить</button>
          </div>
        </form>
      </div>
    </section>
    <section class="section prize prize--inner">
      <div class="container">
        <h2 class="visually-hidden">Доступные призы</h2>
        <ul class="prize__list prize__list--inner list">
		
		<?
		foreach($arResult["ITEMS"] as $cell=>$arElement) { 
			$width = 0;
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM')));
						
			if ( !empty($arElement["PREVIEW_PICTURE"]["SRC"]) ) $img = $arElement["PREVIEW_PICTURE"]["SRC"];
			else $img = "/upload/vigo/priz.jpg";
					
		?>
		
          <li class="prize__item">		  
			<a href="<?=$arElement['DETAIL_PAGE_URL']?>">
				<div class="prize__item-top">
				  <div class="prize__image" aria-hidden="true">
					<picture>
					  <source data-srcset="<?=$img;?>" media="(min-width: 780px)">
					  <img data-src="<?=$img;?>" alt="<?=$arElement["PREVIEW_PICTURE"]["ALT"];?>" class="lazyload">
					</picture>
				  </div>
				  <p class="prize__text"><?=$arElement["NAME"]?></p>
				</div>
			</a>
            <div class="prize__item-bottom">
              <p class="prize__bonuses">
                <span class="prize__quantity">
				
				<? if ( $arElement["PROPERTIES"]["PAR"]["VALUE"] != false ) { ?>
				
					<?=min($arElement["PROPERTIES"]["PAR"]["VALUE"])?> - <?=max($arElement["PROPERTIES"]["PAR"]["VALUE"])?>
				
				<? } else if ( !empty($arElement["PROPERTIES"]["MIN"]["VALUE"]) && !empty($arElement["PROPERTIES"]["MAX"]["VALUE"]) ) { ?>
				
					<?=$arElement["PROPERTIES"]["MIN"]["VALUE"]?> - <?=$arElement["PROPERTIES"]["MAX"]["VALUE"]?>
				
				<? } else if ( $arElement["PROPERTIES"]["PAR_M"]["VALUE"] != false ) { ?>
				
					<?=min($arElement["PROPERTIES"]["PAR_M"]["VALUE"])?> - <?=max($arElement["PROPERTIES"]["PAR_M"]["VALUE"])?>
				
				<? } ?>			
				
				</span><span class="prize__bonus">бонусов</span>
              </p>
              <a href="#" class="prize__btn btn">Получить</a>
            </div>
          </li>
		  
		<? } ?>

        </ul>
      </div>
    </section>


	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?>
	<?endif;?>
	
<script>	
	BX.id_news = '<?=$arParams["AJAX_ID"]?>';
	BX.id_comp_news = 'comp_<?=$arParams["AJAX_ID"]?>';	
</script>