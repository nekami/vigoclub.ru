<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	
    <section class="section education education--inner inner-top">
      <div class="container">
        <h1 class="education__main-title section__title"><?=$arResult["NAME"]?></h1>
        <ul class="education__list list">
          
		<? foreach($arResult["ITEMS"] as $cell=>$arElement) 
		{ 	
			$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CATALOG_ELEMENT_DELETE_CONFIRM'))); ?>
		  
		  <li class="education__item education__item--inner">
            <div class="education__img">
              <picture>
                <source type="image/webp" data-srcset="<?=$arElement["PROPERTIES"]["WEBP_IMAGE"]["FILE_VALUE"]["SRC"]?>">
				
                <img data-src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arElement["PREVIEW_PICTURE"]["ALT"]?>" width="400" class="lazyload">
				
              </picture>
            </div>
            <h3 class="education__title"><?=$arElement["NAME"]?></h3>
            <div class="education__info-wrap">
              <p class="education__info education__info--level">Уровень подготовки: 			  
				<?=$arElement["PROPERTIES"]["LEVEL_TRAINING"]["~VALUE"]?>			  
			  </p>
              <p class="education__info education__info--time">
				<?=$arElement["PROPERTIES"]["VIDEO_TIME"]["~VALUE"]?>			  
			  </p>
            </div>
            <a href="<?=$arElement["DETAIL_PAGE_URL"]?>" class="btn education__btn">Посмотреть курс</a>
          </li>	  
		  
		<? } ?>  

        </ul>
      </div>
    </section>    

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>