<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	  
<? 
$previousLevel = 0;

if (!empty($arResult)) { ?>	

	<nav class="footer__nav">
	
	  <? foreach($arResult as $arItem) { ?>
	  
		  <?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) { ?>
		  
				<?=str_repeat("</ul></div>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		  
		  <? } ?>
		  
		  <?if ( ($arItem["DEPTH_LEVEL"] == 1) && !empty($arItem["IS_PARENT"])) { ?>
		  
			<div class="footer__nav-group">				  
				<a class="footer__nav-title" title="<?=$arItem["TEXT"]?>" href="<?=$arItem["LINK"]?>">					
					<?=$arItem["TEXT"]?>				  
				</a>				  
				<ul class="footer__nav-list list">	
				
		  <? } ?>

		  <? if (($arItem["DEPTH_LEVEL"] == 1) && empty($arItem["IS_PARENT"])) { ?>				
		  
			  <div class="footer__nav-group">
				  <a class="footer__nav-title" title="<?=$arItem["TEXT"]?>" href="<?=$arItem["LINK"]?>">
					<?=$arItem["TEXT"]?>					
				  </a>										
			  </div>	
			  
		  <? } ?>						
		  
		  <?if ($arItem["DEPTH_LEVEL"] > 1) { ?>				
		  
			<li class="footer__nav-item">
			  <a title="<?=$arItem["TEXT"]?>" href="<?=$arItem["LINK"]?>">
				<?=$arItem["TEXT"]?>
			  </a>				
			</li>			
		  <? } ?>						
		  
		  <?$previousLevel = $arItem["DEPTH_LEVEL"];?>					
	  
	  <? } ?>	
	  
	  <?if ($previousLevel > 1) { ?>	
		<?=str_repeat("</ul></div>", ($previousLevel-1) );?>		
	  <? } ?>			
	  
	</nav>    		
	  
<? } ?>