<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Правила участия");
?>

<section class="motivational_program">
	<div class="container">
	
		<h1>ПРАВИЛА<br/>
			проведения мотивационной программы<br/>
			«VIGO CLUB»
		</h1>
		
		<ol>
			<li>Общие положения </li>
			<ul>
				<li>1.1. Мотивационная программа под специальным наименованием «VIGO CLUB» (далее–Программа) проводится среди ограниченного круга лиц – сотрудников и представителей Торговых точек, указанных в п.2.12 Правил.</li>
				<li>1.2. Программа проводится Организатором в интернете на Сайте, указанном в п.2.10 Правил, регламентирована действующим законодательством Российской Федерации, а также настоящими Правилами.</li>
				<li>1.3. Организатором Программы (далее «Организатор») является ООО «СТП» (603003 Россия  г. Нижний Новгород, ул. Коминтерна,  д. 178 пом.4, ОГРН 1165275009679; ИНН 5263123410, адрес электронной почты для связи с Организатором: info@mebel-vigo.ru.</li>
			</ul>

			<li>Термины и определения </li>
			<ul>
				<li>2.1. Анкета участника–сведения, оставляемые Участником на Сайте при регистрации, в том числе:</li>
				<ul>
					<li>2.1.1. фамилия, имя, отчество; номер личного мобильного телефона; адрес электронной почты; информация о Торговой точке, в которой Участник работает;</li>
					<li>2.1.2. почтовый адрес; серия, номер, дата выдачи паспорта (или иного документа, удостоверяющего личность); номер ИНН и СНИЛС (при наличии) Участника; специальная опция (галочка), подтверждающая ознакомление и согласие Участника с настоящими Правилами и согласие на обработку его персональных данных в соответствии с Правилами.</li>
				</ul>
				<li>2.2. Баллы - баллы, начисляемые Участникам за реализацию того или иного вида Продукции. 
		Количество начисляемых Баллов указывается на Стикере и отражается в Личном кабинете Участника после того, как он выполнил действия, установленные в п. 4.1 Правил. Баллы используются Участниками для обмена на Подарки, исходя из курса обмена, устанавливаемого по решению Организатора.</li>
				<li>2.3. Каталог подарков - специальный раздел Сайта, предоставляющий содержащий перечень и полную информацию о Подарках, доступных для получения Участниками в обмен на Баллы.</li>
				<li>2.4. Код - уникальный буквенно цифровой код, расположенный под защитным слоем Стикера.
		Организатор вправе в любой момент запросить у Участника подтвердить правомерность владения Кодом, в ответ на запрос Участник обязан направить копию (скан)  Кода по электронному адресу, указанному Организатором. До момента получения копии (скана) Кода Организатор вправе приостановить начисление баллов и возможность регистрации Кода.</li>
				<li>2.5. Личный кабинет - индивидуальный раздел Участника на Сайте, доступ к которому осуществляется Участником после авторизации (ввод логина и пароля).</li>
				<li>2.6. Модерация - процедура проверки правомерности зарегистрированных Кодов на соответствие
		реальным продажам. Модерация осуществляется Организатором в течение всего срока
		Программы. В случае обнаружения внесения Участником ложной или ошибочной информации,
		Организатор не начисляет/снимает на численные Баллы.</li>
				<li>2.7. Подарки - товары и услуги интернет магазинов, перечень которых будет указан на Сайте, а
		также электронные сертификаты различных торговых сетей и интернет магазинов (далее Сертификаты), которые Участник может обменять на Баллы в Каталоге подарков. Все претензии относительно качества и внешнего вида товаров и качества услуг Участник предъявляет интернет магазину.</li>
				<li>2.8. Продукция – товары, реализуемые под товарным знаком VIGO, содержащий внутри упаковки Стикер.</li>
				<li>2.9. Сайт - сайт, на котором проводится Программа, расположенный в интернете по адресу:
		http://vigo-club.ru.</li>
				<li>2.10. Стикер - наклейка на упаковке Продукции, содержащая название Программы и защитный
		светонепроницаемый слой, под которым размещен Код. Участник Программы вправе
		воспользоваться Стикером только в отношении той Продукции, которая была реализована
		покупателям при участии Участника в качестве представителя Торговой точки. Участникам
		запрещено изымать и стирать защитный слой Стикеров от нереализованной Продукции, а также
		Участникам запрещено использовать какие-либо приспособления и способы прочтения
		символов, содержащихся под защитным слоем таких Стикеров. Участник обязан сохранить
		Стикер до момента получения Подарков.</li>
				<li>2.11. Торговые точки - розничные магазины, осуществляющие реализацию Продукции конечным потребителям на территории Российской Федерации, а также иных стран Евразийского экономического союза. В Программе не принимают участие оптовые торговые точки.</li>
				<li>2.12. Участники - дееспособные лица, достигшие возраста 18-ти лет, являющиеся сотрудниками
		Торговых точек, осуществляющие продажу Продукции (далее Продавцы), выдача Продукции
		со склада (далее Кладовщик) и принимающие решение о закупке Продукции (далее
		Менеджеры). Участниками не могут быть сотрудники Организатора, члены их семей, а также
		работники других юридических лиц и/или индивидуальных предпринимателей, причастных к
		проведению Программы, и члены их семей.</li>
			</ul>
			
			<li>Сроки проведения </li>	
			
			<ul>
				<li>3.1. Все действия, предусмотренные настоящими Правилами, должны быть совершены и
		фиксируются Организатором по московскому времени, с 00 часов 00 минут 00 секунд по 23 час а
		59 минут 59 секунд соответствующих календарных суток, входящих в установленный период,
		если отдельно не оговорено иное. Любое время, указанное в настоящих Правилах, необходимо
		рассматривать как московское.</li>
				<li>3.2. Программа проводится в следующие сроки (включительно):</li>
				<ul>
					<li>3.2.1. Регистрация Участников на Сайте и накопление Баллов в порядке, установленном в п. 4.1
		Правил, осуществляется в период с 1 июня 2020 года по 31 декабря 2022 года.</li>
					<li>3.2.2. Выбор Подарков в Каталоге осуществляется Участниками в период с 15 июня 2020 года по
		31 декабря 2022 года.</li>
					<li>3.2.3. Доставка ( Подарков осуществляется в следующие сроки:</li>
					<ul>
						<li>3.2.3.1. Подарки из интернет магазина в сроки, устанавливаемые интернет магазином при
		обмене Участником Баллов на Подарки;</li>
						<li>3.2.3.2. Сертификаты, в том числе электронные сертификаты  в течение 30 ти календарных дней после их заказа Участником.</li>				
					</ul>
				</ul> 
			</ul>        
			
			<li>Порядок участия в Программе </li>
			<ul>
				<li>4.1. Для участия в Программе Продавцы обязаны в период, указанный в п. 3.2.1 Правил, выполнять
		следующие действия:</li>
				<ul>
					<li>4.1.1. Пройти регистрацию на Сайте, заполнив Анкету участника, а также загрузить копию
		паспорта.</li>
					<li>4.1.2.Совершать продажи Продукции и регистрировать их на Сайте в следующем порядке:</li>
					<ul>
						<li>4.1.2.1.После реализации Продукции Участник находит внутри упаковки Продукции Стикер,
		стирает защитный слой и выявляет скрытый Код.</li>
						<li>4.1.2.2.Далее Участник регистрирует выявленный Код на Сайте.</li>				
					</ul>       
					<li>4.1.3.Участник также имеет возможность зарабатывать дополнительные Баллы, принимая
		участие в активностях, реализуемых на Сайте.</li>
					<li>4.1.4.Стикер необходимо сохранить до момента получения Подарков.</li>
				</ul>
				<li>4.2. По итогам совершения действий, указанных в п. 4.1 Правил, Участникам начисляются Баллы в
		порядке, установленном в разделе 5 Правил.</li>
			</ul>
			<li>Порядок начисления и использования Баллов</li>
			<ul>
				<li>5.1. Для начисления Баллов Продавец фактически отгружающий Продукцию
		покупателю, обязаны регистрировать продажи на Сайте в порядке, установленном в п. 4.1.2
		Правил.</li>
				<li>5.2. В рамках Программы Продавцам начисляются Баллы в зависимости от вида
		реализуемой Продукции, на основании Кода. Количество начисляемых Баллов указывается на
		Стикере.</li>
				<li>5.3. Баллы активируются (становятся доступными для обмена на Подарки) после истечения 14-ти
		календарных дней после даты регистрации Кодов, при условии, что Участник предоставил все
		данные (если от Организатора поступил запрос о подтверждении продажи).</li>
				<li>5.4. По итогам начисления Баллы отражаются в Личном кабинете Участника, о чём ему поступает
		соответствующее уведомление на электронную почту.</li>
				<li>5.5. Организатор вправе проводить модерацию (проверку) начисленных Баллов в течение всего
		срока Программы, снимая Баллы, начисленные ошибочно или неправомерно.
		Правила проведения мотивационной программы «VIGO CLUB»</li>
				<li>5.6. Участник может использовать Баллы в период, указанный в п.3.2.2 Правил, для обмена в Каталоге на Подарки, доступные на дату использования Баллов.</li>
				<li>5.7. При окончании того или иного Подарка или срока, установленного в п. 3.2.2 Правил,
		накопленные Баллы Участникам никаким образом не возмещаются и не обмениваются.</li>
				<li>5.8. В случае если Участник добровольно выходит из Программы, активировав опцию «Уйти из
		программы», все начисленные Баллы аннулируются, и компенсации не подлежат.</li>
				<li>5.9. В случае если Участника из Программы исключает Администратор в связи с тем, что Участник
		уволился из Торговой точки, его Личный кабинет и начисленные Баллы сохраняется до момента
		окончания Программы. В случае если Участник возобновляет рабочие отношения с прежней или новой Торговой точкой, он получает доступ к Личному кабинету и может воспользоваться ранее начисленными Баллами.</li>
			</ul>         
			<li>Вручение Подарков </li>
			<ul>
				<li>6.1. Для получения Подарков Участник обязан пройти в Каталог подарков, выбрать Подарки в соответствии с количеством начисленных Баллов, и подтвердить свой заказ.</li>
				<li>6.2. При выборе Приза Участник выбирает доступные параметры доставки Подарка, при необходимости обменивая Баллы на дополнительные услуги доставки.</li>
				<li>6.3. В случае если общая стоимость Подарков одного Участника превышает 4000 (четыре тысячи)
		рублей, последующие Подарки направляются Участнику в следующем порядке:</li>
				<ul>
					<li>6.3.1.Организатор формирует Акт приёмки передачи всех Подарков (Сертификатов), полученных
		Участником (далее Акт) и отправляет его по электронной почте Участнику.</li>
					<li>6.3.2.Участник обязан в течение 3 х календарных дней распечатать Акт, подписать его и отправить скан-копию Организатору. Организатор вправе запросить от Участника Оригинал Акта, который тот обязан направить в течение 3 х рабочих дней по почтовому адресу, указанному Организатором. После получения Акта Организатор направляет Подарки выбранным Участником способом (электронной почтой или курьерской службой за счет Участника).</li>
					<li>6.3.3.Все последующие Подарки направляются Участнику после подписания Акта.</li>
				</ul> 
				<li>6.4. В отношении Участников, являющихся гражданами РФ, а также получивших от Организатора в
		течение одного календарного года Подарков на общую сумму, превышающую 4000 (четыре
		тысячи), Организатор устанавливает дополнительный денежный приз в сумме, определяемой по решению Организатора, а также Организатор выполняет функции налогового агента (представляет соответствующую информацию в налоговый орган и удерживает и уплачивает из суммы денежного приза НДФЛ по ставке, установленной п. 1 ст. 224 НК РФ (13%) в отношении суммы, превышающей налоговый вычет, установленный п. 28 ст. 217 НК РФ (4000 рублей), в связи с чем у указанных Участников не возникает дополнительных налоговых обязанностей. Принимая участие в Программе и соглашаясь с настоящими Правилами, Участники считаются надлежащим образом Проинформированным и о вышеуказанной обязанности.</li>
				<li>6.5. Подарки не вручаются Участникам по следующим причинам:</li>
				<ul>
					<li>6.5.1. Участник отказался от Подарка.</li>
					<li>6.5.2. Участник не представил или представил несвоевременно в полном объёме информацию или документы, указанные в Правилах.</li>
					<li>6.5.3. Участник предоставил искажённую или недостоверную информацию при регистрации на Сайте.</li>
					<li>6.5.4. Участник указал неполные/некорректные данные для отправки Подарка;</li>
					<li>6.5.5. Участник не выполнил необходимые действия для получения Подарка.</li>
					<li>6.5.6. Участник не вступил в переговоры с почтовой службой.</li>
					<li>6.5.7. На счете недостаточно баллов на Подарок и доставку Подарка.</li>
					<li>6.5.8. Участник нарушил настоящие Правила.</li>
				</ul>     
				<li>6.6. Повторная отправка Подарков, возвращённых Организатору по причинам, независящим от Организатора. По просьбе Участника Подарок может быть отправлен повторно (при условии, что Подарок возвращён Организатору Почтовой службой), при этом в Личном кабинете Участника списываются Баллы, эквивалентные стоимости доставки Подарка в адрес Участника. При отказе от повторной отправки, а также если накопленных баллов Участника недостаточно для оплаты доставки, повторная поставка Подарка не осуществляется, Баллы за Подарок не возвращаются.</li>
				<li>6.7. Повторная отправка Подарков производится при соблюдении всех условий для получения Подарка с учетом п.6.5 настоящих Правил.</li>
			</ul>	
			<li>Информирование Участников</li>
			<ul>
				<li>7.1. Официальные правила Программы в полном объеме для открытого доступа размещаются на Сайте.</li>
				<li>7.2. Информирование Участников об изменении Правил, об отмене Программы или об иных существенных событиях, связанных с проведением Программы, производится через Сайт.</li>		
			</ul>         
			<li>Договор на участие в программе</li>
			<ul>
				<li>8.1. Договор на участие в Программе между Организатором и Участником не предполагает для последнего никаких взносов, заключается путём присоединения Участника к условиям, содержащимся в тексте настоящих Правил, следующим способом:</li>
				<ul>
					<li>8.1.1.Заключение указанного договора производится путем направления публичной оферты (предложения) Организатором посредством размещения настоящих Правил на Сайте и принятия оферты (акцепта) лицом, соответствующим требованиям, установленным в п.2.13Правил, путем совершения действий, установленных в п.4.1Правил.</li>
					<li>8.1.2.Договор между Организатором и Участником считается заключенным в момент регистрации на Сайте, после чего такое лицо признаётся Участником.</li>			
				</ul>
				<li>8.2. Факт регистрации Участника в Программы подразумевает, что:</li>
				<ul>
					<li>8.2.1.Участник ознакомлен с настоящими Правилами и согласен с ними.</li>
					<li>8.2.2.Участник предоставляет своё согласие на обработку его персональных данных на условиях, установленных настоящими Правилами.</li>
					<li>8.2.3.Участник согласен на получение по сетям электросвязи (электронная почта, мобильный телефон) от Организатора информации о Программе.</li>
					<li>8.2.4.Участник согласен на включение его персональных данных в базу рассылок маркетинговых и рекламных предложений в отношении продукции, реализуемой под товарным знаком VIGO и на получение указанных сообщений по сетям электросвязи (мобильный телефон, электронная почта, мобильный мессенджер за пределами Программы.</li>
				</ul>
			</ul>     
			<li>Изменение Правил и досрочное завершение Программы</li>
			<ul>
				<li>9.1. Организатор имеет право отменить, досрочно завершить Программу или изменить настоящие Правила в одностороннем порядке по своему усмотрению. Изменение или досрочное завершение Программы возможно также в случае возникновения каких-либо технических проблем, негативного воздействия на Сайт вредоносных компьютерных программ (вирусов) или возникновения иных сбоев в работе Сайта, препятствующих его проведению, а равно – при возникновении форс-мажорных или других 
		обстоятельств, делающих проведение Программы невозможным, невыполнимым, нецелесообразным, или по любым иным причинам. При досрочном завершении Программы все Участники, которым были начислены Баллы, имеют возможность их использовать в порядке, установленном в разделе 6 Программы, в течение трех месяцев с момента публикации извещения о досрочном завершении Программы.</li>
				<li>9.2. Организатор информирует Участников об изменении Правил или отмене Программы в порядке, установленном в ст. 7 Правил.</li>		
			</ul>       
			<li>Политика обработки персональных данных</li>
			<ul>
				<li>10.1. Факт выполнения действий, установленных настоящими Правилами, является согласием Участника на обработку персональных данных, предоставленных им при регистрации в Программе, в рамках проведения Программы самим Организатором или привлечёнными им лицами в строгом соответствии с целями, установленными настоящими Правилами. Оператором персональных данных является Организатор, сведения о котором указаны в п. 1.3 Правил.</li>
				<li>10.2. Цели обработки персональных данных Участников: 1) проведение Программы в соответствии с настоящими Правилами и действующим законодательством; 2) исполнение Организатором обязанностей налогового агента; 3) использование данных для отправки писем и сообщений от Организатора или 
		уполномоченных ими лицами по сетям электросвязи в рамках Программы; 4) регистрация на Сайте в качестве пользователей; 5) за пределами Программы – осуществление рассылок маркетинговых и рекламных предложений в отношении продукции, реализуемой под товарным знаком VIGO.</li>
				<li>10.3. Перечень персональных данных, которые предоставляются Участником и обрабатываются Организатором или привлекаемыми им лицами ограничивается сведениями, который сообщит Участник в соответствии с настоящими Правилами.</li>
				<li>10.4. Перечень действий с предоставляемыми Участниками персональными данными: сбор, запись, систематизация, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передача (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных.</li>
				<li>10.5. Трансграничная передача персональных данных в рамках проведения Программы не осуществляется, персональные данные обрабатываются и хранятся на территории РФ.</li>
				<li>10.6. Организатор и привлечённые им лица осуществляют обработку персональных данных Участников в строгом соответствии с принципами и правилами, установленными Федеральным законом от 27.07.2006 № 152-ФЗ «О персональных данных», включая соблюдение конфиденциальности и обеспечения безопасности персональных данных при их обработке, включая требования к защите, установленные ст. 19 названного Закона. </li>
				<li>10.7. Организатор организует обработку персональных данных в срок проведения Программы. В течение 60-ти дней после окончания Программы, все персональные данные Участников, находящиеся в распоряжении Организатора, подлежат уничтожению, за исключением документов и сообщений, поступивших от Участников, в отношении доходов которых выполнены функции налогового агента, которые хранятся в течение 5-ти лет, а также за исключением данных, включенных в базу маркетинговых и рекламных рассылок, а также сохраненных в качестве пользователя Сайта, которые хранятся бессрочно или до их отзыва субъектом.</li>
				<li>10.8. Участник вправе в любое время отозвать разрешение на обработку персональных данных путем направления скана письменного заявления по электронному адресу Организатора, указанному в п. 1.3 Правил, или путём активации в личном кабинете на Сайте кнопки «Удалить профиль», что влечёт автоматическое прекращение участия в Программе лица, отозвавшего свои персональные данные, ликвидацию Личного кабинета, а также аннулирование всех накопленных Баллов. В случае если Участник в Программе получил доход, превышающий сумму в размере 4000 рублей, его персональные данные не уничтожаются и хранятся в срок, установленный в п. 10.7 Правил, с целью выполнения обязанностей налогового агента.</li>
				<li>10.9. Персональные данные Покупателей обрабатываются в соответствии с Согласием, подписываемым Покупателем путем проставления «галки» на веб-сайте Программы после прочтения и полного и безоговорочного принятия настоящих Правил. </li>
			</ul>
			<li>Дополнительные условия:</li>
			<ul>
				<li>11.1. Организатор на свое собственное усмотрение может признать недействительными все начисленные Баллы, а также запретить дальнейшее участие в Программы Участникам, которые предоставляют ложную или недостоверную информацию о себе или же действуют в нарушение настоящих Правил, действуют деструктивным образом, или осуществляют действия с намерением досаждать, оскорблять, угрожать или причинять беспокойство любому лицу, которое может быть связано с Программой. Организатор также исключает из Программы лиц, которые используют Коды от нереализованной Продукции либо допускают иные недобросовестные действия.</li>
				<li>11.2. Организатор оставляет за собой право в любой момент вводить дополнительные технические ограничения, препятствующие недобросовестным действиям на Сайте.</li>
				<li>11.3. Факт участия в настоящей Программе подразумевает, что Участник ознакомлен и согласен с настоящими Правилами.</li>
				<li>11.4. Организатор оставляет за собой право не вступать в письменные переговоры, либо иные контакты с лицами, во всех случаях, кроме тех, что предусмотрены настоящими Правилами.</li>
				<li>11.5. Организатор Программы, а также уполномоченные им лица не несут ответственности за технические сбои в сети Интернет-провайдера, к которой подключен Участник, не позволяющие выполнить действия для участия в Программе; за действия/бездействие оператора интернет-связи, к которой подключен Участник; за не ознакомление Участников с настоящими Правилами, а также за неполучение от Участников сведений, необходимых для получения Подарков, по не зависящим от Организатора причинам.</li>
				<li>11.6. Все спорные вопросы, касающиеся Программы, регулируются на основе действующего законодательства РФ.</li>
				<li>11.7. Термины и определения, использованные в настоящих Правилах, устанавливаются Организатором по своему усмотрению и, при необходимости, Организатор вправе публиковать в порядке, установленном разделе 7, разъяснения и дополнения к настоящим Правилам.</li>
				<li>11.8. Перечень изменений, внесенных в Правила:</li>
				<ul>
					<li>11.8.1. Изменения от 19 июня 2019 года, вносимые в п. 2.12 Правил, ступающие в силу с даты публикации (19 июня 2019 года).</li>
				</ul>
			</ul>
		</ol> 

	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>