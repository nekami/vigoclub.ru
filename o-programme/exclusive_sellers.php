    <section class="section for-consultants">
      <div class="container">
        <h2 class="section__title for-consultants__title">Эксклюзивно для продавцов мебели VIGO</h2>
        <ul class="for-consultants__list for-consultants__list--learn list">
          <li class="for-consultants__item">
            <h3 class="for-consultants__item-title">Гибкое обучение онлайн </h3>
            <p class="for-consultants__text">Учитесь в удобном темпе, затрачивайте не более часа в день и исключительно онлайн. Получите официальные сертификаты.</p>
          </li>
          <li class="for-consultants__item">
            <h3 class="for-consultants__item-title">Бесплатно</h3>
            <p class="for-consultants__text">Получайте актуальные знания высочайшего качества от крупной и современной российской компании</p>
          </li>
        </ul>
        <ul class="for-consultants__list for-consultants__list--prize list">
          <li class="for-consultants__item">
            <h3 class="for-consultants__item-title">Более 1000 подарков за вашу активность</h3>
            <p class="for-consultants__text">За продажи и другие активности вы получаете бонусы, которые можно обменять на ценные призы</p>
          </li>
          <li class="for-consultants__item">
            <h3 class="for-consultants__item-title">Доступно на смартфоне</h3>
            <p class="for-consultants__text">Мы заботимся о наших продавцах, и стараемся сделать работу еще более комфортной! </p>
          </li>
        </ul>
      </div>
    </section>