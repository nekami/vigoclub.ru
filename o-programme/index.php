<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О Программе");
?> 
    <h1 class="visually-hidden">О программе</h1>	
	
	<!-- Банер -->	
	<?$APPLICATION->IncludeComponent(
	"bitrix:news.detail", 
	"kontakty_banner", 
	array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => "o-programme",
		"ELEMENT_ID" => $_REQUEST["ELEMENT_ID"],
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "blocks",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PROPERTY_CODE" => array(
			0 => "NAME_BANNER",
			1 => "DESC_BANNER",
			2 => "BANNER_PNG_PC",
			3 => "BANNER_PNG_MOB",
			4 => "BANNER_WEBP_PC",
			5 => "BANNER_WEBP_MOB",
			6 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N",
		"COMPONENT_TEMPLATE" => "kontakty_banner"
	),
	false
);?>	

<? // Ваш путь к успеху
$APPLICATION->IncludeFile(
	"/o-programme/way_success.php",
	Array(),
	Array("MODE"=>"html")
);   
?>	
	
<? // Эксклюзивно для продавцов мебели VIGO
$APPLICATION->IncludeFile(
	"/o-programme/exclusive_sellers.php",
	Array(),
	Array("MODE"=>"html")
);   
?>		
	
	
<section class="section steps">
	<div class="container">
	
	  <? // Присоединяйся к VigoClub!
		$APPLICATION->IncludeFile(
			"/o-programme/join.php",
			Array(),
			Array("MODE"=>"html")
		);   
	  ?>  
	  
	  <?$APPLICATION->IncludeComponent(
			"bitrix:system.auth.registration",
			"mini_auth_vigo",
		     Array()
	  );?>
		
      </div>
    </section>
	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>