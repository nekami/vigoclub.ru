    <section class="section success">
      <div class="container">
        <h2 class="section__title success__title">Ваш путь к успеху</h2>
        <ul class="success__list list">
          <li class="success__item success__item--skills">
            <div class="success__img" aria-hidden="true">
              <img src="/upload/vigo/a-pic1.png" alt="навыки" width="100">
            </div>
            <h3 class="success__item-title">Актуальные навыки для работы</h3>
            <p class="success__text">Развивайте навыки с онлайн-курсами, сертификациями и дипломными программами от современной российской компании VIGO</p>
          </li>
          <li class="success__item success__item--motivation">
            <div class="success__img" aria-hidden="true">
              <img src="/upload/vigo/a-pic2.png" alt="подарки" width="100">
            </div>
            <h3 class="success__item-title">Бонусная мотивация и подарки</h3>
            <p class="success__text">Совершайте продажи продукции и регистрируйте их на нашем сайте. За продажи и другие активности вы получаете бонусы, которые можно обменять на ценные призы</p>
          </li>
          <li class="success__item success__item--community">
            <div class="success__img" aria-hidden="true">
              <img src="/upload/vigo/a-pic3.png" alt="комьюнити" width="100">
            </div>
            <h3 class="success__item-title">Профессиональное комьюнити</h3>
            <p class="success__text">Продемонстрируйте свои новые навыки: покажите окружающим сертификат о прохождении курса. А также участвуйте в наших реферальных программах</p>
          </li>
        </ul>
      </div>
    </section>