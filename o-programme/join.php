<h2 class="section__title steps__title">Присоединяйся к VigoClub!</h2>
        <ul class="steps__list list">
          <li class="steps__item">
            <p class="steps__number"><span>1</span></p>
            <p class="steps__item-title">Авторизуйся</p>
            <p class="steps__text">Чтобы начать пользоваться системой, необходимо авторизоватся</p>
          </li>
          <li class="steps__item">
            <p class="steps__number"><span>2</span></p>
            <p class="steps__item-title">Регистрируй коды</p>
            <p class="steps__text">Продавай продукцию VIGO и регистрируй коды со стикеров</p>
          </li>
          <li class="steps__item">
            <p class="steps__number"><span>3</span></p>
            <p class="steps__item-title">Получай подарки</p>
            <p class="steps__text">Большой выбор подарков для всех участников</p>
          </li>
    </ul>